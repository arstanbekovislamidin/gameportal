using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using WebActivatorEx;
using GamePortal.WebAPI;
using Swashbuckle.Application;
using Swashbuckle.Swagger;
using System.Web.Http.Description;
using System.Web.Http.Filters;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace GamePortal.WebAPI
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                {
                    c.SingleApiVersion("v1", "GamePortal");
                    c.UseFullTypeNameInSchemaIds();
                    c.DocumentFilter<AuthTokenOperation>();
                    c.OperationFilter(() => new AddAuthorizationHeaderParameterOperationFilter());
                    //c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
                })
                .EnableSwaggerUi(c => { });
        }
    }

    public class AddAuthorizationHeaderParameterOperationFilter : IOperationFilter
    {
        public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
        {
            var filterPipeline = apiDescription.ActionDescriptor.GetFilterPipeline();
            var isAuthorized = filterPipeline.Select(filterInfo => filterInfo.Instance).Any(filter => filter is IAuthorizationFilter);

            var allowAnonymouse = apiDescription.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any();

            if (isAuthorized && !allowAnonymouse)
            {
                if (operation.parameters == null)
                {
                    operation.parameters = new List<Parameter>();
                }
                operation.parameters.Add(new Parameter
                {
                    name = "Authorization",
                    @in = "header",
                    description = "access token",
                    required = true,
                    @default = "bearer ",
                    type = "string"
                });
            }
        }
    }

    class AuthTokenOperation : IDocumentFilter
    {
        public void Apply(SwaggerDocument swaggerDoc, SchemaRegistry schemaRegistry, IApiExplorer apiExplorer)
        {
            swaggerDoc.paths.Add("/api/Token", new PathItem
            {
                post = new Operation
                {
                    tags = new List<string> { "Auth" },
                    consumes = new List<string>
                    {
                        "application/x-www-form-urlencoded"
                    },
                    parameters = new List<Parameter> {
                        new Parameter
                        {
                            type = "string",
                            name = "grant_type",
                            required = true,
                            @default = "password",
                            @in = "formData"
                        },
                        new Parameter
                        {
                            type = "string",
                            name = "username",
                            required = false,
                            @default = "Islam",
                            @in = "formData"
                        },
                        new Parameter
                        {
                            type = "string",
                            name = "password",
                            required = false,
                            @default = "12qw!@QW",
                            @in = "formData"
                        },
                        new Parameter
                        {
                            type = "string",
                            name = "client_id",
                            required = false,
                            @default = "dbf50263-1ea1-e811-8947-902b347270c5",
                            @in = "formData"
                        },
                        new Parameter
                        {
                            type = "string",
                            name = "client_secret",
                            required = false,
                            @default = "secret",
                            @in = "formData"
                        }
                    }
                }
            });
        }
    }
}
