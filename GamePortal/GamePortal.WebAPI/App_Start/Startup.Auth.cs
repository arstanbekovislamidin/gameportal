﻿using System;
using GamePortal.Data.DB;
using GamePortal.Domain.Enums;
using GamePortal.Domain.Models.Identity;
using GamePortal.Logic.Services.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Hangfire;
using Owin;
using GamePortal.WebAPI.Providers;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using GlobalConfiguration = System.Web.Http.GlobalConfiguration;

namespace GamePortal.WebAPI
{
    public partial class Startup
    {
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        public static string PublicClientId { get; private set; }

        // For more information on configuring authentication, please visit https://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            Hangfire.GlobalConfiguration.Configuration
                .UseSqlServerStorage("DefaultConnection");

            app.UseHangfireDashboard();
            app.UseHangfireServer();

            var dependencyResolver = GlobalConfiguration.Configuration.DependencyResolver;
            var unitOfWork = dependencyResolver.GetService(typeof(UnitOfWork)) as UnitOfWork;

            // Configure the db context, user manager and signin manager to use a single instance per request
            app.CreatePerOwinContext<UnitOfWork>((options, context) => dependencyResolver.GetService(typeof(UnitOfWork)) as UnitOfWork);
            app.CreatePerOwinContext<IUserStore<User, Guid>>((options, context) => new ApplicationUserStore(unitOfWork?.Context));
            app.CreatePerOwinContext<IRoleStore<Role, Guid>>((options, context) => new ApplicationRoleStore(unitOfWork?.Context));
            app.CreatePerOwinContext<ApplicationUserManager>((options, context) => new ApplicationUserManager(context.Get<IUserStore<User, Guid>>()));
            app.CreatePerOwinContext<ApplicationRoleManager>((options, context) => new ApplicationRoleManager(context.Get<IRoleStore<Role, Guid>>()));
            app.CreatePerOwinContext<ApplicationSignInManager>((options, context) => new ApplicationSignInManager(context.Get<ApplicationUserManager>(), context.Authentication));

            #region SeedDatabaseWithIdentities
            var roleManager = new RoleManager<Role, Guid>(new RoleStore<Role, Guid, UserRole>(unitOfWork?.Context));
            if (!roleManager.RoleExists(RoleNames.Administrator))
            {
                roleManager.Create(new Role
                {
                    Name = RoleNames.Administrator
                });
            }
            if (!roleManager.RoleExists(RoleNames.Manager))
            {
                roleManager.Create(new Role
                {
                    Name = RoleNames.Manager
                });
            }
            if (!roleManager.RoleExists(RoleNames.User))
            {
                roleManager.Create(new Role
                {
                    Name = RoleNames.User
                });
            }
            #endregion

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseCookieAuthentication(new CookieAuthenticationOptions());

            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Configure the application for OAuth based flow
            PublicClientId = "self";
            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/api/Token"),
                Provider = new ApplicationOAuthProvider(PublicClientId),
                AuthorizeEndpointPath = new PathString("/api/Account/ExternalLogin"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(14),
#if DEBUG
                AllowInsecureHttp = true,
#endif
            };

            // Enable the application to use bearer tokens to authenticate users
            app.UseOAuthBearerTokens(OAuthOptions);

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

            //app.UseTwitterAuthentication(
            //    consumerKey: "",
            //    consumerSecret: "");

            //app.UseFacebookAuthentication(
            //    appId: "",
            //    appSecret: "");

            //app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            //{
            //    ClientId = "",
            //    ClientSecret = ""
            //});
        }
    }
}
