﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using GamePortal.Data.DB;
using GamePortal.Domain.Models.Entities;
using GamePortal.Domain.Models.Identity;
using GamePortal.Logic.Services.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;

namespace GamePortal.WebAPI.Providers
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly string _publicClientId;

        public ApplicationOAuthProvider(string publicClientId)
        {
            _publicClientId = publicClientId ?? throw new ArgumentNullException(nameof(publicClientId));
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();

            User user = await userManager.FindAsync(context.UserName, context.Password);

            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }

            ClaimsIdentity identity =
                await user.GenerateUserIdentityAsync(userManager, context.Options.AuthenticationType);
            var roles = await userManager.GetRolesAsync(user.Id);
            AuthenticationProperties properties = CreateProperties(user.UserName, user.Id, Guid.Parse(context.ClientId), roles.ToList());
            AuthenticationTicket ticket = new AuthenticationTicket(identity, properties);
            context.Validated(ticket);
            context.Request.Context.Authentication.SignIn(identity);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            if (!context.TryGetBasicCredentials(out var clientId, out var clientSecret))
            {
                context.TryGetFormCredentials(out clientId, out clientSecret);
            }
            if (!Guid.TryParse(clientId, out var clientGuid))
            {
                context.SetError("invalid_client", "Client credentials could not be retrieved from the Authorization header");
                context.Rejected();
                return;
            }
            UnitOfWork unitOfWork = context.OwinContext.Get<UnitOfWork>();
            var client = await unitOfWork.GetRepository<Client>().FirstOrDefaultAsync(x => x.Id == clientGuid && x.Secret == clientSecret);
            if (client == null)
            {
                context.SetError("invalid_client", "Client credentials could not be retrieved from the Authorization header");
                context.Rejected();
                return;
            }

            context.Validated(clientId);
        }

        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (context.ClientId == _publicClientId)
            {
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

                if (expectedRootUri.AbsoluteUri == context.RedirectUri)
                {
                    context.Validated();
                }
            }

            return Task.FromResult<object>(null);
        }

        public static AuthenticationProperties CreateProperties(string userName, Guid userId = default(Guid), Guid clientId = default(Guid), List<string> roles = null)
        {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", userName }
            };
            if (userId != default(Guid))
            {
                data.Add("userId", userId.ToString());
            }
            if (clientId != default(Guid))
            {
                data.Add("clientId", clientId.ToString());
            }
            if (roles != null)
            {
                data.Add("roles", string.Join(",", roles));
            }
            return new AuthenticationProperties(data);
        }
    }
}