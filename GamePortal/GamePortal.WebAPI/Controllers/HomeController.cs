﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using GamePortal.Domain.Contracts;
using GamePortal.Domain.Contracts.Services;

namespace GamePortal.WebAPI.Controllers
{
    [Authorize]
    public class HomeController : BaseWebApiController
    {
        public HomeController(IServiceHost serviceHost) : base(serviceHost)
        {
        }

        public JsonResult Index()
        {
            var gameService = ServiceHost.GetService<IGameService>();
            var games = gameService.GetGamesPagination(1, 5, string.Empty, new List<Guid>(), new List<Guid>());
            var newsService = ServiceHost.GetService<INewsService>();
            var news = newsService.GetNewsPagination(1, 5, string.Empty, new List<Guid>());
            return new JsonResult
            {
                Data = new 
                {
                    Games = games.Items,
                    News = news.Items
                }
            };
        }
    }
}
