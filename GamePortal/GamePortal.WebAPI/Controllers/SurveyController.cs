﻿using System;
using System.Web.Http;
using GamePortal.Domain.Contracts;
using GamePortal.Domain.Contracts.Services;
using GamePortal.Domain.Enums;
using GamePortal.Domain.Models.DTO.ResponseModels;
using GamePortal.Domain.Models.DTO.ViewModels;

namespace GamePortal.WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/survey")]
    public class SurveyController : BaseWebApiController
    {
        public SurveyController(IServiceHost serviceHost) : base(serviceHost)
        {
        }

        [HttpGet]
        [Route("paginated")]
        public IHttpActionResult GetPaginated(PaginationRequestModel model)
        {
            var service = ServiceHost.GetService<ISurveyService>();
            var result = service.GetSurveysPagination(model.Page, model.PageSize, model.NameFilter, User.IsInRole(RoleNames.Manager) || User.IsInRole(RoleNames.Administrator));
            return Json(new
            {
                data = result
            });
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpPost]
        [Route("create")]
        public IHttpActionResult Create(SurveyResponseModel model)
        {
            var service = ServiceHost.GetService<ISurveyService>();
            var result = service.AddSurvey(model, CurrentUserId);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [HttpGet]
        [Route("survey")]
        public IHttpActionResult Survey(Guid surveyId)
        {
            var service = ServiceHost.GetService<ISurveyService>();
            var model = service.GetSurveyWithUser(surveyId, CurrentUserId);
            if (model.Status != SurveyStatus.Started)
            {
                return BadRequest("Конкурс еще не начался");
            }
            return Json(new
            {
                data = model
            });
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpGet]
        [Route("edit")]
        public IHttpActionResult Edit(Guid surveyId)
        {
            var service = ServiceHost.GetService<ISurveyService>();
            var model = service.GetSurvey(surveyId);
            return Json(new
            {
                data = model
            });
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpPut]
        [Route("edit")]
        public IHttpActionResult Update(SurveyResponseModel model)
        {
            var service = ServiceHost.GetService<ISurveyService>();
            var result = service.UpdateSurvey(model);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [HttpDelete]
        [Route("delete")]
        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        public IHttpActionResult Delete(Guid surveyId)
        {
            var service = ServiceHost.GetService<ISurveyService>();
            var result = service.DeleteSurvey(surveyId);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [HttpPost]
        [Route("chooseAnswer")]
        public IHttpActionResult ChooseAnswer(Guid answerId)
        {
            var service = ServiceHost.GetService<ISurveyService>();
            var result = service.ChooseAnswer(answerId, CurrentUserId);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }
    }
}