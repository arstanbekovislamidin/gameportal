﻿using System;
using System.Web.Http;
using GamePortal.Domain.Contracts;
using GamePortal.Domain.Contracts.Services;
using GamePortal.Domain.Enums;
using GamePortal.Domain.Models.DTO.ResponseModels;

namespace GamePortal.WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/client")]
    public class ClientController : BaseWebApiController
    {
        public ClientController(IServiceHost serviceHost) : base(serviceHost)
        {
        }

        [HttpGet]
        [Route("paginated")]
        public IHttpActionResult GetClientsPaginated(int page = 1, int pageSize = 20, string nameFilter = "")
        {
            return Json(new
            {
                Data = ServiceHost.GetService<IClientService>().GetClientsPagination(page, pageSize, nameFilter, CurrentUserId)
            });
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpPost]
        [Route("create")]
        public IHttpActionResult Create(ClientResponseModel model)
        {
            var result = ServiceHost.GetService<IClientService>().AddClient(model, CurrentUserId);
            if (result.IsOk)
            {
                return Ok();
            }
            ModelState.AddModelError("", result.Message);
            return BadRequest(ModelState);
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpGet]
        [Route("edit")]
        public IHttpActionResult Edit(Guid clientId)
        {
            var clientService = ServiceHost.GetService<IClientService>();
            var model = clientService.GetClient(clientId);
            if (model.CreatedById != CurrentUserId)
            {
                return BadRequest("У вас нет прав на редактирование");
            }
            return Ok(model);
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpPut]
        [Route("edit")]
        public IHttpActionResult Edit(ClientResponseModel model)
        {
            if (model.CreatedById != CurrentUserId)
            {
                ModelState.AddModelError("", @"У вас нет прав на редактирование.");
            }
            var result = ServiceHost.GetService<IClientService>().UpdateClient(model);
            if (result.IsOk)
            {
                return Ok();
            }
            ModelState.AddModelError("", result.Message);
            return BadRequest(ModelState);
        }

        [HttpDelete]
        [Route("delete")]
        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        public IHttpActionResult Delete(Guid clientId)
        {
            var result = ServiceHost.GetService<IClientService>().DeleteClient(clientId, CurrentUserId);
            return Json(new
            {
                Success = result.IsOk,
                result.Message
            });
        }
    }
}