﻿using System;
using System.Web.Http;
using GamePortal.Domain.Contracts;
using GamePortal.Domain.Contracts.Services;
using GamePortal.Domain.Enums;
using GamePortal.Domain.Models.DTO.ResponseModels;
using GamePortal.Domain.Models.DTO.ViewModels;

namespace GamePortal.WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/tag")]
    public class TagController : BaseWebApiController
    {
        public TagController(IServiceHost serviceHost) : base(serviceHost)
        {
        }

        [HttpGet]
        [Route("paginated")]
        public IHttpActionResult GetPaginated(PaginationRequestModel model)
        {
            return Json(new
            {
                Data = ServiceHost.GetService<ITagService>().GetTagsPagination(model.Page, model.PageSize, model.NameFilter, model.SelectedTags)
            });
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpPost]
        [Route("create")]
        public IHttpActionResult Create(TagResponseModel model)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var result = ServiceHost.GetService<ITagService>().AddTag(model);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpGet]
        [Route("edit")]
        public IHttpActionResult Edit(Guid tagId)
        {
            var tagService = ServiceHost.GetService<ITagService>();
            var model = tagService.GetTag(tagId);
            return Json(new
            {
                data = model
            });
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpPut]
        [Route("edit")]
        public IHttpActionResult Update(TagResponseModel model)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var result = ServiceHost.GetService<ITagService>().UpdateTag(model);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [HttpDelete]
        [Route("delete")]
        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        public IHttpActionResult Delete(Guid tagId)
        {
            var result = ServiceHost.GetService<ITagService>().DeleteTag(tagId);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }
    }
}