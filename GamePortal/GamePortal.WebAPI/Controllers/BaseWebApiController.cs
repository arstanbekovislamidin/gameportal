﻿using System;
using System.Net.Http;
using System.Web.Http;
using GamePortal.Domain.Contracts;
using GamePortal.Domain.Models.Identity;
using GamePortal.Logic.Services.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace GamePortal.WebAPI.Controllers
{
    [Authorize]
    public class BaseWebApiController : ApiController
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;
        protected readonly IServiceHost ServiceHost;
        protected Guid CurrentUserId => Guid.Parse(User.Identity.GetUserId());
        protected User CurrentUser => UserManager.GetUserWithIncludes(CurrentUserId);

        protected ApplicationSignInManager SignInManager
        {
            get => _signInManager ?? Request.GetOwinContext().Get<ApplicationSignInManager>();
            private set => _signInManager = value;
        }

        protected ApplicationUserManager UserManager
        {
            get => _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            private set => _userManager = value;
        }

        protected ApplicationRoleManager RoleManager
        {
            get => _roleManager ?? Request.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            private set => _roleManager = value;
        }

        public BaseWebApiController(IServiceHost serviceHost)
        {
            ServiceHost = serviceHost;
        }
    }
}