﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using GamePortal.Common;
using GamePortal.Domain.Contracts;
using GamePortal.Domain.Contracts.Services;
using GamePortal.Domain.Enums;
using GamePortal.Domain.Models.DTO.ResponseModels;
using GamePortal.Domain.Models.DTO.ViewModels;

namespace GamePortal.WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/news")]
    public class NewsController : BaseWebApiController
    {
        public NewsController(IServiceHost serviceHost) : base(serviceHost)
        {
        }

        [HttpGet]
        [Route("paginated")]
        public IHttpActionResult GetPaginated(PaginationRequestModel model)
        {
            var service = ServiceHost.GetService<INewsService>();
            var result = service.GetNewsWebApiPagination(model.Page, model.PageSize, model.NameFilter, model.SelectedTags);
            return Json(new
            {
                data = result
            });
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpPost]
        [Route("create")]
        public IHttpActionResult Create(NewsResponseModel model, HttpPostedFileBase file)
        {
            var service = ServiceHost.GetService<INewsService>();
            if (file == null || file.ContentLength == 0)
            {
                return Json(new
                {
                    success = false,
                    message = "Выберите файл для заставки"
                });
            }
            if (!FileHelper.IsImage(file.ContentType))
            {
                return Json(new
                {
                    success = false,
                    message = "Неверный формат файла. Требуется изображение."
                });
            }
            var result = service.AddNews(model, file, CurrentUserId);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [HttpGet]
        [Route("news")]
        public IHttpActionResult News(Guid newsId)
        {
            var service = ServiceHost.GetService<INewsService>();
            var model = service.GetNewsWithUser(newsId, CurrentUser);
            return Json(new
            {
                data = model
            });
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpGet]
        [Route("edit")]
        public IHttpActionResult Edit(Guid newsId)
        {
            var service = ServiceHost.GetService<INewsService>();
            var model = service.GetNews(newsId);
            return Json(new
            {
                data = model
            });
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpPut]
        [Route("edit")]
        public IHttpActionResult Update(NewsResponseModel model, HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0 && !FileHelper.IsImage(file.ContentType))
            {
                return Json(new
                {
                    success = false,
                    message = "Неверный формат файла. Требуется изображение."
                });
            }
            var service = ServiceHost.GetService<INewsService>();
            var result = service.UpdateNews(model, file);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [HttpDelete]
        [Route("delete")]
        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        public IHttpActionResult Delete(Guid newsId)
        {
            var service = ServiceHost.GetService<INewsService>();
            var result = service.DeleteNews(newsId);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [Route("upload")]
        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpPost]
        public IHttpActionResult UploadFile(HttpPostedFileBase file)
        {
            if (file.ContentLength == 0)
                return Json(new
                {
                    success = false,
                    message = "Выберите файл."
                });
            try
            {
                var result = ServiceHost.GetService<IFileService>().UploadNewsFile(file);
                return Json(new
                {
                    success = true,
                    data = result
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    success = false,
                    error = e
                });
            }
        }

        [Route("addComment")]
        [HttpPost]
        public IHttpActionResult AddComment(string text, Guid newsId, Guid? replyId)
        {
            var service = ServiceHost.GetService<INewsService>();
            var result = service.AddComment(text, newsId, replyId, CurrentUserId);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [Route("vote")]
        [HttpPost]
        public IHttpActionResult Vote(Guid newsId, ScoreType score)
        {
            var service = ServiceHost.GetService<INewsService>();
            var result = service.VoteNews(newsId, score, CurrentUserId);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [Route("voteComment")]
        [HttpPost]
        public IHttpActionResult VoteComment(Guid commentId, ScoreType score)
        {
            var service = ServiceHost.GetService<INewsService>();
            var result = service.VoteComment(commentId, score, CurrentUserId);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [Route("~/news/image")]
        [HttpGet]
        public HttpResponseMessage Image(Guid fileId)
        {
            var service = ServiceHost.GetService<IFileService>();
            var path = service.GetNewsFilePath(fileId);
            var file = File.ReadAllBytes(path);
            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(file)
            };
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("application/octet-stream");
            return result;
        }
    }
}