﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using GamePortal.Common;
using GamePortal.Domain.Contracts;
using GamePortal.Domain.Contracts.Services;
using GamePortal.Domain.Enums;
using GamePortal.Domain.Models.DTO.ResponseModels;
using GamePortal.Domain.Models.DTO.ViewModels;

namespace GamePortal.WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/game")]
    public class GameController : BaseWebApiController
    {
        public GameController(IServiceHost serviceHost) : base(serviceHost)
        {
        }

        [HttpGet]
        [Route("paginated")]
        public IHttpActionResult GetPaginated(PaginationRequestModel model)
        {
            var service = ServiceHost.GetService<IGameService>();
            var result = service.GetGamesWebApiPagination(model?.Page ?? 1, model?.PageSize ?? 20, model?.NameFilter ?? string.Empty, model?.SelectedGenres, model?.SelectedTags);
            return Json(new
            {
                data = result
            });
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpPost]
        [Route("create")]
        public IHttpActionResult Create(GameResponseModel model, HttpPostedFileBase file)
        {
            var service = ServiceHost.GetService<IGameService>();
            if (file == null || file.ContentLength == 0)
            {
                return Json(new
                {
                    success = false,
                    message = "Выберите картинку для заставки."
                });
            }
            if (!FileHelper.IsImage(file.ContentType))
            {
                return Json(new
                {
                    success = false,
                    message = "Неверный формат файла. Требуется изображение."
                });
            }
            var result = service.AddGame(model, file, CurrentUserId);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [HttpGet]
        [Route("game")]
        public IHttpActionResult Game(Guid gameId)
        {
            var service = ServiceHost.GetService<IGameService>();
            var model = service.GetGameWithUser(gameId, CurrentUser);
            return Json(new
            {
                data = model
            });
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpGet]
        [Route("edit")]
        public IHttpActionResult Edit(Guid gameId)
        {
            var service = ServiceHost.GetService<IGameService>();
            var model = service.GetGame(gameId);
            return Json(new
            {
                data = model
            });
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpPut]
        [Route("edit")]
        public IHttpActionResult Update(GameResponseModel model, HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0 && !FileHelper.IsImage(file.ContentType))
            {
                return Json(new
                {
                    success = false,
                    message = "Неверный формат файла. Требуется изображение."
                });
            }
            var service = ServiceHost.GetService<IGameService>();
            var result = service.UpdateGame(model, file);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [HttpDelete]
        [Route("delete")]
        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        public IHttpActionResult Delete(Guid gameId)
        {
            var service = ServiceHost.GetService<IGameService>();
            var result = service.DeleteGame(gameId);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [Route("upload")]
        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpPost]
        public IHttpActionResult UploadFile(HttpPostedFileBase file)
        {
            if (file.ContentLength == 0)
                return Json(new
                {
                    success = false,
                    message = "Выберите файл."
                });
            try
            {
                var result = ServiceHost.GetService<IFileService>().UploadGameFile(file);
                return Json(new
                {
                    success = true,
                    data = result
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    success = false,
                    error = e
                });
            }
        }

        [Route("addComment")]
        [HttpPost]
        public IHttpActionResult AddComment(string text, Guid gameId, Guid? replyId)
        {
            var service = ServiceHost.GetService<IGameService>();
            var result = service.AddComment(text, gameId, replyId, CurrentUserId);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [Route("vote")]
        [HttpPost]
        public IHttpActionResult Vote(Guid gameId, ScoreType score)
        {
            var service = ServiceHost.GetService<IGameService>();
            var result = service.VoteGame(gameId, score, CurrentUserId);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [Route("voteComment")]
        [HttpPost]
        public IHttpActionResult VoteComment(Guid commentId, ScoreType score)
        {
            var service = ServiceHost.GetService<IGameService>();
            var result = service.VoteComment(commentId, score, CurrentUserId);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [Route("~/game/image")]
        [HttpGet]
        public HttpResponseMessage Image(Guid fileId)
        {
            var service = ServiceHost.GetService<IFileService>();
            var path = service.GetGameFilePath(fileId);
            var file = File.ReadAllBytes(path);
            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(file)
            };
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue(FileHelper.GetMymeType(path));
            return result;
        }
    }
}