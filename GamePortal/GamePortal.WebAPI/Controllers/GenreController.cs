﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using GamePortal.Common;
using GamePortal.Domain.Contracts;
using GamePortal.Domain.Contracts.Services;
using GamePortal.Domain.Enums;
using GamePortal.Domain.Models.DTO.ResponseModels;
using GamePortal.Domain.Models.DTO.ViewModels;

namespace GamePortal.WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/genre")]
    public class GenreController : BaseWebApiController
    {
        public GenreController(IServiceHost serviceHost) : base(serviceHost)
        {
        }

        [HttpGet]
        [Route("paginated")]
        public IHttpActionResult GetPaginated(PaginationRequestModel model)
        {
            var result = ServiceHost.GetService<IGenreService>()
                .GetGenresPagination(model.Page, model.PageSize, model.NameFilter, model.SelectedGenres);
            return Json(new
            {
                data = result
            });
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpPost]
        [Route("create")]
        public IHttpActionResult Create(GenreResponseModel model, HttpPostedFileBase file)
        {
            if (file == null || file.ContentLength == 0)
            {
                return Json(new
                {
                    success = false,
                    message = "Выберите файл для заставки"
                });
            }
            if (!FileHelper.IsImage(file.ContentType))
            {
                return Json(new
                {
                    success = false,
                    message = "Неверный формат файла. Требуется изображение."
                });
            }
            var result = ServiceHost.GetService<IGenreService>().AddGenre(model, file);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }
        
        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpGet]
        [Route("edit")]
        public IHttpActionResult Edit(Guid genreId)
        {
            var genreService = ServiceHost.GetService<IGenreService>();
            var model = genreService.GetGenre(genreId);
            return Json(new
            {
                data = model
            });
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpPut]
        [Route("edit")]
        public IHttpActionResult Update(GenreResponseModel model, HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0 && !FileHelper.IsImage(file.ContentType))
            {
                return Json(new
                {
                    success = false,
                    message = "Неверный формат файла. Требуется изображение."
                });
            }
            var result = ServiceHost.GetService<IGenreService>().UpdateGenre(model, file);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [HttpDelete]
        [Route("delete")]
        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        public IHttpActionResult Delete(Guid genreId)
        {
            var result = ServiceHost.GetService<IGenreService>().DeleteGenre(genreId);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [Route("~/genre/image")]
        [HttpGet]
        public HttpResponseMessage Image(Guid fileId)
        {
            var service = ServiceHost.GetService<IFileService>();
            var path = service.GetGenreFilePath(fileId);
            var file = File.ReadAllBytes(path);
            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(file)
            };
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("application/octet-stream");
            return result;
        }
    }
}