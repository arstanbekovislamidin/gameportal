﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(GamePortal.WebAPI.Startup))]

namespace GamePortal.WebAPI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
