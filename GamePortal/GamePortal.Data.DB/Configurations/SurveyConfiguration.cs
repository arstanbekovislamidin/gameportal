﻿using System.Data.Entity.ModelConfiguration;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Data.DB.Configurations
{
    public class SurveyConfiguration : EntityTypeConfiguration<Survey>
    {
        public SurveyConfiguration()
        {
            HasKey(x => x.Id);

            Property(x => x.Name).HasMaxLength(100);
            Property(x => x.Description).HasMaxLength(300);
            Property(x => x.Status).IsRequired();

            HasRequired(x => x.CreatedByUser).WithMany().HasForeignKey(x => x.CreatedByUserId);

            ToTable("Survey");
        }
    }
}
