﻿using System.Data.Entity.ModelConfiguration;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Data.DB.Configurations
{
    public class QuestionConfiguration : EntityTypeConfiguration<Question>
    {
        public QuestionConfiguration()
        {
            HasKey(x => x.Id);
            Property(x => x.Text).HasMaxLength(200);

            HasRequired(x => x.Survey).WithMany(x => x.Questions).HasForeignKey(x => x.Survey_Id);

            ToTable("Questions");
        }
    }
}
