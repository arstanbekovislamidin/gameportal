﻿using System.Data.Entity.ModelConfiguration;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Data.DB.Configurations
{
    public class ClientConfiguration : EntityTypeConfiguration<Client>
    {
        public ClientConfiguration()
        {
            HasKey(x => x.Id);
            Property(x => x.Secret).HasMaxLength(100).IsRequired();
            Property(x => x.Name).HasMaxLength(100).IsRequired();
            Property(x => x.RedirectUrl).HasMaxLength(100).IsRequired();
            Property(x => x.WebSiteUrl).HasMaxLength(100).IsRequired();
            HasRequired(x => x.CreatedBy).WithMany(x => x.Clients).HasForeignKey(x => x.CreatedById);

            ToTable("Clients");
        }
    }
}
