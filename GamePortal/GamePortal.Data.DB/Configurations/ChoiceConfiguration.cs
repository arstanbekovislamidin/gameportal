﻿using System.Data.Entity.ModelConfiguration;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Data.DB.Configurations
{
    public class ChoiceConfiguration : EntityTypeConfiguration<Choice>
    {
        public ChoiceConfiguration()
        {
            HasKey(x => x.Id);
            Property(x => x.CreateDateTime);

            HasRequired(x => x.CreatedByUser).WithMany(x => x.Choices).HasForeignKey(x => x.CreatedByUserId);
            HasRequired(x => x.Answer).WithMany(x => x.Choices).WillCascadeOnDelete();

            ToTable("Choices");
        }
    }
}
