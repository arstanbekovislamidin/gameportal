﻿using System.Data.Entity.ModelConfiguration;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Data.DB.Configurations
{
    public class AnswerConfiguration : EntityTypeConfiguration<Answer>
    {
        public AnswerConfiguration()
        {
            HasKey(x => x.Id);
            Property(x => x.Text).HasMaxLength(100);

            HasRequired(x => x.Question).WithMany(x => x.Answers).WillCascadeOnDelete();

            ToTable("Answers");
        }
    }
}
