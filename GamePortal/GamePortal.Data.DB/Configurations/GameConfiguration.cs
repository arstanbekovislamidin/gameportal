﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Data.DB.Configurations
{
    public class GameConfiguration : EntityTypeConfiguration<Game>
    {
        public GameConfiguration()
        {
            HasKey(x => x.Id);
            Property(x => x.Name).HasMaxLength(100).HasColumnAnnotation(IndexAnnotation.AnnotationName,
                new IndexAnnotation(new IndexAttribute("Unique Name", 1) { IsUnique = true })).IsRequired();
            Property(x => x.Description).IsRequired().IsMaxLength();
            Property(x => x.CreateDateTime);
            Property(x => x.LastUpdateDateTime);
            
            HasRequired(x => x.CreatedByUser).WithMany().HasForeignKey(x => x.CreatedByUserId);
            HasRequired(x => x.HeaderFile).WithMany().HasForeignKey(x => x.HeaderFileId);
            HasMany(x => x.Files).WithMany(x => x.Games);
            HasMany(x => x.Tags).WithMany(x => x.Games);
            HasMany(x => x.Genres).WithMany(x => x.Games);
            HasMany(x => x.Comments).WithOptional(x => x.Game);
            HasMany(x => x.Votes).WithOptional(x => x.Game);

            ToTable("Games");
        }
    }
}
