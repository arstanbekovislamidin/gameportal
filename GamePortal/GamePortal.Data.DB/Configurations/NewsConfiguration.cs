﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Data.DB.Configurations
{
    public class NewsConfiguration : EntityTypeConfiguration<News>
    {
        public NewsConfiguration()
        {
            HasKey(x => x.Id);
            Property(x => x.ShortDescription).HasMaxLength(300).IsRequired();
            Property(x => x.Description).IsMaxLength().IsRequired();
            Property(x => x.Title).HasMaxLength(100).HasColumnAnnotation(IndexAnnotation.AnnotationName,
                new IndexAnnotation(new IndexAttribute("Unique Title", 1) { IsUnique = true })).IsRequired();
            Property(x => x.CreateDateTime);
            Property(x => x.LastUpdateDateTime);

            HasRequired(x => x.CreatedByUser).WithMany().HasForeignKey(x => x.CreatedByUserId);
            HasRequired(x => x.HeaderFile).WithMany().HasForeignKey(x => x.HeaderFileId);
            HasMany(x => x.Files).WithMany(x => x.News);
            HasMany(x => x.Tags).WithMany(x => x.News);
            HasMany(x => x.Comments).WithOptional(x => x.News);
            HasMany(x => x.Votes).WithOptional(x => x.News);

            ToTable("News");
        }
    }
}
