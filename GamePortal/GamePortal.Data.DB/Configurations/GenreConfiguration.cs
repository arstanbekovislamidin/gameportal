﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Data.DB.Configurations
{
    public class GenreConfiguration : EntityTypeConfiguration<Genre>
    {
        public GenreConfiguration()
        {
            HasKey(x => x.Id);
            Property(x => x.Name).HasMaxLength(100).IsRequired();
            HasRequired(x => x.HeaderFile).WithMany().HasForeignKey(x => x.HeaderFileId);
            Property(x => x.Name)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName,
                    new IndexAnnotation(new IndexAttribute("UQ_Genres_Name", 1) { IsUnique = true }));

            ToTable("Genres");
        }
    }
}
