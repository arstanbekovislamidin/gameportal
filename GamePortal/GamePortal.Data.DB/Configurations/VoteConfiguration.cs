﻿using System.Data.Entity.ModelConfiguration;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Data.DB.Configurations
{
    public class VoteConfiguration : EntityTypeConfiguration<Vote>
    {
        public VoteConfiguration()
        {
            HasKey(x => x.Id);
            Property(x => x.Score).IsRequired();
            Property(x => x.CreateDateTime);

            HasRequired(x => x.CreatedByUser).WithMany(x => x.Votes).HasForeignKey(x => x.CreatedByUserId);
            HasOptional(x => x.Comment);
            HasOptional(x => x.Game);
            HasOptional(x => x.News);

            ToTable("Votes");
        }
    }
}
