﻿using System.ComponentModel.DataAnnotations.Schema;
using GamePortal.Domain.Models.Identity;
using System.Data.Entity.ModelConfiguration;

namespace GamePortal.Data.DB.Configurations
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            HasKey(x => x.Id);
            Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.FirstName).HasMaxLength(250).IsRequired();
            Property(x => x.LastName).HasMaxLength(250).IsRequired();
            Property(x => x.BirthDate).HasColumnType("datetime2");
            Property(x => x.RegisterDateTime).HasColumnType("datetime2");
            HasRequired(x => x.HeaderFile).WithMany().HasForeignKey(x => x.HeaderFileId);
            ToTable("Users");
        }
    }
}
