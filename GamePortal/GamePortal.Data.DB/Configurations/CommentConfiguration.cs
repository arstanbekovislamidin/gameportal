﻿using System.Data.Entity.ModelConfiguration;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Data.DB.Configurations
{
    public class CommentConfiguration : EntityTypeConfiguration<Comment>
    {
        public CommentConfiguration()
        {
            HasKey(x => x.Id);
            Property(x => x.Text).HasMaxLength(1000).IsRequired();
            Property(x => x.CreateDateTime);
            Property(x => x.LastUpdateDateTime);

            HasRequired(x => x.CreatedByUser).WithMany(x => x.Comments).HasForeignKey(x => x.CreatedByUserId);
            HasOptional(x => x.CommentReply);
            HasMany(x => x.Votes).WithOptional(x => x.Comment);

            ToTable("Comments");
        }
    }
}
