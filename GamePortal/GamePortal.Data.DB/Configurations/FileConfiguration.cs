﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Data.DB.Configurations
{
    public class FileConfiguration : EntityTypeConfiguration<File>
    {
        public FileConfiguration()
        {
            HasKey(x => x.Id);
            Property(x => x.Name).HasMaxLength(100).IsRequired();
            Property(x => x.Name)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName,
                    new IndexAnnotation(new IndexAttribute("UQ_Files_Name", 1) { IsUnique = true }));

            ToTable("Files");
        }
    }
}
