﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using GamePortal.Data.DB.Configurations;
using GamePortal.Domain.Models.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace GamePortal.Data.DB
{
    public class EntityContext : IdentityDbContext<User, Role, Guid, UserLogin, UserRole, UserClaim>
    {
        public EntityContext()
            : base("DefaultConnection")
        {
            Configuration.LazyLoadingEnabled = true;
        }

        public static EntityContext Create()
        {
            return new EntityContext();
        }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Configurations.Add(new UserConfiguration());
            builder.Configurations.Add(new RoleConfiguration());
            builder.Configurations.Add(new FileConfiguration());
            builder.Configurations.Add(new NewsConfiguration());
            builder.Configurations.Add(new TagConfiguration());
            builder.Configurations.Add(new GameConfiguration());
            builder.Configurations.Add(new GenreConfiguration());
            builder.Configurations.Add(new CommentConfiguration());
            builder.Configurations.Add(new VoteConfiguration());
            builder.Configurations.Add(new ChoiceConfiguration());
            builder.Configurations.Add(new AnswerConfiguration());
            builder.Configurations.Add(new QuestionConfiguration());
            builder.Configurations.Add(new SurveyConfiguration());
            builder.Configurations.Add(new ClientConfiguration());

            builder.Entity<UserRole>().ToTable("UserRoles");
            builder.Entity<UserLogin>().ToTable("UserLogins");
            builder.Entity<UserClaim>().ToTable("UserClaims");

            builder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
}
