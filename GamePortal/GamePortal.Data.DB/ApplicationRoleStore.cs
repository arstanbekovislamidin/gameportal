﻿using System;
using System.Data.Entity;
using GamePortal.Domain.Models.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace GamePortal.Data.DB
{
    public class ApplicationRoleStore : RoleStore<Role, Guid, UserRole>
    {
        public ApplicationRoleStore(DbContext context) : base(context)
        {
        }
    }
}
