﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GamePortal.Domain.Contracts.GenericRepository;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Data.DB
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : BaseEntity<Guid>
    {
        private readonly DbContext _entities;

        private DbSet<TEntity> EntityDbSet => _entities.Set<TEntity>();
        
        public GenericRepository(DbContext context)
        {
            _entities = context;
        }

        public virtual IQueryable<TEntity> GetAll()
        {
            return EntityDbSet;
        }

        public bool Any(Expression<Func<TEntity, bool>> predicate)
        {
            return EntityDbSet.Any(predicate);
        }

        public IQueryable<TEntity> FindBy(Expression<Func<TEntity, bool>> predicate)
        {
            return EntityDbSet.Where(predicate);
        }

        public TEntity FindById(Guid id)
        {
            return EntityDbSet.SingleOrDefault(x => x.Id == id);
        }

        public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return EntityDbSet.FirstOrDefault(predicate);
        }

        public async Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await EntityDbSet.FirstOrDefaultAsync(predicate);
        }

        public virtual TEntity Add(TEntity entity)
        {
            return EntityDbSet.Add(entity);
        }
        
        public virtual void AddRange(IEnumerable<TEntity> entities)
        {
            EntityDbSet.AddRange(entities);
        }

        public virtual void Delete(TEntity entity)
        {
            EntityDbSet.Remove(entity);
        }

        public virtual void DeleteRange(IEnumerable<TEntity> entities)
        {
            EntityDbSet.RemoveRange(entities);
        }

        public virtual TEntity Edit(TEntity entity)
        {
            _entities.Entry(entity).State = EntityState.Modified;
            return entity;
        }

        public virtual void EditRange(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                Edit(entity);
            }
        }

        public TEntity Attach(TEntity entity)
        {
            return EntityDbSet.Attach(entity);
        }
    }
}
