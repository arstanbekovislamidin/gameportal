﻿using System;
using System.Data.Entity;
using GamePortal.Domain.Models.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace GamePortal.Data.DB
{
    public class ApplicationUserStore : UserStore<User, Role, Guid, UserLogin, UserRole, UserClaim>
    {
        public ApplicationUserStore(DbContext context) : base(context)
        {
        }
    }
}
