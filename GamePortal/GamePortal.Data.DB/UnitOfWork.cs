﻿using System;
using System.Collections.Concurrent;
using System.Data.Entity.Validation;
using System.Text;
using GamePortal.Domain.Contracts.GenericRepository;
using GamePortal.Domain.Contracts.UnitOfWork;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Data.DB
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ConcurrentDictionary<Type, object> _repositories;
        private bool _disposed;

        public EntityContext Context { get; }

        public UnitOfWork(EntityContext context)
        {
            Context = context;
            _repositories = new ConcurrentDictionary<Type, object>();
            _disposed = false;
        }

        public IGenericRepository<TEntity> GetRepository<TEntity>() where TEntity : BaseEntity<Guid>
        {
            return _repositories.GetOrAdd(typeof(TEntity),
                x => new GenericRepository<TEntity>(Context)) as IGenericRepository<TEntity>;
        }

        public void SaveChanges()
        {
            try
            {
                Context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                var sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    sb.AppendLine($"Entity of type \"{eve.Entry.Entity.GetType().Name}\" in state \"{eve.Entry.State}\" has the following validation errors:");
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine($"- Property: \"{ve.PropertyName}\", Error: \"{ve.ErrorMessage}\"");
                    }
                }
                throw new Exception(sb.ToString());
            }
            catch (Exception e)
            {
                throw e.GetBaseException();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (_disposed) return;

            if (disposing)
            {
                Context.Dispose();
            }
            _disposed = true;
        }
    }
}
