namespace GamePortal.Data.DB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedClients : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Secret = c.String(nullable: false, maxLength: 100),
                        Name = c.String(nullable: false, maxLength: 100),
                        WebSiteUrl = c.String(nullable: false, maxLength: 100),
                        RedirectUrl = c.String(nullable: false, maxLength: 100),
                        CreatedById = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreatedById)
                .Index(t => t.CreatedById);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Clients", "CreatedById", "dbo.Users");
            DropIndex("dbo.Clients", new[] { "CreatedById" });
            DropTable("dbo.Clients");
        }
    }
}
