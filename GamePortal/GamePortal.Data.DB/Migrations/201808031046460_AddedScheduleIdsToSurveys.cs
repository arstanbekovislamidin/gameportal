namespace GamePortal.Data.DB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedScheduleIdsToSurveys : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Survey", "StartJobId", c => c.String());
            AddColumn("dbo.Survey", "EndJobId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Survey", "EndJobId");
            DropColumn("dbo.Survey", "StartJobId");
        }
    }
}
