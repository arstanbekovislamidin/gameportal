namespace GamePortal.Data.DB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedPathFromFile : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Files", "UQ_Files_Name_Path");
            RenameColumn(table: "dbo.Games", name: "HeaderFile_Id", newName: "HeaderFileId");
            RenameColumn(table: "dbo.News", name: "HeaderFile_Id", newName: "HeaderFileId");
            RenameColumn(table: "dbo.Genres", name: "HeaderFile_Id", newName: "HeaderFileId");
            RenameIndex(table: "dbo.Games", name: "IX_HeaderFile_Id", newName: "IX_HeaderFileId");
            RenameIndex(table: "dbo.News", name: "IX_HeaderFile_Id", newName: "IX_HeaderFileId");
            RenameIndex(table: "dbo.Genres", name: "IX_HeaderFile_Id", newName: "IX_HeaderFileId");
            CreateIndex("dbo.Files", "Name", unique: true, name: "UQ_Files_Name");
            DropColumn("dbo.Files", "Path");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Files", "Path", c => c.String(nullable: false, maxLength: 200));
            DropIndex("dbo.Files", "UQ_Files_Name");
            RenameIndex(table: "dbo.Genres", name: "IX_HeaderFileId", newName: "IX_HeaderFile_Id");
            RenameIndex(table: "dbo.News", name: "IX_HeaderFileId", newName: "IX_HeaderFile_Id");
            RenameIndex(table: "dbo.Games", name: "IX_HeaderFileId", newName: "IX_HeaderFile_Id");
            RenameColumn(table: "dbo.Genres", name: "HeaderFileId", newName: "HeaderFile_Id");
            RenameColumn(table: "dbo.News", name: "HeaderFileId", newName: "HeaderFile_Id");
            RenameColumn(table: "dbo.Games", name: "HeaderFileId", newName: "HeaderFile_Id");
            CreateIndex("dbo.Files", new[] { "Name", "Path" }, unique: true, name: "UQ_Files_Name_Path");
        }
    }
}
