namespace GamePortal.Data.DB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixUserHeaderFile : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Users", new[] { "HeaderFile_Id" });
            RenameColumn(table: "dbo.Users", name: "HeaderFile_Id", newName: "HeaderFileId");
            AlterColumn("dbo.Users", "HeaderFileId", c => c.Guid(nullable: false));
            CreateIndex("dbo.Users", "HeaderFileId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Users", new[] { "HeaderFileId" });
            AlterColumn("dbo.Users", "HeaderFileId", c => c.Guid());
            RenameColumn(table: "dbo.Users", name: "HeaderFileId", newName: "HeaderFile_Id");
            CreateIndex("dbo.Users", "HeaderFile_Id");
        }
    }
}
