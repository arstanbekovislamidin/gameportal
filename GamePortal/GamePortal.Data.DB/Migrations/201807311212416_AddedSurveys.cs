namespace GamePortal.Data.DB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedSurveys : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Comments", name: "CreatedByUser_Id", newName: "CreatedByUserId");
            RenameColumn(table: "dbo.Votes", name: "CreatedByUser_Id", newName: "CreatedByUserId");
            RenameColumn(table: "dbo.Games", name: "CreatedByUser_Id", newName: "CreatedByUserId");
            RenameColumn(table: "dbo.News", name: "CreatedByUser_Id", newName: "CreatedByUserId");
            RenameIndex(table: "dbo.Comments", name: "IX_CreatedByUser_Id", newName: "IX_CreatedByUserId");
            RenameIndex(table: "dbo.Games", name: "IX_CreatedByUser_Id", newName: "IX_CreatedByUserId");
            RenameIndex(table: "dbo.News", name: "IX_CreatedByUser_Id", newName: "IX_CreatedByUserId");
            RenameIndex(table: "dbo.Votes", name: "IX_CreatedByUser_Id", newName: "IX_CreatedByUserId");
            CreateTable(
                "dbo.Choices",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        CreateDateTime = c.DateTime(nullable: false),
                        CreatedByUserId = c.Guid(nullable: false),
                        Answer_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Answers", t => t.Answer_Id)
                .ForeignKey("dbo.Users", t => t.CreatedByUserId)
                .Index(t => t.CreatedByUserId)
                .Index(t => t.Answer_Id);
            
            CreateTable(
                "dbo.Answers",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Text = c.String(maxLength: 100),
                        Question_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Questions", t => t.Question_Id)
                .Index(t => t.Question_Id);
            
            CreateTable(
                "dbo.Questions",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Text = c.String(maxLength: 200),
                        Survey_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Survey", t => t.Survey_Id)
                .Index(t => t.Survey_Id);
            
            CreateTable(
                "dbo.Survey",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(maxLength: 100),
                        Description = c.String(maxLength: 300),
                        CreatedDateTime = c.DateTime(nullable: false),
                        StartDateTime = c.DateTime(nullable: false),
                        EndDateTime = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        CreatedByUserId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreatedByUserId)
                .Index(t => t.CreatedByUserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Choices", "CreatedByUserId", "dbo.Users");
            DropForeignKey("dbo.Choices", "Answer_Id", "dbo.Answers");
            DropForeignKey("dbo.Answers", "Question_Id", "dbo.Questions");
            DropForeignKey("dbo.Questions", "Survey_Id", "dbo.Survey");
            DropForeignKey("dbo.Survey", "CreatedByUserId", "dbo.Users");
            DropIndex("dbo.Survey", new[] { "CreatedByUserId" });
            DropIndex("dbo.Questions", new[] { "Survey_Id" });
            DropIndex("dbo.Answers", new[] { "Question_Id" });
            DropIndex("dbo.Choices", new[] { "Answer_Id" });
            DropIndex("dbo.Choices", new[] { "CreatedByUserId" });
            DropTable("dbo.Survey");
            DropTable("dbo.Questions");
            DropTable("dbo.Answers");
            DropTable("dbo.Choices");
            RenameIndex(table: "dbo.Votes", name: "IX_CreatedByUserId", newName: "IX_CreatedByUser_Id");
            RenameIndex(table: "dbo.News", name: "IX_CreatedByUserId", newName: "IX_CreatedByUser_Id");
            RenameIndex(table: "dbo.Games", name: "IX_CreatedByUserId", newName: "IX_CreatedByUser_Id");
            RenameIndex(table: "dbo.Comments", name: "IX_CreatedByUserId", newName: "IX_CreatedByUser_Id");
            RenameColumn(table: "dbo.News", name: "CreatedByUserId", newName: "CreatedByUser_Id");
            RenameColumn(table: "dbo.Games", name: "CreatedByUserId", newName: "CreatedByUser_Id");
            RenameColumn(table: "dbo.Votes", name: "CreatedByUserId", newName: "CreatedByUser_Id");
            RenameColumn(table: "dbo.Comments", name: "CreatedByUserId", newName: "CreatedByUser_Id");
        }
    }
}
