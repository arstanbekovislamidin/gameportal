namespace GamePortal.Data.DB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(maxLength: 13),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserRoles",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                        RoleId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.Roles", t => t.RoleId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 250),
                        LastName = c.String(nullable: false, maxLength: 250),
                        BirthDate = c.DateTime(precision: 7, storeType: "datetime2"),
                        RegisterDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        BanEndDate = c.DateTime(),
                        Email = c.String(),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(),
                        HeaderFile_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Files", t => t.HeaderFile_Id)
                .Index(t => t.HeaderFile_Id);
            
            CreateTable(
                "dbo.UserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Text = c.String(nullable: false, maxLength: 1000),
                        CreateDateTime = c.DateTime(nullable: false),
                        LastUpdateDateTime = c.DateTime(nullable: false),
                        CommentReply_Id = c.Guid(),
                        CreatedByUser_Id = c.Guid(nullable: false),
                        Game_Id = c.Guid(),
                        News_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Comments", t => t.CommentReply_Id)
                .ForeignKey("dbo.Users", t => t.CreatedByUser_Id)
                .ForeignKey("dbo.Games", t => t.Game_Id)
                .ForeignKey("dbo.News", t => t.News_Id)
                .Index(t => t.CommentReply_Id)
                .Index(t => t.CreatedByUser_Id)
                .Index(t => t.Game_Id)
                .Index(t => t.News_Id);
            
            CreateTable(
                "dbo.Games",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Description = c.String(nullable: false),
                        CreateDateTime = c.DateTime(nullable: false),
                        LastUpdateDateTime = c.DateTime(nullable: false),
                        CreatedByUser_Id = c.Guid(nullable: false),
                        HeaderFile_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreatedByUser_Id)
                .ForeignKey("dbo.Files", t => t.HeaderFile_Id)
                .Index(t => t.Name, unique: true, name: "Unique Name")
                .Index(t => t.CreatedByUser_Id)
                .Index(t => t.HeaderFile_Id);
            
            CreateTable(
                "dbo.Files",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Path = c.String(nullable: false, maxLength: 200),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.Name, t.Path }, unique: true, name: "UQ_Files_Name_Path");
            
            CreateTable(
                "dbo.News",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 100),
                        ShortDescription = c.String(nullable: false, maxLength: 300),
                        Description = c.String(nullable: false),
                        CreateDateTime = c.DateTime(nullable: false),
                        LastUpdateDateTime = c.DateTime(nullable: false),
                        CreatedByUser_Id = c.Guid(nullable: false),
                        HeaderFile_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreatedByUser_Id)
                .ForeignKey("dbo.Files", t => t.HeaderFile_Id)
                .Index(t => t.Title, unique: true, name: "Unique Title")
                .Index(t => t.CreatedByUser_Id)
                .Index(t => t.HeaderFile_Id);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "UQ_Tags_Name");
            
            CreateTable(
                "dbo.Votes",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Score = c.Int(nullable: false),
                        CreateDateTime = c.DateTime(nullable: false),
                        CreatedByUser_Id = c.Guid(nullable: false),
                        News_Id = c.Guid(),
                        Game_Id = c.Guid(),
                        Comment_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreatedByUser_Id)
                .ForeignKey("dbo.News", t => t.News_Id)
                .ForeignKey("dbo.Games", t => t.Game_Id)
                .ForeignKey("dbo.Comments", t => t.Comment_Id)
                .Index(t => t.CreatedByUser_Id)
                .Index(t => t.News_Id)
                .Index(t => t.Game_Id)
                .Index(t => t.Comment_Id);
            
            CreateTable(
                "dbo.Genres",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        HeaderFile_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Files", t => t.HeaderFile_Id)
                .Index(t => t.Name, unique: true, name: "UQ_Genres_Name")
                .Index(t => t.HeaderFile_Id);
            
            CreateTable(
                "dbo.UserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.NewsFiles",
                c => new
                    {
                        News_Id = c.Guid(nullable: false),
                        File_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.News_Id, t.File_Id })
                .ForeignKey("dbo.News", t => t.News_Id, cascadeDelete: true)
                .ForeignKey("dbo.Files", t => t.File_Id, cascadeDelete: true)
                .Index(t => t.News_Id)
                .Index(t => t.File_Id);
            
            CreateTable(
                "dbo.NewsTags",
                c => new
                    {
                        News_Id = c.Guid(nullable: false),
                        Tag_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.News_Id, t.Tag_Id })
                .ForeignKey("dbo.News", t => t.News_Id, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.Tag_Id, cascadeDelete: true)
                .Index(t => t.News_Id)
                .Index(t => t.Tag_Id);
            
            CreateTable(
                "dbo.GameFiles",
                c => new
                    {
                        Game_Id = c.Guid(nullable: false),
                        File_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Game_Id, t.File_Id })
                .ForeignKey("dbo.Games", t => t.Game_Id, cascadeDelete: true)
                .ForeignKey("dbo.Files", t => t.File_Id, cascadeDelete: true)
                .Index(t => t.Game_Id)
                .Index(t => t.File_Id);
            
            CreateTable(
                "dbo.GameGenres",
                c => new
                    {
                        Game_Id = c.Guid(nullable: false),
                        Genre_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Game_Id, t.Genre_Id })
                .ForeignKey("dbo.Games", t => t.Game_Id, cascadeDelete: true)
                .ForeignKey("dbo.Genres", t => t.Genre_Id, cascadeDelete: true)
                .Index(t => t.Game_Id)
                .Index(t => t.Genre_Id);
            
            CreateTable(
                "dbo.GameTags",
                c => new
                    {
                        Game_Id = c.Guid(nullable: false),
                        Tag_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Game_Id, t.Tag_Id })
                .ForeignKey("dbo.Games", t => t.Game_Id, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.Tag_Id, cascadeDelete: true)
                .Index(t => t.Game_Id)
                .Index(t => t.Tag_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserRoles", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserLogins", "UserId", "dbo.Users");
            DropForeignKey("dbo.Users", "HeaderFile_Id", "dbo.Files");
            DropForeignKey("dbo.Votes", "Comment_Id", "dbo.Comments");
            DropForeignKey("dbo.Votes", "Game_Id", "dbo.Games");
            DropForeignKey("dbo.GameTags", "Tag_Id", "dbo.Tags");
            DropForeignKey("dbo.GameTags", "Game_Id", "dbo.Games");
            DropForeignKey("dbo.Games", "HeaderFile_Id", "dbo.Files");
            DropForeignKey("dbo.GameGenres", "Genre_Id", "dbo.Genres");
            DropForeignKey("dbo.GameGenres", "Game_Id", "dbo.Games");
            DropForeignKey("dbo.Genres", "HeaderFile_Id", "dbo.Files");
            DropForeignKey("dbo.GameFiles", "File_Id", "dbo.Files");
            DropForeignKey("dbo.GameFiles", "Game_Id", "dbo.Games");
            DropForeignKey("dbo.Votes", "News_Id", "dbo.News");
            DropForeignKey("dbo.Votes", "CreatedByUser_Id", "dbo.Users");
            DropForeignKey("dbo.NewsTags", "Tag_Id", "dbo.Tags");
            DropForeignKey("dbo.NewsTags", "News_Id", "dbo.News");
            DropForeignKey("dbo.News", "HeaderFile_Id", "dbo.Files");
            DropForeignKey("dbo.NewsFiles", "File_Id", "dbo.Files");
            DropForeignKey("dbo.NewsFiles", "News_Id", "dbo.News");
            DropForeignKey("dbo.News", "CreatedByUser_Id", "dbo.Users");
            DropForeignKey("dbo.Comments", "News_Id", "dbo.News");
            DropForeignKey("dbo.Games", "CreatedByUser_Id", "dbo.Users");
            DropForeignKey("dbo.Comments", "Game_Id", "dbo.Games");
            DropForeignKey("dbo.Comments", "CreatedByUser_Id", "dbo.Users");
            DropForeignKey("dbo.Comments", "CommentReply_Id", "dbo.Comments");
            DropForeignKey("dbo.UserClaims", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserRoles", "RoleId", "dbo.Roles");
            DropIndex("dbo.GameTags", new[] { "Tag_Id" });
            DropIndex("dbo.GameTags", new[] { "Game_Id" });
            DropIndex("dbo.GameGenres", new[] { "Genre_Id" });
            DropIndex("dbo.GameGenres", new[] { "Game_Id" });
            DropIndex("dbo.GameFiles", new[] { "File_Id" });
            DropIndex("dbo.GameFiles", new[] { "Game_Id" });
            DropIndex("dbo.NewsTags", new[] { "Tag_Id" });
            DropIndex("dbo.NewsTags", new[] { "News_Id" });
            DropIndex("dbo.NewsFiles", new[] { "File_Id" });
            DropIndex("dbo.NewsFiles", new[] { "News_Id" });
            DropIndex("dbo.UserLogins", new[] { "UserId" });
            DropIndex("dbo.Genres", new[] { "HeaderFile_Id" });
            DropIndex("dbo.Genres", "UQ_Genres_Name");
            DropIndex("dbo.Votes", new[] { "Comment_Id" });
            DropIndex("dbo.Votes", new[] { "Game_Id" });
            DropIndex("dbo.Votes", new[] { "News_Id" });
            DropIndex("dbo.Votes", new[] { "CreatedByUser_Id" });
            DropIndex("dbo.Tags", "UQ_Tags_Name");
            DropIndex("dbo.News", new[] { "HeaderFile_Id" });
            DropIndex("dbo.News", new[] { "CreatedByUser_Id" });
            DropIndex("dbo.News", "Unique Title");
            DropIndex("dbo.Files", "UQ_Files_Name_Path");
            DropIndex("dbo.Games", new[] { "HeaderFile_Id" });
            DropIndex("dbo.Games", new[] { "CreatedByUser_Id" });
            DropIndex("dbo.Games", "Unique Name");
            DropIndex("dbo.Comments", new[] { "News_Id" });
            DropIndex("dbo.Comments", new[] { "Game_Id" });
            DropIndex("dbo.Comments", new[] { "CreatedByUser_Id" });
            DropIndex("dbo.Comments", new[] { "CommentReply_Id" });
            DropIndex("dbo.UserClaims", new[] { "UserId" });
            DropIndex("dbo.Users", new[] { "HeaderFile_Id" });
            DropIndex("dbo.UserRoles", new[] { "RoleId" });
            DropIndex("dbo.UserRoles", new[] { "UserId" });
            DropTable("dbo.GameTags");
            DropTable("dbo.GameGenres");
            DropTable("dbo.GameFiles");
            DropTable("dbo.NewsTags");
            DropTable("dbo.NewsFiles");
            DropTable("dbo.UserLogins");
            DropTable("dbo.Genres");
            DropTable("dbo.Votes");
            DropTable("dbo.Tags");
            DropTable("dbo.News");
            DropTable("dbo.Files");
            DropTable("dbo.Games");
            DropTable("dbo.Comments");
            DropTable("dbo.UserClaims");
            DropTable("dbo.Users");
            DropTable("dbo.UserRoles");
            DropTable("dbo.Roles");
        }
    }
}
