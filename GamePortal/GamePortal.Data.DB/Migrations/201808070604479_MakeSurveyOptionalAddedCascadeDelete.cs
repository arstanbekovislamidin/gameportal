namespace GamePortal.Data.DB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MakeSurveyOptionalAddedCascadeDelete : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Choices", "Answer_Id", "dbo.Answers");
            DropForeignKey("dbo.Answers", "Question_Id", "dbo.Questions");
            DropForeignKey("dbo.Questions", "Survey_Id", "dbo.Survey");
            DropIndex("dbo.Questions", new[] { "Survey_Id" });
            AlterColumn("dbo.Questions", "Survey_Id", c => c.Guid());
            CreateIndex("dbo.Questions", "Survey_Id");
            AddForeignKey("dbo.Choices", "Answer_Id", "dbo.Answers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Answers", "Question_Id", "dbo.Questions", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Questions", "Survey_Id", "dbo.Survey", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Questions", "Survey_Id", "dbo.Survey");
            DropForeignKey("dbo.Answers", "Question_Id", "dbo.Questions");
            DropForeignKey("dbo.Choices", "Answer_Id", "dbo.Answers");
            DropIndex("dbo.Questions", new[] { "Survey_Id" });
            AlterColumn("dbo.Questions", "Survey_Id", c => c.Guid(nullable: false));
            CreateIndex("dbo.Questions", "Survey_Id");
            AddForeignKey("dbo.Questions", "Survey_Id", "dbo.Survey", "Id");
            AddForeignKey("dbo.Answers", "Question_Id", "dbo.Questions", "Id");
            AddForeignKey("dbo.Choices", "Answer_Id", "dbo.Answers", "Id");
        }
    }
}
