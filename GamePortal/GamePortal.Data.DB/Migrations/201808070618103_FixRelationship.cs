namespace GamePortal.Data.DB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixRelationship : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Questions", "Survey_Id", "dbo.Survey");
            DropIndex("dbo.Questions", new[] { "Survey_Id" });
            AlterColumn("dbo.Questions", "Survey_Id", c => c.Guid(nullable: false));
            CreateIndex("dbo.Questions", "Survey_Id");
            AddForeignKey("dbo.Questions", "Survey_Id", "dbo.Survey", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Questions", "Survey_Id", "dbo.Survey");
            DropIndex("dbo.Questions", new[] { "Survey_Id" });
            AlterColumn("dbo.Questions", "Survey_Id", c => c.Guid());
            CreateIndex("dbo.Questions", "Survey_Id");
            AddForeignKey("dbo.Questions", "Survey_Id", "dbo.Survey", "Id", cascadeDelete: true);
        }
    }
}
