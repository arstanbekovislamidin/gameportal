﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using GamePortal.Common;
using GamePortal.Domain.Contracts;
using GamePortal.Domain.Contracts.Services;
using GamePortal.Domain.Enums;
using GamePortal.Domain.Models.DTO.ResponseModels;

namespace GamePortal.Web.Controllers
{
    public class NewsController : BaseSiteController
    {
        public NewsController(IServiceHost serviceHost) : base(serviceHost)
        {
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetPaginated(int page = 1, int pageSize = 20, string titleFilter = "", List<Guid> selectedTags = null)
        {
            var service = ServiceHost.GetService<INewsService>();
            var result = service.GetNewsPagination(page, pageSize, titleFilter, selectedTags);
            return new JsonResult
            {
                Data = result,
                MaxJsonLength = int.MaxValue
            };
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpGet]
        public ActionResult Create()
        {
            return View(new NewsResponseModel());
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(NewsResponseModel model, HttpPostedFileBase file)
        {
            var service = ServiceHost.GetService<INewsService>();
            if (file == null || file.ContentLength == 0)
            {
                return Json(new
                {
                    success = false,
                    message = "Выберите файл для заставки"
                });
            }
            if (!FileHelper.IsImage(file.ContentType))
            {
                return Json(new
                {
                    success = false,
                    message = "Неверный формат файла. Требуется изображение."
                });
            }
            var result = service.AddNews(model, file, CurrentUserId);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        public ActionResult News(Guid newsId)
        {
            var service = ServiceHost.GetService<INewsService>();
            var model = service.GetNewsWithUser(newsId, CurrentUser);
            return View(model);
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpGet]
        public ActionResult Edit(Guid newsId)
        {
            var service = ServiceHost.GetService<INewsService>();
            var model = service.GetNews(newsId);
            return View(model);
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(NewsResponseModel model, HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0 && !FileHelper.IsImage(file.ContentType))
            {
                return Json(new
                {
                    success = false,
                    message = "Неверный формат файла. Требуется изображение."
                });
            }
            var service = ServiceHost.GetService<INewsService>();
            var result = service.UpdateNews(model, file);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        public ActionResult Delete(Guid newsId)
        {
            var service = ServiceHost.GetService<INewsService>();
            var result = service.DeleteNews(newsId);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            }, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpPost]
        public JsonResult UploadFile(HttpPostedFileBase file)
        {
            if (file.ContentLength == 0)
                return Json(new
                {
                    error = "Выберите файл."
                }, JsonRequestBehavior.AllowGet);
            try
            {
                var result = ServiceHost.GetService<IFileService>().UploadNewsFiles(file);
                return Json(new
                {
                    success = true,
                    data = result
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    error = e
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult AddComment(string text, Guid newsId, Guid? replyId)
        {
            var service = ServiceHost.GetService<INewsService>();
            var result = service.AddComment(text, newsId, replyId, CurrentUserId);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [HttpPost]
        public JsonResult Vote(Guid newsId, ScoreType score)
        {
            var service = ServiceHost.GetService<INewsService>();
            var result = service.VoteNews(newsId, score, CurrentUserId);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [HttpPost]
        public JsonResult VoteComment(Guid commentId, ScoreType score)
        {
            var service = ServiceHost.GetService<INewsService>();
            var result = service.VoteComment(commentId, score, CurrentUserId);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }
        
        public ActionResult Image(Guid fileId)
        {
            var service = ServiceHost.GetService<IFileService>();
            var path = service.GetNewsFilePath(fileId);
            return File(path, FileHelper.GetMymeType(path));
        }
    }
}