﻿using System;
using System.Web.Mvc;
using GamePortal.Domain.Contracts;
using GamePortal.Domain.Contracts.Services;
using GamePortal.Domain.Enums;
using GamePortal.Domain.Models.DTO.ResponseModels;

namespace GamePortal.Web.Controllers
{
    public class ClientController : BaseSiteController
    {
        public ClientController(IServiceHost serviceHost) : base(serviceHost)
        {
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetClientsPaginated(int page = 1, int pageSize = 20, string nameFilter = "")
        {
            return Json(new
            {
                Data = ServiceHost.GetService<IClientService>().GetClientsPagination(page, pageSize, nameFilter, CurrentUserId)
            }, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpGet]
        public ActionResult Create()
        {
            return View(new ClientResponseModel());
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpPost]
        public ActionResult Create(ClientResponseModel model)
        {
            if (!ModelState.IsValid) return View(model);
            var result = ServiceHost.GetService<IClientService>().AddClient(model, CurrentUserId);
            if (result.IsOk)
            {
                return RedirectToAction("Index");
            }
            ModelState.AddModelError("", result.Message);
            return View(model);
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpGet]
        public ActionResult Edit(Guid clientId)
        {
            var clientService = ServiceHost.GetService<IClientService>();
            var model = clientService.GetClient(clientId);
            if (model.CreatedById != CurrentUserId)
            {
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpPost]
        public ActionResult Edit(ClientResponseModel model)
        {
            if (!ModelState.IsValid) return View(model);
            if (model.CreatedById != CurrentUserId)
            {
                ModelState.AddModelError("", @"У вас нет прав на редактирование.");
            }
            var result = ServiceHost.GetService<IClientService>().UpdateClient(model);
            if (result.IsOk)
            {
                return RedirectToAction("Index");
            }
            ModelState.AddModelError("", result.Message);
            return View(model);
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        public JsonResult Delete(Guid clientId)
        {
            var result = ServiceHost.GetService<IClientService>().DeleteClient(clientId, CurrentUserId);
            return Json(new
            {
                Success = result.IsOk,
                result.Message
            }, JsonRequestBehavior.AllowGet);
        }
    }
}