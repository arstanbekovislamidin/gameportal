﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using GamePortal.Domain.Contracts;
using GamePortal.Domain.Contracts.Services;
using GamePortal.Domain.Enums;
using GamePortal.Domain.Models.DTO.ResponseModels;

namespace GamePortal.Web.Controllers
{
    public class TagController : BaseSiteController
    {
        public TagController(IServiceHost serviceHost) : base(serviceHost)
        {
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetTagsPaginated(int page = 1, int pageSize = 20, string nameFilter = "", List<Guid> selectedTags = null)
        {
            return Json(new
            {
                Data = ServiceHost.GetService<ITagService>().GetTagsPagination(page, pageSize, nameFilter, selectedTags)
            }, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpGet]
        public ActionResult Create()
        {
            return View(new TagResponseModel());
        }
        
        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpPost]
        public ActionResult Create(TagResponseModel model)
        {
            if (!ModelState.IsValid) return View(model);
            var result = ServiceHost.GetService<ITagService>().AddTag(model);
            if (result.IsOk)
            {
                return RedirectToAction("Index");
            }
            ModelState.AddModelError("", result.Message);
            return View(model);
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpGet]
        public ActionResult Edit(Guid tagId)
        {
            var tagService = ServiceHost.GetService<ITagService>();
            var model = tagService.GetTag(tagId);
            return View(model);
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpPost]
        public ActionResult Edit(TagResponseModel model)
        {
            if (!ModelState.IsValid) return View(model);
            var result = ServiceHost.GetService<ITagService>().UpdateTag(model);
            if (result.IsOk)
            {
                return RedirectToAction("Index");
            }
            ModelState.AddModelError("", result.Message);
            return View(model);
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        public JsonResult Delete(Guid tagId)
        {
            var result = ServiceHost.GetService<ITagService>().DeleteTag(tagId);
            return Json(new
            {
                Success = result.IsOk,
                result.Message
            }, JsonRequestBehavior.AllowGet);
        }
    }
}