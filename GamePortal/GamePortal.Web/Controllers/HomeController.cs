﻿using System;
using System.Web.Mvc;
using GamePortal.Common;
using GamePortal.Domain.Contracts;
using GamePortal.Domain.Contracts.Services;

namespace GamePortal.Web.Controllers
{
    [Authorize]
    public class HomeController : BaseSiteController
    {
        public HomeController(IServiceHost serviceHost) : base(serviceHost)
        {
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}