﻿using System;
using System.Web;
using System.Web.Mvc;
using GamePortal.Domain.Contracts;
using GamePortal.Domain.Models.Identity;
using GamePortal.Logic.Services.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace GamePortal.Web.Controllers
{
    [Authorize]
    public class BaseSiteController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;
        protected readonly IServiceHost ServiceHost;
        protected Guid CurrentUserId => Guid.Parse(HttpContext.User.Identity.GetUserId());
        protected User CurrentUser => UserManager.GetUserWithIncludes(CurrentUserId);

        protected ApplicationSignInManager SignInManager
        {
            get => _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            private set => _signInManager = value;
        }

        protected ApplicationUserManager UserManager
        {
            get => _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            private set => _userManager = value;
        }

        protected ApplicationRoleManager RoleManager
        {
            get => _roleManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            private set => _roleManager = value;
        }

        public BaseSiteController(IServiceHost serviceHost)
        {
            ServiceHost = serviceHost;
        }
    }
}