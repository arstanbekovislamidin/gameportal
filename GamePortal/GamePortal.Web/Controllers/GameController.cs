﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using GamePortal.Common;
using GamePortal.Domain.Contracts;
using GamePortal.Domain.Contracts.Services;
using GamePortal.Domain.Enums;
using GamePortal.Domain.Models.DTO.ResponseModels;

namespace GamePortal.Web.Controllers
{
    public class GameController : BaseSiteController
    {
        public GameController(IServiceHost serviceHost) : base(serviceHost)
        {
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetPaginated(int page = 1, int pageSize = 20, string nameFilter = "",
            List<Guid> selectedGenres = null, List<Guid> selectedTags = null)
        {
            var service = ServiceHost.GetService<IGameService>();
            var result = service.GetGamesPagination(page, pageSize, nameFilter, selectedGenres, selectedTags);
            return new JsonResult
            {
                Data = result,
                MaxJsonLength = int.MaxValue
            };
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpGet]
        public ActionResult Create()
        {
            return View(new GameResponseModel());
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(GameResponseModel model, HttpPostedFileBase file)
        {
            var service = ServiceHost.GetService<IGameService>();
            if (file == null || file.ContentLength == 0)
            {
                return Json(new
                {
                    success = false,
                    message = "Выберите картинку для заставки."
                });
            }
            if (!FileHelper.IsImage(file.ContentType))
            {
                return Json(new
                {
                    success = false,
                    message = "Неверный формат файла. Требуется изображение."
                });
            }
            var result = service.AddGame(model, file, CurrentUserId);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        public ActionResult Game(Guid gameId)
        {
            var service = ServiceHost.GetService<IGameService>();
            var model = service.GetGameWithUser(gameId, CurrentUser);
            return View(model);
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpGet]
        public ActionResult Edit(Guid gameId)
        {
            var service = ServiceHost.GetService<IGameService>();
            var model = service.GetGame(gameId);
            return View(model);
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(GameResponseModel model, HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0 && !FileHelper.IsImage(file.ContentType))
            {
                return Json(new
                {
                    success = false,
                    message = "Неверный формат файла. Требуется изображение."
                });
            }
            var service = ServiceHost.GetService<IGameService>();
            var result = service.UpdateGame(model, file);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        public ActionResult Delete(Guid gameId)
        {
            var service = ServiceHost.GetService<IGameService>();
            var result = service.DeleteGame(gameId);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            }, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpPost]
        public JsonResult UploadFile(HttpPostedFileBase file)
        {
            if (file.ContentLength == 0)
                return Json(new
                {
                    error = "Выберите файл."
                }, JsonRequestBehavior.AllowGet);
            try
            {
                var result = ServiceHost.GetService<IFileService>().UploadGameFiles(file);
                return Json(new
                {
                    success = true,
                    data = result
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    error = e
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult AddComment(string text, Guid gameId, Guid? replyId)
        {
            var service = ServiceHost.GetService<IGameService>();
            var result = service.AddComment(text, gameId, replyId, CurrentUserId);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [HttpPost]
        public JsonResult Vote(Guid gameId, ScoreType score)
        {
            var service = ServiceHost.GetService<IGameService>();
            var result = service.VoteGame(gameId, score, CurrentUserId);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [HttpPost]
        public JsonResult VoteComment(Guid commentId, ScoreType score)
        {
            var service = ServiceHost.GetService<IGameService>();
            var result = service.VoteComment(commentId, score, CurrentUserId);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        public ActionResult Image(Guid fileId)
        {
            var service = ServiceHost.GetService<IFileService>();
            var path = service.GetGameFilePath(fileId);
            return File(path, FileHelper.GetMymeType(path));
        }
    }
}