﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using GamePortal.Common;
using GamePortal.Domain.Contracts;
using GamePortal.Domain.Contracts.Services;
using GamePortal.Domain.Enums;
using GamePortal.Domain.Models.DTO.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

namespace GamePortal.Web.Controllers
{
    public class ManageController : BaseSiteController
    {
        public ManageController(IServiceHost serviceHost) : base(serviceHost)
        {
        }

        //
        // GET: /Manage/Index
        public async Task<ActionResult> Index()
        {
            var model = new IndexViewModel
            {
                HasPassword = HasPassword(),
                BrowserRemembered = await AuthenticationManager.TwoFactorBrowserRememberedAsync(HttpContext.User.Identity.GetUserId())
            };
            return View(model);
        }

        // GET: /Manage/ChangePassword
        public ActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePasswordAsync(CurrentUserId, model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(CurrentUserId);
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, false, false);
                }
                return RedirectToAction("Index");
            }
            AddErrors(result);
            return View(model);
        }

        [Authorize(Roles = RoleNames.Administrator)]
        public ActionResult Users()
        {
            return View();
        }

        [Authorize(Roles = RoleNames.Administrator)]
        [HttpPost]
        public JsonResult GetPaginated(int page = 1, int pageSize = 20, string firstNameFilter = "", string lastNamefilter = "", string emailFilter = "", string userNameFilter = "")
        {
            return new JsonResult
            {
                Data = UserManager.GetUsers(page, pageSize, firstNameFilter, lastNamefilter, emailFilter, userNameFilter),
                MaxJsonLength = int.MaxValue
            };
        }

        [Authorize(Roles = RoleNames.Administrator)]
        [HttpGet]
        public ActionResult Create()
        {
            return View(new RegisterViewModel
            {
                RolesToChoose = RoleManager.Roles.Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Name
                }).ToList()
            });
        }

        [Authorize(Roles = RoleNames.Administrator)]
        [HttpPost]
        public async Task<ActionResult> Create(RegisterViewModel user, HttpPostedFileBase file)
        {
            if (file == null || file.ContentLength == 0)
            {
                ModelState.AddModelError("", @"Выберите картинку для заставки.");
                return View(user);
            }
            if (!FileHelper.IsImage(file.ContentType))
            {
                ModelState.AddModelError("", @"Неверный формат файла. Требуется изображение.");
                return View(user);
            }
            var service = ServiceHost.GetService<IFileService>();
            user.HeaderFileId = service.UploadUserFile(file);
            var createResult = await UserManager.CreateUserAsync(user);
            if (!createResult.Succeeded)
            {
                AddErrors(createResult);
                return View(user);
            }
            var createdUser = await UserManager.FindByNameAsync(user.UserName);
            var roles = RoleManager.Roles.Where(x => user.SelectedRoles.Contains(x.Name)).Select(x => x.Name).ToArray();
            var roleAddResult = UserManager.AddToRoles(createdUser.Id, roles);
            if (roleAddResult.Succeeded) return RedirectToAction("Users");
            AddErrors(roleAddResult);
            return View(user);
        }

        [Authorize(Roles = RoleNames.Administrator)]
        [HttpGet]
        public async Task<ActionResult> Edit(Guid userId)
        {
            var user = await UserManager.GetUserByIdAsync(userId);
            user.RolesToChoose = RoleManager.Roles.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Name
            }).ToList();
            user.SelectedRoles = UserManager.GetRoles(user.Id).ToList();
            return View(user);
        }

        [Authorize(Roles = RoleNames.Administrator)]
        [HttpPost]
        public async Task<ActionResult> Edit(UpdateUserViewModel user, HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                if (!FileHelper.IsImage(file.ContentType))
                {
                    ModelState.AddModelError("", @"Неверный формат файла. Требуется изображение.");
                    return View(user);
                }
                var service = ServiceHost.GetService<IFileService>();
                service.DeleteUserFile(user.HeaderFile.Name);
                user.HeaderFileId = service.UploadUserFile(file);
            }
            var result = await UserManager.UpdateUserAsync(user);
            if (!result.Succeeded)
            {
                AddErrors(result);
                return View(user);
            }
            var userRoles = await UserManager.GetRolesAsync(user.Id);
            var removeRolesResult = UserManager.RemoveFromRoles(user.Id, userRoles.ToArray());
            if (!removeRolesResult.Succeeded)
            {
                AddErrors(removeRolesResult);
                return View(user);
            }
            var roles = RoleManager.Roles.Where(x => user.SelectedRoles.Contains(x.Name)).Select(x => x.Name).ToArray();
            var roleAddResult = UserManager.AddToRoles(user.Id, roles);
            if (!roleAddResult.Succeeded)
            {
                AddErrors(roleAddResult);
                return View(user);
            }
            return RedirectToAction("Users");
        }

        [Authorize(Roles = RoleNames.Administrator)]
        public async Task<JsonResult> Delete(Guid userId)
        {
            if (userId == CurrentUserId)
                return Json(new
                {
                    success = false,
                    message = "Вы не можете удалить себя"
                }, JsonRequestBehavior.AllowGet);
            var result = await UserManager.DeleteAsync(await UserManager.FindByIdAsync(userId));
            return Json(new
            {
                success = result.Succeeded,
                message = string.Join("<br>", result.Errors)
            }, JsonRequestBehavior.AllowGet);
        }

        #region Helpers

        private IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(CurrentUserId);
            return user?.PasswordHash != null;
        }
        #endregion
    }
}