﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using GamePortal.Common;
using GamePortal.Domain.Contracts;
using GamePortal.Domain.Contracts.Services;
using GamePortal.Domain.Enums;
using GamePortal.Domain.Models.DTO.ResponseModels;

namespace GamePortal.Web.Controllers
{
    public class GenreController : BaseSiteController
    {
        public GenreController(IServiceHost serviceHost) : base(serviceHost)
        {
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetPaginated(int page = 1, int pageSize = 20, string nameFilter = "", List<Guid> selectedGenres = null)
        {
            var result = ServiceHost.GetService<IGenreService>()
                .GetGenresPagination(page, pageSize, nameFilter, selectedGenres);
            return new JsonResult
            {
                Data = result,
                MaxJsonLength = int.MaxValue
            };
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpGet]
        public ActionResult Create()
        {
            return View(new GenreResponseModel());
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpPost]
        public JsonResult Create(GenreResponseModel model, HttpPostedFileBase file)
        {
            if (file == null || file.ContentLength == 0)
            {
                return Json(new
                {
                    success = false,
                    message = "Выберите файл для заставки"
                });
            }
            if (!FileHelper.IsImage(file.ContentType))
            {
                return Json(new
                {
                    success = false,
                    message = "Неверный формат файла. Требуется изображение."
                });
            }
            var result = ServiceHost.GetService<IGenreService>().AddGenre(model, file);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpGet]
        public ActionResult Edit(Guid genreId)
        {
            var genreService = ServiceHost.GetService<IGenreService>();
            var model = genreService.GetGenre(genreId);
            return View(model);
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpPost]
        public JsonResult Edit(GenreResponseModel model, HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0 && !FileHelper.IsImage(file.ContentType))
            {
                return Json(new
                {
                    success = false,
                    message = "Неверный формат файла. Требуется изображение."
                });
            }
            var result = ServiceHost.GetService<IGenreService>().UpdateGenre(model, file);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        public JsonResult Delete(Guid genreId)
        {
            var result = ServiceHost.GetService<IGenreService>().DeleteGenre(genreId);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            }, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult Image(Guid fileId)
        {
            var service = ServiceHost.GetService<IFileService>();
            var path = service.GetGenreFilePath(fileId);
            return File(path, FileHelper.GetMymeType(path));
        }
    }
}