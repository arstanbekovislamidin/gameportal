﻿using System;
using System.Web.Mvc;
using GamePortal.Domain.Contracts;
using GamePortal.Domain.Contracts.Services;
using GamePortal.Domain.Enums;
using GamePortal.Domain.Models.DTO.ResponseModels;

namespace GamePortal.Web.Controllers
{
    public class SurveyController : BaseSiteController
    {
        public SurveyController(IServiceHost serviceHost) : base(serviceHost)
        {
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetPaginated(int page = 1, int pageSize = 20, string nameFilter = "")
        {
            var service = ServiceHost.GetService<ISurveyService>();
            var result = service.GetSurveysPagination(page, pageSize, nameFilter, User.IsInRole(RoleNames.Manager) || User.IsInRole(RoleNames.Administrator));
            return new JsonResult
            {
                Data = result,
                MaxJsonLength = int.MaxValue
            };
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpGet]
        public ActionResult Create()
        {
            return View(new SurveyResponseModel());
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(SurveyResponseModel model)
        {
            var service = ServiceHost.GetService<ISurveyService>();
            var result = service.AddSurvey(model, CurrentUserId);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        public ActionResult Survey(Guid surveyId)
        {
            var service = ServiceHost.GetService<ISurveyService>();
            var model = service.GetSurveyWithUser(surveyId, CurrentUserId);
            if (model.Status != SurveyStatus.Started)
            {
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpGet]
        public ActionResult Edit(Guid surveyId)
        {
            var service = ServiceHost.GetService<ISurveyService>();
            var model = service.GetSurvey(surveyId);
            return View(model);
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(SurveyResponseModel model)
        {
            var service = ServiceHost.GetService<ISurveyService>();
            var result = service.UpdateSurvey(model);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }

        [Authorize(Roles = RoleNames.Administrator + "," + RoleNames.Manager)]
        public ActionResult Delete(Guid surveyId)
        {
            var service = ServiceHost.GetService<ISurveyService>();
            var result = service.DeleteSurvey(surveyId);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ChooseAnswer(Guid answerId)
        {
            var service = ServiceHost.GetService<ISurveyService>();
            var result = service.ChooseAnswer(answerId, CurrentUserId);
            return Json(new
            {
                success = result.IsOk,
                message = result.Message
            });
        }
    }
}