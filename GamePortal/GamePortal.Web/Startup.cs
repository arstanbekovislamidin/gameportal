﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GamePortal.Web.Startup))]
namespace GamePortal.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
