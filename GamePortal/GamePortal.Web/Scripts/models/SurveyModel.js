﻿function SurveyModel(model) {
    var self = this;
    self.Id = model ? model.Id : ko.observable('');
    self.Name = model ? model.Name : ko.observable('');
    self.Description = model ? model.Description : ko.observable('');
    self.Questions = model && model.Questions ? model.Questions : ko.observableArray([]);
    self.StartDateTime = model && model.StartDateTime ? model.StartDateTime : ko.observable('');
    self.EndDateTime = model && model.EndDateTime ? model.EndDateTime : ko.observable('');
}