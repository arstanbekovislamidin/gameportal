﻿function GameModel(model) {
    var self = this;
    self.Id = model ? model.Id : ko.observable('');
    self.Name = model ? model.Name : ko.observable('');
    self.Description = model ? model.Description : ko.observable('');
    self.HeaderFile = model ? model.HeaderFile : ko.observable({});
    self.Genres = model ? model.Genres : ko.observableArray([]);
    self.Tags = model ? model.Tags : ko.observableArray([]);
    self.Files = model && model.Files ? model.Files : ko.observableArray([]);
    self.Comments = model && model.Comments ? model.Comments : ko.observableArray([]);
    self.PositiveVotes = model && model.PositiveVotes ? model.PositiveVotes : ko.observable(0);
    self.NegativeVotes = model && model.NegativeVotes ? model.NegativeVotes : ko.observable(0);
    self.IsLiked = model && model.IsLiked ? model.IsLiked : ko.observable(false);
    self.IsUnliked = model && model.IsUnliked ? model.IsUnliked : ko.observable(false);
}