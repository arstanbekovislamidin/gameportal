﻿function GenreModel(model) {
    var self = this;
    self.Id = model ? model.Id : ko.observable('');
    self.Name = model ? model.Name : ko.observable('');
    self.HeaderFile = model ? model.HeaderFile : ko.observable({});
}