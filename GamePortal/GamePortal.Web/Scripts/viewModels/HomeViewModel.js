﻿function HomeVm() {
    var self = this;
    self.gamesList = ko.observableArray([]);
    self.newsList = ko.observableArray([]);
    self.LoadInformation = function () {
        $('.modal').modal('show');
        var param = {
            'page': 1,
            'pageSize': 5
        };
        $.ajax({
            contentType: 'application/json',
            type: 'POST',
            traditional: true,
            url: '/Game/GetPaginated',
            data: JSON.stringify(param),
            success: function (data) {
                self.gamesList.removeAll();
                data.Items.forEach(function (item) {
                    self.gamesList.push(item);
                });
                $('.modal').modal('hide');
            }
        });
        $.ajax({
            contentType: 'application/json',
            type: 'POST',
            traditional: true,
            url: '/News/GetPaginated',
            data: JSON.stringify(param),
            success: function (data) {
                self.newsList.removeAll();
                data.Items.forEach(function (item) {
                    self.newsList.push(item);
                });
                $('.modal').modal('hide');
            }
        });
    }
    self.LoadInformation();
}