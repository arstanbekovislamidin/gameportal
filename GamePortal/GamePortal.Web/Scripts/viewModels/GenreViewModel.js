﻿function GenreVm(model, isEdit) {
    var nameError = "Поле имя обязательно и должно быть меньше 100 символов в длину";
    var firstPaginationPageClick = true;
    var isSearch = false;
    var $pagination = $('#ulPagination');
    var tmpNameFilter = "";
    var self = this;

    self.model = ko.observable(model);
    self.headerFile = ko.observable(null);
    self.errors = ko.observableArray();
    self.genresList = window.ko.observableArray();
    self.nameFilter = ko.observable();
    self.currentPage = 1;
    self.pageSize = 20;
    self.totalPages = 1;

    self.OnFileSelected = function (vm, event) {
        if (event.target.files.length > 0) {
            self.headerFile(event.target.files.item(0));
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#previewHeaderFile').attr('src', e.target.result);
            }
            reader.readAsDataURL(event.target.files.item(0));
            $('#previewHeaderFile').show();
        } else {
            $('#previewHeaderFile').hide();
        }
    };

    self.Save = function () {
        if (self.headerFile() === null && self.model().HeaderFilePath() === null) {
            window.bootbox.alert("Выберите файл для заставки");
            return;
        }
        let indexNameError = self.errors.indexOf(nameError);
        if (self.model().Name == null || self.model().Name.length > 100) {
            if (indexNameError === -1) {
                self.errors.push(nameError);
            }
            return;
        } else {
            if (indexNameError !== -1) {
                self.errors.splice(indexNameError, 1);
            }
        }
        self.model(ko.mapping.toJS(self.model()));
        var formData = toFormData(self.model());
        if (self.headerFile() !== null) {
            formData.append('file', self.headerFile(), self.headerFile().name);
        }
        var dialog = window.bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Загрузка...</div>', closeButton: false });
        $.ajax({
            type: "POST",
            url: isEdit ? '/Genre/Edit' : '/Genre/Create',
            contentType: false,
            processData: false,
            data: formData,
            success: function (response) {
                dialog.modal('hide');
                if (response != null && response.success) {
                    window.bootbox.alert({ message: response.message, callback: function () { window.location.href = '/Genre/Index'; } });
                } else {
                    if (typeof response != "undefined" && response != null) {
                        window.bootbox.alert(
                            {
                                message: "<div style='max-height: 300px;overflow: auto;'>" + response.message + "</div>",
                                title: "Проверьте загружаемый файл и имя."
                            });
                    }
                }
            },
            error: function (xhr) {
                window.bootbox.alert(xhr.responseText);
            }
        });
    };

    self.LoadInformation = function (page, isDestroy) {
        $('.modal').modal('show');
        var param = {
            page: page,
            pageSize: self.pageSize,
            nameFilter: tmpNameFilter
        };
        $.ajax({
            type: 'POST',
            url: '/Genre/GetPaginated',
            data: param,
            success: function (data) {
                self.genresList.removeAll();
                data.Items.forEach(function (item) {
                    self.genresList.push(item);
                });
                if (isDestroy) $pagination.twbsPagination('destroy');
                self.currentPage = data.Page;
                self.totalPages = data.TotalPages;
                $('#divPagination').text('Страница ' + page + ' из ' + self.totalPages);
                $pagination.twbsPagination({
                    startPage: 1,
                    totalPages: self.totalPages,
                    first: 'Первая',
                    prev: 'Предыдущая',
                    next: 'Следующая',
                    last: 'Последняя',
                    onPageClick: function (evt, page) {
                        if (firstPaginationPageClick) {
                            firstPaginationPageClick = false;
                            isSearch = false;
                            return;
                        }
                        if (isSearch) {
                            isSearch = false;
                            return;
                        }
                        self.LoadInformation(page, false);
                    }
                });
                $('.modal').modal('hide');
            }
        });
    }

    self.EditGenre = function (genreId) {
        var url = "/Genre/Edit?genreId=" + genreId;
        window.location.href = url;
    }

    self.DeleteGenre = function (genreId) {
        window.bootbox.confirm("Вы действительно хотите удалить этот жанр?",
            function (result) {
                if (result) {
                    var url = "/Genre/Delete?genreId=" + genreId;
                    $.ajax({
                        url: url,
                        success: function (data) {
                            if (data.Success === true) {
                                window.bootbox.alert(data.Message,
                                    function () {
                                        window.location.reload();
                                    });
                            } else {
                                window.bootbox.alert(data.Text);
                            }
                        }
                    });
                }
            });
    }

    self.Search = function (page, isDestroy) {
        isSearch = true;
        tmpNameFilter = self.nameFilter();
        self.LoadInformation(page, isDestroy);
    }
}