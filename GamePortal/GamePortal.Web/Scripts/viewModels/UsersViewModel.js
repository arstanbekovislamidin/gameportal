﻿var UsersVm = function () {
    var firstPaginationPageClick = true;
    var isSearch = false;
    var $pagination = $('#ulPagination');
    var tmpFirstNameFilter = "";
    var tmpLastNameFilter = "";
    var tmpEmailFilter = "";
    var tmpUserNameFilter = "";
    var self = this;

    self.usersList = ko.observableArray([]);
    self.firstNameFilter = ko.observable();
    self.lastNameFilter = ko.observable();
    self.emailFilter = ko.observable();
    self.userNameFilter = ko.observable();
    self.currentPage = 1;
    self.pageSize = 20;
    self.totalPages = 1;

    self.LoadInformation = function (page, isDestroy) {
        $('.modal').modal('show');
        var param = {
            'page': page,
            'pageSize': self.pageSize,
            'firstNameFilter': tmpFirstNameFilter,
            'lastNameFilter': tmpLastNameFilter,
            'emailFilter': tmpEmailFilter,
            'userNameFilter': tmpUserNameFilter
        };
        $.ajax({
            contentType: 'application/json',
            type: 'POST',
            traditional: true,
            url: '/Manage/GetPaginated',
            data: JSON.stringify(param),
            success: function (data) {
                self.usersList.removeAll();
                data.Items.forEach(function (item) {
                    self.usersList.push(item);
                });
                if (isDestroy) $pagination.twbsPagination('destroy');
                self.currentPage = data.Page;
                self.totalPages = data.TotalPages;
                $('#divPagination').text('Страница ' + page + ' из ' + self.totalPages);
                $pagination.twbsPagination({
                    startPage: 1,
                    totalPages: self.totalPages,
                    first: 'Первая',
                    prev: 'Предыдущая',
                    next: 'Следующая',
                    last: 'Последняя',
                    onPageClick: function (evt, page) {
                        if (firstPaginationPageClick) {
                            firstPaginationPageClick = false;
                            isSearch = false;
                            return;
                        }
                        if (isSearch) {
                            isSearch = false;
                            return;
                        }
                        self.LoadInformation(page, false);
                    }
                });
                $('.modal').modal('hide');
            }
        });
    }

    self.EditUser = function (userId) {
        var url = "/Manage/Edit?userId=" + userId;
        window.location.href = url;
    }

    self.DeleteUser = function (userId) {
        window.bootbox.confirm("Вы действительно хотите удалить этого пользователя?",
            function (result) {
                if (result) {
                    var url = "/Manage/Delete?userId=" + userId;
                    $.ajax({
                        url: url,
                        success: function (data) {
                            if (data.success === true) {
                                window.location.reload();
                            } else {
                                window.bootbox.alert(data.message);
                            }
                        }
                    });
                }
            });
    }

    self.Search = function (page, isDestroy) {
        isSearch = true;
        tmpFirstNameFilter = self.firstNameFilter();
        tmpLastNameFilter = self.lastNameFilter();
        tmpEmailFilter = self.emailFilter();
        tmpUserNameFilter = self.userNameFilter();
        self.LoadInformation(page, isDestroy);
    }
}