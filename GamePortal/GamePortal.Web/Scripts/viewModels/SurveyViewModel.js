﻿function SurveyVm(model, isEdit) {
    var nameError = "Поле \"Название\" обязательно и должно быть меньше 100 символов в длину";
    var shortDescriptionError = "Поле \"Описание\" обязательно и должно быть меньше 300 символов в длину";
    var wrongDatesError = "Поле \"Конец конкурса\" не может быть раньше \"Начало конкурса\"";
    var firstPaginationPageClick = true;
    var isSearch = false;
    var $pagination = $('#ulPagination');
    var tmpNameFilter = "";
    var self = this;

    self.model = ko.observable(ko.mapping.fromJS(model));
    if (self.model() && self.model().StartDateTime) {
        var startDate = new Date(self.model().StartDateTime().match(/\d+/)[0] * 1);
        self.model().StartDateTime(startDate.toLocaleString('ru-Ru'));
    }
    if (self.model() && self.model().EndDateTime) {
        var endDate = new Date(self.model().EndDateTime().match(/\d+/)[0] * 1);
        self.model().EndDateTime(endDate.toLocaleString('ru-Ru'));
    }
    self.errors = ko.observableArray();
    self.surveysList = ko.observableArray([]);
    self.nameFilter = ko.observable();
    self.currentPage = 1;
    self.pageSize = 20;
    self.totalPages = 1;

    function Answer() {
        this.Text = ko.observable();
    }

    function Question() {
        this.Text = ko.observable();
        this.Answers = ko.observableArray();
    }

    self.AddQuestion = function () {
        self.model().Questions.push(new Question());
    }

    self.RemoveQuestion = function (index) {
        self.model().Questions.splice(index, 1);
    }

    self.AddAnswer = function (index) {
        self.model().Questions()[index].Answers.push(new Answer());
    }

    self.RemoveAnswer = function (parent, index) {
        parent.Answers.splice(index, 1);
    }

    self.Save = function () {
        let indexNameError = self.errors.indexOf(nameError);
        let indexDescriptionError = self.errors.indexOf(shortDescriptionError);
        let indexWrongDatesError = self.errors.indexOf(wrongDatesError);
        if (self.model().Name == null || self.model().Name.length > 100) {
            if (indexNameError === -1) {
                self.errors.push(nameError);
            }
            window.scrollTo(0, 0);
            return;
        } else {
            if (indexNameError !== -1) {
                self.errors.splice(indexNameError, 1);
            }
        }
        if (self.model().Description == null || self.model().Description.length > 300) {
            if (indexDescriptionError === -1) {
                self.errors.push(shortDescriptionError);
            }
            window.scrollTo(0, 0);
            return;
        } else {
            if (indexDescriptionError !== -1) {
                self.errors.splice(indexDescriptionError, 1);
            }
        }
        if (new Date(self.model().StartDateTime) > new Date(self.model().EndDateTime())) {
            if (indexWrongDatesError === -1) {
                self.errors.push(wrongDatesError);
            }
            window.scrollTo(0, 0);
            return;
        } else {
            if (indexWrongDatesError !== -1) {
                self.errors.splice(indexWrongDatesError, 1);
            }
        }
        var dialog = window.bootbox.dialog({
            message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Загрузка...</div>',
            closeButton: false
        });
        var model = ko.toJS(self.model);
        $.ajax({
            type: 'POST',
            url: isEdit ? '/Survey/Edit' : '/Survey/Create',
            data: {
                'model': model
            },
            success: function (response) {
                dialog.modal('hide');
                if (response != null && response.success) {
                    window.bootbox.alert({
                        message: response.message,
                        callback: function () { window.location.href = '/Survey/Index'; }
                    });
                } else {
                    if (typeof response != "undefined" && response != null) {
                        window.bootbox.alert(
                            {
                                message: "<div style='max-height: 300px;overflow: auto;'>" +
                                    response.message +
                                    "</div>",
                                title: "Проверьте правильность вводимых данных."
                            });
                    }
                }
            },
            error: function (xhr) {
                dialog.modal('hide');
                window.bootbox.alert(xhr.responseText);
            }
        });
    };

    self.LoadInformation = function (page, isDestroy) {
        $('.modal').modal('show');
        var param = {
            'page': page,
            'pageSize': self.pageSize,
            'nameFilter': tmpNameFilter
        };
        $.ajax({
            contentType: 'application/json',
            type: 'POST',
            traditional: true,
            url: '/Survey/GetPaginated',
            data: JSON.stringify(param),
            success: function (data) {
                self.surveysList.removeAll();
                data.Items.forEach(function (item) {
                    self.surveysList.push(item);
                });
                if (isDestroy) $pagination.twbsPagination('destroy');
                self.currentPage = data.Page;
                self.totalPages = data.TotalPages;
                $('#divPagination').text('Страница ' + page + ' из ' + self.totalPages);
                $pagination.twbsPagination({
                    startPage: 1,
                    totalPages: self.totalPages,
                    first: 'Первая',
                    prev: 'Предыдущая',
                    next: 'Следующая',
                    last: 'Последняя',
                    onPageClick: function (evt, page) {
                        if (firstPaginationPageClick) {
                            firstPaginationPageClick = false;
                            isSearch = false;
                            return;
                        }
                        if (isSearch) {
                            isSearch = false;
                            return;
                        }
                        self.LoadInformation(page, false);
                    }
                });
                $('.modal').modal('hide');
            }
        });
    }

    self.OpenSurvey = function (surveyId) {
        window.location.href = "/Survey/Survey?surveyId=" + surveyId;
    }

    self.EditSurvey = function (surveyId) {
        window.location.href = "/Survey/Edit?surveyId=" + surveyId;;
    }

    self.DeleteSurvey = function (surveyId) {
        window.bootbox.confirm("Вы действительно хотите удалить эту игру?",
            function (result) {
                if (result) {
                    $.ajax({
                        url: "/Survey/Delete?surveyId=" + surveyId,
                        success: function (data) {
                            if (data.Success === true) {
                                window.bootbox.alert(data.Message,
                                    function () {
                                        window.location.reload();
                                    });
                            } else {
                                window.bootbox.alert(data.Text);
                            }
                        }
                    });
                }
            });
    }

    self.Search = function (page, isDestroy) {
        isSearch = true;
        tmpNameFilter = self.nameFilter();
        self.LoadInformation(page, isDestroy);
    }

    self.ChooseAnswer = function (question, answer) {
        var data = {
            'questionId': question.Id(),
            'answerId': answer.Id()
        };
        $.ajax({
            contentType: 'application/json',
            type: 'POST',
            traditional: true,
            url: '/Survey/ChooseAnswer',
            data: JSON.stringify(data),
            success: function (response) {
                if (response != null && response.success) {
                    if (answer.IsAnsweredByCurrentUser()) {
                        answer.IsAnsweredByCurrentUser(false);
                        answer.ChoicesCount(answer.ChoicesCount() - 1);
                    } else {
                        question.Answers().forEach(a => {
                            if (a.IsAnsweredByCurrentUser()) {
                                a.IsAnsweredByCurrentUser(false);
                                a.ChoicesCount(a.ChoicesCount() - 1);
                            };
                        });
                        answer.IsAnsweredByCurrentUser(true);
                        answer.ChoicesCount(answer.ChoicesCount() + 1);
                    }
                }
            },
            error: function (xhr) {
                window.bootbox.alert(xhr.responseText);
            }
        });
    }
}