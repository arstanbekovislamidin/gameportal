﻿function GameVm(model, isEdit) {
    var firstPaginationPageClick = true;
    var isSearch = false;
    var $pagination = $('#ulPagination');
    var tmpNameFilter = "";
    var self = this;

    self.headerFile = ko.observable(null);
    self.model = ko.observable(model);
    self.commentText = ko.observable('');
    self.replyToComment = ko.observable(false);
    self.replyName = ko.observable('');
    self.replyId = ko.observable();
    self.gamesList = ko.observableArray([]);
    self.nameFilter = ko.observable();
    self.selectedGenres = ko.observableArray([]);
    self.selectedTags = ko.observableArray([]);
    self.currentPage = 1;
    self.pageSize = 20;
    self.totalPages = 1;

    self.isGameLiked = ko.pureComputed(function () {
        return self.model().IsLiked() ? 'fas' : 'far';
    }, self);
    self.isGameUnliked = ko.pureComputed(function () {
        return self.model().IsUnliked() ? 'fas' : 'far';
    }, self);


    self.OnHeaderFileSelected = function (vm, event) {
        if (event.target.files.length > 0) {
            self.headerFile(event.target.files.item(0));
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#previewHeaderFile').attr('src', e.target.result);
            }
            reader.readAsDataURL(event.target.files.item(0));
            $('#previewHeaderFile').show();
        } else {
            self.headerFile(null);
            $('#previewHeaderFile').hide();
        }
    };

    self.Save = function () {
        if (self.headerFile() === null && self.model().HeaderFile().Path === null) {
            window.bootbox.alert("Выберите файл для заставки");
            return;
        }
        var dialog = window.bootbox.dialog({
            message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Загрузка...</div>',
            closeButton: false
        });
        var model = ko.mapping.toJS(self.model());
        var formData = toFormData(model);
        formData.delete("Description");
        formData.append("Description", tinyMCE.get('Description').getContent());
        if (self.headerFile() != null) {
            formData.append("file", self.headerFile(), self.headerFile().name);
        }
        $.ajax({
            type: 'POST',
            url: isEdit ? '/Game/Edit' : '/Game/Create',
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                dialog.modal('hide');
                if (response != null && response.success) {
                    window.bootbox.alert({
                        message: response.message,
                        callback: function () { window.location.href = '/Game/Index'; }
                    });
                } else {
                    if (typeof response != "undefined" && response != null) {
                        window.bootbox.alert(
                            {
                                message: "<div style='max-height: 300px;overflow: auto;'>" +
                                    response.message +
                                    "</div>",
                                title: "Проверьте правильность вводимых данных."
                            });
                    }
                }
            },
            error: function (xhr) {
                window.bootbox.alert(xhr.responseText);
            }
        });
    };

    self.RemoveGenre = function (genre) {
        var index = self.model().Genres.indexOf(genre);
        if (index !== -1) {
            self.model().Genres.splice(index, 1);
        }
    };

    self.RemoveTag = function (tag) {
        var index = self.model().Tags.indexOf(tag);
        if (index !== -1) {
            self.model().Tags.splice(index, 1);
        }
    };

    self.Vote = function (score) {
        var data = {
            'gameId': self.model().Id(),
            'score': score
        }
        $.ajax({
            contentType: 'application/json',
            type: 'POST',
            traditional: true,
            url: '/Game/Vote',
            data: JSON.stringify(data),
            success: function (response) {
                if (response != null && response.success) {
                    if (self.model().IsLiked()) {
                        if (score === 1) {
                            self.model().PositiveVotes(self.model().PositiveVotes() - 1);
                            self.model().IsLiked(false);
                        } else {
                            self.model().PositiveVotes(self.model().PositiveVotes() - 1);
                            self.model().NegativeVotes(self.model().NegativeVotes() + 1);
                            self.model().IsLiked(false);
                            self.model().IsUnliked(true);
                        }
                    } else if (self.model().IsUnliked()) {
                        if (score === -1) {
                            self.model().NegativeVotes(self.model().NegativeVotes() - 1);
                            self.model().IsUnliked(false);
                        } else {
                            self.model().PositiveVotes(self.model().PositiveVotes() + 1);
                            self.model().NegativeVotes(self.model().NegativeVotes() - 1);
                            self.model().IsLiked(true);
                            self.model().IsUnliked(false);
                        }
                    } else {
                        if (score === 1) {
                            self.model().PositiveVotes(self.model().PositiveVotes() + 1);
                            self.model().IsLiked(true);
                        } else {
                            self.model().NegativeVotes(self.model().NegativeVotes() + 1);
                            self.model().IsUnliked(true);
                        }
                    }
                }
            },
            error: function (xhr) {
                window.bootbox.alert(xhr.responseText);
            }
        });
    };

    self.VoteComment = function (comment, score) {
        var data = {
            'commentId': comment.Id(),
            'score': score
        }
        $.ajax({
            contentType: 'application/json',
            type: 'POST',
            traditional: true,
            url: '/Game/VoteComment',
            data: JSON.stringify(data),
            success: function (response) {
                if (response != null && response.success) {
                    if (comment.IsLiked()) {
                        if (score === 1) {
                            comment.PositiveVotes(comment.PositiveVotes() - 1);
                            comment.IsLiked(false);
                        } else {
                            comment.PositiveVotes(comment.PositiveVotes() - 1);
                            comment.NegativeVotes(comment.NegativeVotes() + 1);
                            comment.IsLiked(false);
                            comment.IsUnliked(true);
                        }
                    } else if (comment.IsUnliked()) {
                        if (score === -1) {
                            comment.NegativeVotes(comment.NegativeVotes() - 1);
                            comment.IsUnliked(false);
                        } else {
                            comment.PositiveVotes(comment.PositiveVotes() + 1);
                            comment.NegativeVotes(comment.NegativeVotes() - 1);
                            comment.IsLiked(true);
                            comment.IsUnliked(false);
                        }
                    } else {
                        if (score === 1) {
                            comment.PositiveVotes(comment.PositiveVotes() + 1);
                            comment.IsLiked(true);
                        } else {
                            comment.NegativeVotes(comment.NegativeVotes() + 1);
                            comment.IsUnliked(true);
                        }
                    }
                }
            },
            error: function (xhr) {
                window.bootbox.alert(xhr.responseText);
            }
        });
    };

    self.SendComment = function () {
        var data = {
            'text': self.commentText(),
            'gameId': self.model().Id(),
            'replyId': self.replyId()
        }
        $.ajax({
            contentType: 'application/json',
            type: 'POST',
            traditional: true,
            url: '/Game/AddComment',
            data: JSON.stringify(data),
            success: function (response) {
                if (response != null && response.success) {
                    window.location.reload();
                } else if (response != null) {
                    window.bootbox.alert(response.message);
                }
            },
            error: function (xhr) {
                window.bootbox.alert(xhr.responseText);
            }
        });
    }

    self.ReplyToComment = function (reply) {
        self.replyToComment(true);
        self.replyName(reply.CreatedByUserName());
        self.replyId(reply.Id());
    }

    self.CancelReply = function () {
        self.replyToComment(false);
        self.replyName('');
        self.replyId(null);
    }

    self.LoadInformation = function (page, isDestroy) {
        $('.modal').modal('show');
        var param = {
            'page': page,
            'pageSize': self.pageSize,
            'nameFilter': tmpNameFilter,
            'selectedGenres': $.map(self.selectedGenres(),
                function (v) {
                    return v.Id;
                }),
            'selectedTags': $.map(self.selectedTags(),
                function (v) {
                    return v.Id;
                })
        };
        $.ajax({
            contentType: 'application/json',
            type: 'POST',
            traditional: true,
            url: '/Game/GetPaginated',
            data: JSON.stringify(param),
            success: function (data) {
                self.gamesList.removeAll();
                data.Items.forEach(function (item) {
                    self.gamesList.push(item);
                });
                if (isDestroy) $pagination.twbsPagination('destroy');
                self.currentPage = data.Page;
                self.totalPages = data.TotalPages;
                $('#divPagination').text('Страница ' + page + ' из ' + self.totalPages);
                $pagination.twbsPagination({
                    startPage: 1,
                    totalPages: self.totalPages,
                    first: 'Первая',
                    prev: 'Предыдущая',
                    next: 'Следующая',
                    last: 'Последняя',
                    onPageClick: function (evt, page) {
                        if (firstPaginationPageClick) {
                            firstPaginationPageClick = false;
                            isSearch = false;
                            return;
                        }
                        if (isSearch) {
                            isSearch = false;
                            return;
                        }
                        self.LoadInformation(page, false);
                    }
                });
                $('.modal').modal('hide');
            }
        });
    }

    self.OpenGame = function (gameId) {
        var url = "/Game/Game?gameId=" + gameId;
        window.location.href = url;
    }

    self.EditGame = function (gameId) {
        var url = "/Game/Edit?gameId=" + gameId;
        window.location.href = url;
    }

    self.DeleteGame = function (gameId) {
        window.bootbox.confirm("Вы действительно хотите удалить эту игру?",
            function (result) {
                if (result) {
                    var url = "/Game/Delete?gameId=" + gameId;
                    $.ajax({
                        url: url,
                        success: function (data) {
                            if (data.Success === true) {
                                window.bootbox.alert(data.Message,
                                    function () {
                                        window.location.reload();
                                    });
                            } else {
                                window.bootbox.alert(data.Text);
                            }
                        }
                    });
                }
            });
    }

    self.Search = function (page, isDestroy) {
        isSearch = true;
        tmpNameFilter = self.nameFilter();
        self.LoadInformation(page, isDestroy);
    }

    self.AddGenreToSearch = function (genre) {
        if (!self.selectedGenres().find(x => { return x.Id === genre.Id })) {
            self.selectedGenres.push(genre);
            self.Search(1, true);
        }
    }

    self.AddTagToSearch = function (tag) {
        if (!self.selectedTags().find(x => { return x.Id === tag.Id })) {
            self.selectedTags.push(tag);
            self.Search(1, true);
        }
    }

    self.RemoveGenreFromSearch = function (genre) {
        var index = self.selectedGenres.indexOf(genre);
        if (index !== -1) {
            self.selectedGenres.splice(index, 1);
            self.Search(1, true);
        }
    }

    self.RemoveTagFromSearch = function (tag) {
        var index = self.selectedTags.indexOf(tag);
        if (index !== -1) {
            self.selectedTags.splice(index, 1);
            self.Search(1, true);
        }
    }


    $('#textarea-autocomplete-to-genres input')
        .on("keydown",
            function (event) {
                if (event.keyCode === $.ui.keyCode.TAB &&
                    $(this).autocomplete("instance").menu.active) {
                    event.preventDefault();
                }
            })
        .autocomplete({
            source: function (request, response) {
                var data =
                    {
                        'NameFilter': request.term,
                        'SelectedGenres': $.map(self.model().Genres(),
                            function (v) {
                                return v.Id;
                            })
                    }
                    ;
                $.ajax({
                    contentType: 'application/json',
                    type: 'POST',
                    traditional: true,
                    url: '/Genre/GetPaginated',
                    data: ko.toJSON(data),
                    success: function (data) {
                        response($.map(data.Items,
                            function (item) {
                                return {
                                    plink: item.Id,
                                    label: item.Name
                                }
                            }));
                    }
                });
            },
            focus: function () {
                return false;
            },
            select: function (event, ui) {
                this.value = '';
                self.model().Genres.push({
                    Id: ui.item.plink,
                    Name: ui.item.value
                });
                return false;
            }
        });

    $('#textarea-autocomplete-to-genres').click(function () {
        $('#textarea-autocomplete-to-genres input').focus();
    });

    $('#textarea-autocomplete-to-tags').on('click',
        'button',
        function (e) {
            e.preventDefault();
            var tag = self.model().Tags().find(function (t) {
                if (t.Id === this.name) {
                    return t;
                }
                return null;
            });
            var index = self.model().Tags().indexOf(tag);
            self.model().Tags().splice(index, 1);
            $(this).remove();
            return false;
        });

    $('#textarea-autocomplete-to-tags input')
        .on("keydown",
            function (event) {
                if (event.keyCode === $.ui.keyCode.TAB &&
                    $(this).autocomplete("instance").menu.active) {
                    event.preventDefault();
                }
            })
        .autocomplete({
            source: function (request, response) {
                var data =
                    {
                        'NameFilter': request.term,
                        'SelectedTags': $.map(self.model().Tags(),
                            function (v) {
                                return v.Id;
                            })
                    }
                    ;
                $.ajax({
                    contentType: 'application/json',
                    type: 'POST',
                    traditional: true,
                    url: '/Tag/GetTagsPaginated',
                    data: ko.toJSON(data),
                    success: function (data) {
                        response($.map(data.Data.Items,
                            function (item) {
                                return {
                                    plink: item.Id,
                                    label: item.Name
                                }
                            }));
                    }
                });
            },
            focus: function () {
                return false;
            },
            select: function (event, ui) {
                this.value = '';
                self.model().Tags.push({
                    Id: ui.item.plink,
                    Name: ui.item.value
                });
                return false;
            }
        });

    $('#textarea-autocomplete-to-tags').click(function () {
        $('#textarea-autocomplete-to-tags input').focus();
    });

    tinyMCE.init({
        selector: '#Description',
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern imagetools"
        ],
        toolbar1:
            "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        toolbar2: "print preview media | forecolor backcolor emoticons | ltr rtl",
        document_base_url: "http://localhost:55356",
        file_picker_callback: function (callback, value, meta) {
            if (meta.filetype === "media" || meta.filetype === "image") {
                jQuery("#game-files").trigger("click");
                $("#game-files").unbind('change');
                jQuery("#game-files").on("change",
                    function () {
                        var file = this.files[0];
                        var reader = new FileReader();
                        var fd = new FormData();
                        fd.append("file", file);
                        fd.append('filetype', meta.filetype);
                        var filePath = "";
                        jQuery.ajax({
                            url: '/Game/UploadFile',
                            type: "post",
                            data: fd,
                            contentType: false,
                            processData: false,
                            async: false,
                            success: function (response) {
                                if (response.success === true) {
                                    filePath = response.data.Url;
                                    response.data.Path = ko.observable(response.data.Path);
                                    self.model().Files.push(response.data);
                                }
                            }
                        });
                        reader.onload = function () {
                            callback(filePath);
                        };
                        reader.readAsDataURL(file);
                    });
            }
        }
    });
};