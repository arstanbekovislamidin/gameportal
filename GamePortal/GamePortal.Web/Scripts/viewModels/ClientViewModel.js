﻿var ClientVm = function () {
    var firstPaginationPageClick = true;
    var isSearch = false;
    var $pagination = $('#ulPagination');
    var tmpNameFilter = "";
    var self = this;

    self.clientsList = window.ko.observableArray();
    self.nameFilter = ko.observable();
    self.CurrentPage = 1;
    self.PageSize = 20;
    self.TotalPages = 1;

    self.LoadInformation = function (page, isDestroy) {
        $('.modal').modal('show');
        var param = {
            page: page,
            pageSize: self.PageSize,
            nameFilter: tmpNameFilter
        };
        $.ajax({
            type: 'POST',
            url: '/Client/GetClientsPaginated',
            data: param,
            success: function (data) {
                self.clientsList.removeAll();
                var response = data.Data;
                response.Items.forEach(function (item) {
                    self.clientsList.push(item);
                });
                if (isDestroy) $pagination.twbsPagination('destroy');
                self.CurrentPage = response.Page;
                self.TotalPages = response.TotalPages;
                $('#divPagination').text('Страница ' + page + ' из ' + self.TotalPages);
                $pagination.twbsPagination({
                    startPage: 1,
                    totalPages: self.TotalPages,
                    first: 'Первая',
                    prev: 'Предыдущая',
                    next: 'Следующая',
                    last: 'Последняя',
                    onPageClick: function (evt, page) {
                        if (firstPaginationPageClick) {
                            firstPaginationPageClick = false;
                            isSearch = false;
                            return;
                        }
                        if (isSearch) {
                            isSearch = false;
                            return;
                        }
                        self.LoadInformation(page, false);
                    }
                });
                $('.modal').modal('hide');
            }
        });
    }

    self.EditClient = function (clientId) {
        var url = "/Client/Edit?clientId=" + clientId;
        window.location.href = url;
    }

    self.DeleteClient = function (clientId) {
        window.bootbox.confirm("Вы действительно хотите удалить этот клиента?",
            function (result) {
                if (result) {
                    var url = "/Client/Delete?clientId=" + clientId;
                    $.ajax({
                        url: url,
                        success: function (data) {
                            if (data.Success === true) {
                                window.bootbox.alert(data.Message,
                                    function () {
                                        window.location.reload();
                                    });
                            } else {
                                window.bootbox.alert(data.Message);
                            }
                        }
                    });
                }
            });
    }

    self.Search = function (page, isDestroy) {
        isSearch = true;
        tmpNameFilter = self.nameFilter();
        self.LoadInformation(page, isDestroy);
    }
}