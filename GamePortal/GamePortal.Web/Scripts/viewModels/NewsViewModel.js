﻿function NewsVm(model, isEdit) {
    var titleError = "Поле \"Заголовок\" обязательно и должно быть меньше 100 символов в длину";
    var shortDescriptionError = "Поле \"Краткое содержание\" обязательно и должно быть меньше 300 символов в длину";
    var firstPaginationPageClick = true;
    var isSearch = false;
    var $pagination = $('#ulPagination');
    var tmpTitleFilter = "";
    var self = this;

    self.model = ko.observable(ko.mapping.fromJS(model));
    self.headerFile = ko.observable(null);
    self.errors = ko.observableArray();
    self.commentText = ko.observable('');
    self.replyToComment = ko.observable(false);
    self.replyName = ko.observable('');
    self.replyId = ko.observable();
    self.newsList = ko.observableArray([]);
    self.titleFilter = ko.observable();
    self.selectedTags = ko.observableArray([]);
    self.currentPage = 1;
    self.pageSize = 20;
    self.totalPages = 1;

    self.isNewsLiked = ko.pureComputed(function () {
        return self.model().IsLiked() ? 'fas' : 'far';
    },
        self);

    self.isNewsUnliked = ko.pureComputed(function () {
        return self.model().IsUnliked() ? 'fas' : 'far';
    },
        self);

    self.OnHeaderFileSelected = function (vm, event) {
        if (event.target.files.length > 0) {
            self.headerFile(event.target.files.item(0));
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#previewHeaderFile').attr('src', e.target.result);
            }
            reader.readAsDataURL(event.target.files.item(0));
            $('#previewHeaderFile').show();
        } else {
            self.headerFile(null);
            $('#previewHeaderFile').hide();
        }
    };

    self.Save = function () {
        if (self.headerFile() === null && self.model().HeaderFile().Path === null) {
            window.bootbox.alert("Выберите файл для заставки");
            return;
        }
        let indexTitleError = self.errors.indexOf(titleError);
        let indexShortDescriptionError = self.errors.indexOf(shortDescriptionError);
        if (self.model().Title == null || self.model().Title.length > 100) {
            if (indexTitleError === -1) {
                self.errors.push(titleError);
            }
            window.scrollTo(0, 0);
            return;
        } else {
            if (indexTitleError !== -1) {
                self.errors.splice(indexTitleError, 1);
            }
        }
        if (self.model().ShortDescription == null || self.model().ShortDescription.length > 300) {
            if (indexShortDescriptionError === -1) {
                self.errors.push(shortDescriptionError);
            }
            window.scrollTo(0, 0);
            return;
        } else {
            if (indexShortDescriptionError !== -1) {
                self.errors.splice(indexShortDescriptionError, 1);
            }
        }
        var dialog = window.bootbox.dialog({
            message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Загрузка...</div>',
            closeButton: false
        });
        var model = ko.mapping.toJS(self.model());
        var formData = toFormData(model);
        formData.delete("Description");
        formData.append("Description", tinyMCE.get('Description').getContent());
        if (self.headerFile() != null) {
            formData.append("file", self.headerFile(), self.headerFile().name);
        }
        $.ajax({
            type: 'POST',
            url: isEdit ? '/News/Edit' : '/News/Create',
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                dialog.modal('hide');
                if (response != null && response.success) {
                    window.bootbox.alert({
                        message: response.message,
                        callback: function () { window.location.href = '/News/Index'; }
                    });
                } else {
                    if (typeof response != "undefined" && response != null) {
                        window.bootbox.alert(
                            {
                                message: "<div style='max-height: 300px;overflow: auto;'>" +
                                    response.message +
                                    "</div>",
                                title: "Проверьте правильность вводимых данных."
                            });
                    }
                }
            },
            error: function (xhr) {
                window.bootbox.alert(xhr.responseText);
            }
        });
    };

    self.RemoveTag = function (tag) {
        var index = self.model().Tags.indexOf(tag);
        if (index !== -1) {
            self.model().Tags.splice(index, 1);
        }
    }

    self.Vote = function (score) {
        var data = {
            'newsId': self.model().Id(),
            'score': score
        }
        $.ajax({
            contentType: 'application/json',
            type: 'POST',
            traditional: true,
            url: '/News/Vote',
            data: JSON.stringify(data),
            success: function (response) {
                if (response != null && response.success) {
                    if (self.model().IsLiked()) {
                        if (score === 1) {
                            self.model().PositiveVotes(self.model().PositiveVotes() - 1);
                            self.model().IsLiked(false);
                        } else {
                            self.model().PositiveVotes(self.model().PositiveVotes() - 1);
                            self.model().NegativeVotes(self.model().NegativeVotes() + 1);
                            self.model().IsLiked(false);
                            self.model().IsUnliked(true);
                        }
                    } else if (self.model().IsUnliked()) {
                        if (score === -1) {
                            self.model().NegativeVotes(self.model().NegativeVotes() - 1);
                            self.model().IsUnliked(false);
                        } else {
                            self.model().PositiveVotes(self.model().PositiveVotes() + 1);
                            self.model().NegativeVotes(self.model().NegativeVotes() - 1);
                            self.model().IsLiked(true);
                            self.model().IsUnliked(false);
                        }
                    } else {
                        if (score === 1) {
                            self.model().PositiveVotes(self.model().PositiveVotes() + 1);
                            self.model().IsLiked(true);
                        } else {
                            self.model().NegativeVotes(self.model().NegativeVotes() + 1);
                            self.model().IsUnliked(true);
                        }
                    }
                }
            },
            error: function (xhr) {
                window.bootbox.alert(xhr.responseText);
            }
        });
    };

    self.VoteComment = function (comment, score) {
        var data = {
            'commentId': comment.Id(),
            'score': score
        }
        $.ajax({
            contentType: 'application/json',
            type: 'POST',
            traditional: true,
            url: '/News/VoteComment',
            data: JSON.stringify(data),
            success: function (response) {
                if (response != null && response.success) {
                    if (comment.IsLiked()) {
                        if (score === 1) {
                            comment.PositiveVotes(comment.PositiveVotes() - 1);
                            comment.IsLiked(false);
                        } else {
                            comment.PositiveVotes(comment.PositiveVotes() - 1);
                            comment.NegativeVotes(comment.NegativeVotes() + 1);
                            comment.IsLiked(false);
                            comment.IsUnliked(true);
                        }
                    } else if (comment.IsUnliked()) {
                        if (score === -1) {
                            comment.NegativeVotes(comment.NegativeVotes() - 1);
                            comment.IsUnliked(false);
                        } else {
                            comment.PositiveVotes(comment.PositiveVotes() + 1);
                            comment.NegativeVotes(comment.NegativeVotes() - 1);
                            comment.IsLiked(true);
                            comment.IsUnliked(false);
                        }
                    } else {
                        if (score === 1) {
                            comment.PositiveVotes(comment.PositiveVotes() + 1);
                            comment.IsLiked(true);
                        } else {
                            comment.NegativeVotes(comment.NegativeVotes() + 1);
                            comment.IsUnliked(true);
                        }
                    }
                }
            },
            error: function (xhr) {
                window.bootbox.alert(xhr.responseText);
            }
        });
    };

    self.SendComment = function () {
        var data = {
            'text': self.commentText(),
            'newsId': self.model().Id(),
            'replyId': self.replyId()
        }
        $.ajax({
            contentType: 'application/json',
            type: 'POST',
            traditional: true,
            url: '/News/AddComment',
            data: JSON.stringify(data),
            success: function (response) {
                if (response != null && response.success) {
                    window.location.reload();
                } else if (response != null) {
                    window.bootbox.alert(response.message);
                }
            },
            error: function (xhr) {
                window.bootbox.alert(xhr.responseText);
            }
        });
    }

    self.ReplyToComment = function (reply) {
        self.replyToComment(true);
        self.replyName(reply.CreatedByUserName());
        self.replyId(reply.Id());
    }

    self.CancelReply = function () {
        self.replyToComment(false);
        self.replyName('');
        self.replyId(null);
    }

    self.LoadInformation = function (page, isDestroy) {
        $('.modal').modal('show');
        var param = {
            'page': page,
            'pageSize': self.pageSize,
            'titleFilter': tmpTitleFilter,
            'selectedTags': $.map(self.selectedTags(),
                function (v) {
                    return v.Id;
                })
        };
        $.ajax({
            contentType: 'application/json',
            type: 'POST',
            traditional: true,
            url: '/News/GetPaginated',
            data: JSON.stringify(param),
            success: function (data) {
                self.newsList.removeAll();
                data.Items.forEach(function (item) {
                    self.newsList.push(item);
                });
                if (isDestroy) $pagination.twbsPagination('destroy');
                self.currentPage = data.Page;
                self.totalPages = data.TotalPages;
                $('#divPagination').text('Страница ' + page + ' из ' + self.totalPages);
                $pagination.twbsPagination({
                    startPage: 1,
                    totalPages: self.totalPages,
                    first: 'Первая',
                    prev: 'Предыдущая',
                    next: 'Следующая',
                    last: 'Последняя',
                    onPageClick: function (evt, page) {
                        if (firstPaginationPageClick) {
                            firstPaginationPageClick = false;
                            isSearch = false;
                            return;
                        }
                        if (isSearch) {
                            isSearch = false;
                            return;
                        }
                        self.LoadInformation(page, false);
                    }
                });
                $('.modal').modal('hide');
            }
        });
    }

    self.OpenNews = function (newsId) {
        var url = "/News/News?newsId=" + newsId;
        window.location.href = url;
    }

    self.EditNews = function (newsId) {
        var url = "/News/Edit?newsId=" + newsId;
        window.location.href = url;
    }

    self.DeleteNews = function (newsId) {
        window.bootbox.confirm("Вы действительно хотите удалить эту новость?",
            function (result) {
                if (result) {
                    var url = "/News/Delete?newsId=" + newsId;
                    $.ajax({
                        url: url,
                        success: function (data) {
                            if (data.Success === true) {
                                window.bootbox.alert(data.Message,
                                    function () {
                                        window.location.reload();
                                    });
                            } else {
                                window.bootbox.alert(data.Text);
                            }
                        }
                    });
                }
            });
    }

    self.Search = function (page, isDestroy) {
        isSearch = true;
        tmpTitleFilter = self.titleFilter();
        self.LoadInformation(page, isDestroy);
    }

    self.AddTagToSearch = function (tag) {
        if (!self.selectedTags().find(x => { return x.Id === tag.Id })) {
            self.selectedTags.push(tag);
            self.Search(1, true);
        }
    }

    self.RemoveTagFromSearch = function (tag) {
        var index = self.selectedTags.indexOf(tag);
        if (index !== -1) {
            self.selectedTags.splice(index, 1);
            self.Search(1, true);
        }
    }

    $('#textarea-autocomplete-to-tags').on('click',
        'button',
        function (e) {
            e.preventDefault();
            var tag = self.model().Tags.find(function (t) {
                if (t.Id === this.name) {
                    return t;
                }
                return null;
            });
            var index = self.model().Tags.indexOf(tag);
            self.model().Tags.splice(index, 1);
            $(this).remove();
            return false;
        });
    $('#textarea-autocomplete-to-tags input')
        .on("keydown",
            function (event) {
                if (event.keyCode === $.ui.keyCode.TAB &&
                    $(this).autocomplete("instance").menu.active) {
                    event.preventDefault();
                }
            })
        .autocomplete({
            source: function (request, response) {
                var data =
                    {
                        'NameFilter': request.term,
                        'SelectedTags': $.map(self.model().Tags(),
                            function (v) {
                                return v.Id;
                            })
                    }
                    ;
                $.ajax({
                    contentType: 'application/json',
                    type: 'POST',
                    traditional: true,
                    url: '/Tag/GetTagsPaginated',
                    data: ko.toJSON(data),
                    success: function (data) {
                        response($.map(data.Data.Items,
                            function (item) {
                                return {
                                    plink: item.Id,
                                    label: item.Name
                                }
                            }));
                    }
                });
            },
            focus: function () {
                return false;
            },
            select: function (event, ui) {
                this.value = '';
                self.model().Tags.push({
                    Id: ui.item.plink,
                    Name: ui.item.value
                });
                return false;
            }
        });

    $('#textarea-autocomplete-to-tags').click(function () {
        $('#textarea-autocomplete-to-tags input').focus();
    });

    tinyMCE.init({
        selector: '#Description',
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern imagetools"
        ],
        toolbar1:
            "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        toolbar2: "print preview media | forecolor backcolor emoticons | ltr rtl",
        document_base_url: "http://localhost:55356",
        file_picker_callback: function (callback, value, meta) {
            if (meta.filetype === "media" || meta.filetype === "image") {
                jQuery("#news-files").trigger("click");
                $("#news-files").unbind('change');
                jQuery("#news-files").on("change",
                    function () {
                        var file = this.files[0];
                        var reader = new FileReader();
                        var fd = new FormData();
                        fd.append("file", file);
                        fd.append('filetype', meta.filetype);
                        var filePath = "";
                        jQuery.ajax({
                            url: "/News/UploadFile",
                            type: "post",
                            data: fd,
                            contentType: false,
                            processData: false,
                            async: false,
                            success: function (response) {
                                if (response.success === true) {
                                    filePath = response.data.Url;
                                    response.data.Path = ko.observable(response.data.Path);
                                    self.model().Files.push(response.data);
                                }
                            }
                        });
                        reader.onload = function () {
                            callback(filePath);
                        };
                        reader.readAsDataURL(file);
                    });
            }
        }
    });
}