﻿using System;
using System.Web.Mvc;
using GamePortal.Data.DB;
using GamePortal.Domain.Enums;
using GamePortal.Domain.Models.Identity;
using GamePortal.Logic.Services.Identity;
using Hangfire;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;

namespace GamePortal.Web
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit https://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            GlobalConfiguration.Configuration
                .UseSqlServerStorage("DefaultConnection");

            app.UseHangfireDashboard();
            app.UseHangfireServer();

            var dependencyResolver = DependencyResolver.Current;
            var unitOfWork = dependencyResolver.GetService(typeof(UnitOfWork)) as UnitOfWork;

            // Configure the db context, user manager and signin manager to use a single instance per request
            app.CreatePerOwinContext<IUserStore<User, Guid>>((options, context) => new ApplicationUserStore(unitOfWork?.Context));
            app.CreatePerOwinContext<IRoleStore<Role, Guid>>((options, context) => new ApplicationRoleStore(unitOfWork?.Context));
            app.CreatePerOwinContext<ApplicationUserManager>((options, context) => new ApplicationUserManager(context.Get<IUserStore<User, Guid>>()));
            app.CreatePerOwinContext<ApplicationRoleManager>((options, context) => new ApplicationRoleManager(context.Get<IRoleStore<Role, Guid>>()));
            app.CreatePerOwinContext<ApplicationSignInManager>((options, context) => new ApplicationSignInManager(context.Get<ApplicationUserManager>(), context.Authentication));

            #region SeedDatabaseWithIdentities
            var roleManager = new RoleManager<Role, Guid>(new RoleStore<Role, Guid, UserRole>(unitOfWork?.Context));
            if (!roleManager.RoleExists(RoleNames.Administrator))
            {
                roleManager.Create(new Role
                {
                    Name = RoleNames.Administrator
                });
            }
            if (!roleManager.RoleExists(RoleNames.Manager))
            {
                roleManager.Create(new Role
                {
                    Name = RoleNames.Manager
                });
            }
            if (!roleManager.RoleExists(RoleNames.User))
            {
                roleManager.Create(new Role
                {
                    Name = RoleNames.User
                });
            }
            #endregion

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                Provider = new CookieAuthenticationProvider
                {
                    // Enables the application to validate the security stamp when the user logs in.
                    // This is a security feature which is used when you change a password or add an external login to your account.  
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, User, Guid>(
                        validateInterval: TimeSpan.FromMinutes(30),
                        regenerateIdentityCallback: (manager, user) => user.GenerateUserIdentityAsync(manager),
                        getUserIdCallback: (claimsIdentity) => Guid.NewGuid())
                }
            });
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Enables the application to temporarily store user information when they are verifying the second factor in the two-factor authentication process.
            app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));

            // Enables the application to remember the second login verification factor such as phone or email.
            // Once you check this option, your second step of verification during the login process will be remembered on the device where you logged in from.
            // This is similar to the RememberMe option when you log in.
            app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

            //app.UseTwitterAuthentication(
            //   consumerKey: "",
            //   consumerSecret: "");

            //app.UseFacebookAuthentication(
            //   appId: "",
            //   appSecret: "");

            //app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            //{
            //    ClientId = "",
            //    ClientSecret = ""
            //});
        }
    }
}