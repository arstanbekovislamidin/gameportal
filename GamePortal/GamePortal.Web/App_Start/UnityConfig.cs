using System;
using GamePortal.Data.DB;
using GamePortal.Domain.Contracts;
using GamePortal.Domain.Contracts.Services;
using GamePortal.Domain.Contracts.UnitOfWork;
using GamePortal.Domain.Models.Identity;
using GamePortal.Logic;
using GamePortal.Logic.Services;
using GamePortal.Logic.Services.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Unity;
using Unity.AspNet.Mvc;
using Unity.Injection;

namespace GamePortal.Web
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static readonly Lazy<IUnityContainer> UnityContainer =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => UnityContainer.Value;
        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="unityContainer">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        private static void RegisterTypes(IUnityContainer unityContainer)
        {
            // NOTE: To load from web.config uncomment the line below.
            // Make sure to add a Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            unityContainer.RegisterType<IServiceHost, ServiceHost>(new InjectionConstructor(unityContainer))
                .RegisterType<IUnitOfWork, UnitOfWork>(new PerRequestLifetimeManager())
                .RegisterType<IGameService, GameService>(new PerRequestLifetimeManager())
                .RegisterType<INewsService, NewsService>(new PerRequestLifetimeManager())
                .RegisterType<ITagService, TagService>(new PerRequestLifetimeManager())
                .RegisterType<IGenreService, GenreService>(new PerRequestLifetimeManager())
                .RegisterType<IFileService, FileService>(new PerRequestLifetimeManager())
                .RegisterType<ISurveyService, SurveyService>(new PerRequestLifetimeManager())
                .RegisterType<IClientService, ClientService>(new PerRequestLifetimeManager())
                .RegisterType<ApplicationUserManager>()
                .RegisterType<ApplicationSignInManager>()
                .RegisterType<ApplicationRoleManager>()
                .RegisterType<UserStore<User, Role, Guid, UserLogin, UserRole, UserClaim>, ApplicationUserStore>()
                .RegisterType<RoleStore<Role, Guid, UserRole>, ApplicationRoleStore>();
        }
    }
}