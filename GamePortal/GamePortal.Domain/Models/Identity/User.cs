﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;
using GamePortal.Domain.Models.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace GamePortal.Domain.Models.Identity
{
    public class User : IdentityUser<Guid, UserLogin, UserRole, UserClaim>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? BirthDate { get; set; }
        public DateTime RegisterDateTime { get; set; }
        public virtual ICollection<Vote> Votes { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Choice> Choices { get; set; }
        public virtual ICollection<Client> Clients { get; set; }
        public DateTime? BanEndDate { get; set; }
        public virtual File HeaderFile { get; set; }
        public Guid HeaderFileId { get; set; }

        [NotMapped]
        public bool IsBannedForNow => BanEndDate > DateTime.Now;

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User, Guid> manager, string authenticationType = "")
        {
            var userIdentity = await manager.CreateIdentityAsync(this, string.IsNullOrEmpty(authenticationType) ? DefaultAuthenticationTypes.ApplicationCookie : authenticationType);
            return userIdentity;
        }
    }
}
