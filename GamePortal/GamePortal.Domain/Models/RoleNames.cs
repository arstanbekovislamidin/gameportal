﻿namespace GamePortal.Domain.Enums
{
    public class RoleNames
    {
        public const string Administrator = "Administrator";
        public const string Manager = "Manager";
        public const string User = "User";
    }
}
