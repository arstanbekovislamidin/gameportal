﻿using System;
using GamePortal.Domain.Enums;

namespace GamePortal.Domain.Models
{
    public class ExecuteResult
    {
        public ExecuteState State { get; set; }

        public string Message { get; set; } = string.Empty;

        public bool IsOk => State == ExecuteState.Ok;


        public static ExecuteResult Ok()
        {
            return new ExecuteResult { State = ExecuteState.Ok };
        }

        public static ExecuteResult Ok(string message)
        {
            return new ExecuteResult { State = ExecuteState.Ok, Message = message };
        }

        public static ExecuteResult Error(string errorMessage)
        {
            return new ExecuteResult { State = ExecuteState.Error, Message = errorMessage };
        }
        
        public static TR Ok<TR>(Action<TR> action) where TR : ExecuteResult, new()
        {
            var result = new TR { State = ExecuteState.Ok };
            action(result);
            return result;
        }

        public static ExecuteResult Error(Exception exception)
        {
            return Error(exception.Message);
        }
    }
}
