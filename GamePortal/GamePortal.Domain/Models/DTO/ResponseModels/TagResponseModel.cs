﻿using System;
using System.ComponentModel.DataAnnotations;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Domain.Models.DTO.ResponseModels
{
    public class TagResponseModel : BaseEntity<Guid>
    {
        [Required(ErrorMessage = "Поле {0} обязательно.")]
        [MaxLength(100, ErrorMessage = "Поле {0} должно быть не больше {1} символов в длину.")]
        [Display(Name = "Имя")]
        public string Name { get; set; }
    }
}
