﻿using System;

namespace GamePortal.Domain.Models.DTO.ResponseModels
{
    public class NewsWebApiResponseModel : NewsResponseModel
    {
        public Guid HeaderFileId { get; set; }
        public byte[] HeaderFileContent { get; set; }
    }
}
