﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Domain.Models.DTO.ResponseModels
{
    public class NewsResponseModel : BaseEntity<Guid>
    {
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        public DateTime CreateDateTime { get; set; }
        public DateTime LastUpdateDateTime { get; set; }
        
        public FileResponseModel HeaderFile { get; set; }
        public UserResponseModel CurrentUser { get; set; }
        public List<FileResponseModel> Files { get; set; }
        public List<TagResponseModel> Tags { get; set; }
        public List<CommentResponseModel> Comments { get; set; }
    }
}
