﻿using System;
using GamePortal.Domain.Enums;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Domain.Models.DTO.ResponseModels
{
    public class VoteResponseModel : BaseEntity<Guid>
    {
        public ScoreType Score { get; set; }
        public DateTime CreateDateTime { get; set; }

        public Guid? GameId { get; set; }
        public Guid? NewsId { get; set; }
        public Guid? CommentId { get; set; }
    }
}
