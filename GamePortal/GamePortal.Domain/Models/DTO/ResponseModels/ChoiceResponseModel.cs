﻿using System;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Domain.Models.DTO.ResponseModels
{
    public class ChoiceResponseModel : BaseEntity<Guid>
    {
        public DateTime CreateDateTime { get; set; }
        public Guid CreatedByUserId { get; set; }
    }
}
