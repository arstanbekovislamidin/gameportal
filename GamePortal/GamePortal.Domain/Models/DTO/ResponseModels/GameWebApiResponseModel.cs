﻿using System;

namespace GamePortal.Domain.Models.DTO.ResponseModels
{
    public class GameWebApiResponseModel : GameResponseModel
    {
        public Guid HeaderFileId { get; set; }
        public byte[] HeaderFileContent { get; set; }
    }
}
