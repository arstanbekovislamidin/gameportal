﻿using System;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Domain.Models.DTO.ResponseModels
{
    public class FileResponseModel : BaseEntity<Guid>
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
