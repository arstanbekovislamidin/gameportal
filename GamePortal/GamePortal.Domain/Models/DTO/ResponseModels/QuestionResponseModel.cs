﻿using System;
using System.Collections.Generic;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Domain.Models.DTO.ResponseModels
{
    public class QuestionResponseModel : BaseEntity<Guid>
    {
        public string Text { get; set; }
        public List<AnswerResponseModel> Answers { get; set; } = new List<AnswerResponseModel>();
    }
}
