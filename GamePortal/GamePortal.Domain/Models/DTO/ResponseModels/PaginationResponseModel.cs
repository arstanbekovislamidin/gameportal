﻿using System.Collections.Generic;

namespace GamePortal.Domain.Models.DTO.ResponseModels
{
    public class PaginationResponseModel<T> where T : class 
    {
        public PaginationResponseModel()
        {
            Items = new List<T>();
        }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public int TotalItems { get; set; }
        public int TotalPages { get; set; }
        public IEnumerable<T> Items { get; set; }
    }
}
