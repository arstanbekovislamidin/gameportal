﻿using System;
using System.Collections.Generic;
using GamePortal.Domain.Enums;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Domain.Models.DTO.ResponseModels
{
    public class SurveyResponseModel : BaseEntity<Guid>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public SurveyStatus Status { get; set; }

        public List<QuestionResponseModel> Questions { get; set; } = new List<QuestionResponseModel>();
        public UserResponseModel CreatedByUser { get; set; }
        public Guid CreatedByUserId { get; set; }
    }
}
