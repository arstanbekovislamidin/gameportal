﻿using System;
using System.Collections.Generic;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Domain.Models.DTO.ResponseModels
{
    public class CommentResponseModel : BaseEntity<Guid>
    {
        public string Text { get; set; }
        public string CreateDateTime { get; set; }
        public DateTime LastUpdateDateTime { get; set; }
        public string CreatedByUserName { get; set; }
        public int? ReplyId { get; set; }
        public int PositiveVotes { get; set; }
        public int NegativeVotes { get; set; }
        public bool IsLiked { get; set; } = false;
        public bool IsUnliked { get; set; } = false;
        public Guid? GameId { get; set; }
        public Guid? NewsId { get; set; }
        public List<CommentResponseModel> RepliesToComment { get; set; }
    }
}
