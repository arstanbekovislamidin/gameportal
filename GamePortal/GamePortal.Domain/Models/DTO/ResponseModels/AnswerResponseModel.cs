﻿using System;
using System.Collections.Generic;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Domain.Models.DTO.ResponseModels
{
    public class AnswerResponseModel : BaseEntity<Guid>
    {
        public string Text { get; set; }
        public List<ChoiceResponseModel> Choices { get; set; }
    }
}
