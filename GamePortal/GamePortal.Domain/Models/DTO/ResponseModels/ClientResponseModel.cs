﻿using System;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Domain.Models.DTO.ResponseModels
{
    public class ClientResponseModel : BaseEntity<Guid>
    {
        public string Secret { get; set; }
        public string Name { get; set; }
        public string WebSiteUrl { get; set; }
        public string RedirectUrl { get; set; }
        public Guid CreatedById { get; set; }
    }
}
