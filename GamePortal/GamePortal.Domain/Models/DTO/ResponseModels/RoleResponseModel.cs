﻿using System;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Domain.Models.DTO.ResponseModels
{
    public class RoleResponseModel : BaseEntity<Guid>
    {
        public string Name { get; set; }
    }
}
