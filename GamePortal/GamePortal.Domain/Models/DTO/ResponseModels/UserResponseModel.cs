﻿using System;
using System.Collections.Generic;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Domain.Models.DTO.ResponseModels
{
    public class UserResponseModel : BaseEntity<Guid>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public List<RoleResponseModel> Roles { get; set; }
        public List<VoteResponseModel> Votes { get; set; }
        public List<CommentResponseModel> Comments { get; set; }
    }
}
