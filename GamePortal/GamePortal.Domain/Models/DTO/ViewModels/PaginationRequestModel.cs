﻿using System;
using System.Collections.Generic;

namespace GamePortal.Domain.Models.DTO.ViewModels
{
    public class PaginationRequestModel
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string NameFilter { get; set; }
        public List<Guid> SelectedGenres { get; set; }
        public List<Guid> SelectedTags { get; set; }
    }
}
