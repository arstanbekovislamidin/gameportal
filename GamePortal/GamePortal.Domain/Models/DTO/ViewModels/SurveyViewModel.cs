﻿using System;
using System.Collections.Generic;
using GamePortal.Domain.Enums;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Domain.Models.DTO.ViewModels
{
    public class SurveyViewModel : BaseEntity<Guid>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public SurveyStatus Status { get; set; }

        public List<QuestionViewModel> Questions { get; set; } = new List<QuestionViewModel>();
    }
}
