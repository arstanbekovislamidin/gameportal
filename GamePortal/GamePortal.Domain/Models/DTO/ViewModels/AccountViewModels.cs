﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using GamePortal.Domain.Models.DTO.ResponseModels;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Domain.Models.DTO.ViewModels
{
    public class ExternalLoginConfirmationViewModel
    {
        [EmailAddress]
        [Display(Name = "Email")]
        [Required(ErrorMessage = "Поле {0} обязательно")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required(ErrorMessage = "Поле {0} обязательно")]
        public string Provider { get; set; }

        [Required(ErrorMessage = "Поле {0} обязательно")]
        [Display(Name = "Код")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Запомнить этот браузер?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required(ErrorMessage = "Поле {0} обязательно")]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required(ErrorMessage = "Поле \"{0}\" обязательно")]
        [Display(Name = "Псевдоним")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Поле {0} обязательно")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Display(Name = "Запомнить вас?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel : UpdateUserViewModel
    {
        [Required(ErrorMessage = "Поле \"{0}\" обязательно")]
        [StringLength(100, ErrorMessage = "\"{0}\" должен состоять из {2} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Подтверждение пароля")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Пароль не совпадает с подтверждением.")]
        public string ConfirmPassword { get; set; }
    }

    public class UpdateUserViewModel : BaseEntity<Guid>
    {
        [Required(ErrorMessage = "Поле \"{0}\" обязательно")]
        [Display(Name = "Имя")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Поле \"{0}\" обязательно")]
        [Display(Name = "Фамилия")]
        public string LastName { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd'/'MM'/'yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Дата рождения")]
        public DateTime? BirthDate { get; set; }

        [Required(ErrorMessage = "Поле \"{0}\" обязательно")]
        [EmailAddress(ErrorMessage = "Некорректный Email. Проверьте правильность написания.")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Поле \"{0}\" обязательно")]
        [Display(Name = "Псевдоним")]
        public string UserName { get; set; }

        public List<string> SelectedRoles { get; set; }

        public List<SelectListItem> RolesToChoose { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd'/'MM'/'yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Дата окончания бана")]
        public DateTime? BanEndDate { get; set; }

        public FileResponseModel HeaderFile { get; set; }

        public Guid HeaderFileId { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required(ErrorMessage = "Поле {0} обязательно")]
        [EmailAddress(ErrorMessage = "Некорректный Email. Проверьте правильность написания.")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Поле {0} обязательно")]
        [StringLength(100, ErrorMessage = "{0} должен состоять из {2} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Подтверждение пароля")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Пароль не совпадает с подтверждением.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessage = "Поле {0} обязательно")]
        [EmailAddress(ErrorMessage = "Некорректный Email. Проверьте правильность написания.")]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginViewModel
    {
        public string Name { get; set; }

        public string Url { get; set; }

        public string State { get; set; }
    }

    public class ManageInfoViewModel
    {
        public string LocalLoginProvider { get; set; }

        public string Email { get; set; }

        public IEnumerable<UserLoginInfoViewModel> Logins { get; set; }

        public IEnumerable<ExternalLoginViewModel> ExternalLoginProviders { get; set; }
    }

    public class UserInfoViewModel
    {
        public string Email { get; set; }

        public bool HasRegistered { get; set; }

        public string LoginProvider { get; set; }
    }

    public class UserLoginInfoViewModel
    {
        public string LoginProvider { get; set; }

        public string ProviderKey { get; set; }
    }
}
