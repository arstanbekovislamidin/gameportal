﻿using System;
using System.Collections.Generic;
using GamePortal.Domain.Models.DTO.ResponseModels;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Domain.Models.DTO.ViewModels
{
    public class AnswerViewModel : BaseEntity<Guid>
    {
        public string Text { get; set; }
        public bool IsAnsweredByCurrentUser { get; set; }
        public int ChoicesCount { get; set; }
        public List<ChoiceResponseModel> Choices { get; set; }
    }
}
