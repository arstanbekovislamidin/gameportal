﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using GamePortal.Domain.Models.DTO.ResponseModels;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Domain.Models.DTO.ViewModels
{
    public class GameViewModel : BaseEntity<Guid>
    {
        public string Name { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        public DateTime LastUpdateDateTime { get; set; }
        public string HeaderFilePath { get; set; }
        public int PositiveVotes { get; set; }
        public int NegativeVotes { get; set; }
        public bool IsLiked { get; set; } = false;
        public bool IsUnliked { get; set; } = false;

        public UserResponseModel CurrentUser { get; set; }
        public List<GenreResponseModel> Genres { get; set; }
        public List<TagResponseModel> Tags { get; set; }
        public List<CommentResponseModel> Comments { get; set; }
    }
}
