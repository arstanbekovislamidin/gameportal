﻿using System;
using System.Collections.Generic;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Domain.Models.DTO.ViewModels
{
    public class QuestionViewModel : BaseEntity<Guid>
    {
        public string Text { get; set; }
        public List<AnswerViewModel> Answers { get; set; } = new List<AnswerViewModel>();
    }
}
