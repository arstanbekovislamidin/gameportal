﻿using System;
using GamePortal.Domain.Models.Identity;

namespace GamePortal.Domain.Models.Entities
{
    public class Client : BaseEntity<Guid>
    {
        public string Secret { get; set; }
        public string Name { get; set; }
        public string WebSiteUrl { get; set; }
        public string RedirectUrl { get; set; }

        public Guid CreatedById { get; set; }
        public virtual User CreatedBy { get; set; }
    }
}
