﻿using System;
using System.Collections.Generic;

namespace GamePortal.Domain.Models.Entities
{
    public class File : BaseEntity<Guid>
    {
        public string Name { get; set; }

        public virtual ICollection<Game> Games { get; set; }
        public virtual ICollection<News> News { get; set; }
    }
}
