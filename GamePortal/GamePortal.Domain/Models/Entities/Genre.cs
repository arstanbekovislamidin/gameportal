﻿using System;
using System.Collections.Generic;

namespace GamePortal.Domain.Models.Entities
{
    public class Genre : BaseEntity<Guid>
    {
        public string Name { get; set; }

        public Guid HeaderFileId { get; set; }
        public virtual File HeaderFile { get; set; }
        public ICollection<Game> Games { get; set; }
    }
}
