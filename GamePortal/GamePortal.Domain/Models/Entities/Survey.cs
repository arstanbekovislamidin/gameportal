﻿using System;
using System.Collections.Generic;
using GamePortal.Domain.Enums;
using GamePortal.Domain.Models.Identity;

namespace GamePortal.Domain.Models.Entities
{
    public class Survey : BaseEntity<Guid>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public SurveyStatus Status { get; set; }
        public string StartJobId { get; set; }
        public string EndJobId { get; set; }

        public virtual ICollection<Question> Questions { get; set; }
        public virtual User CreatedByUser { get; set; }
        public Guid CreatedByUserId { get; set; }
    }
}
