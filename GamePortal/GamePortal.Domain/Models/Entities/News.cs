﻿using System;
using System.Collections.Generic;
using GamePortal.Domain.Models.Identity;

namespace GamePortal.Domain.Models.Entities
{
    public class News : BaseEntity<Guid>
    {
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }
        public DateTime CreateDateTime { get; set; }
        public DateTime LastUpdateDateTime { get; set; }

        public Guid HeaderFileId { get; set; }
        public virtual File HeaderFile { get; set; }
        public Guid CreatedByUserId { get; set; }
        public virtual User CreatedByUser { get; set; }
        public ICollection<File> Files { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Vote> Votes { get; set; }
    }
}
