﻿using System.ComponentModel.DataAnnotations.Schema;

namespace GamePortal.Domain.Models.Entities
{
    public class BaseEntity<TKey>
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public TKey Id { get; set; }
    }
}
