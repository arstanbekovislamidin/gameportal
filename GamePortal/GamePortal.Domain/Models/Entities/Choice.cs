﻿using System;
using GamePortal.Domain.Models.Identity;

namespace GamePortal.Domain.Models.Entities
{
    public class Choice : BaseEntity<Guid>
    {
        public DateTime CreateDateTime { get; set; }

        public virtual Answer Answer { get; set; }
        public virtual User CreatedByUser { get; set; }
        public Guid CreatedByUserId { get; set; }
    }
}
