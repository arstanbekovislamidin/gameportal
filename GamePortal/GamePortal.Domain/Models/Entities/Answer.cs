﻿using System;
using System.Collections.Generic;

namespace GamePortal.Domain.Models.Entities
{
    public class Answer : BaseEntity<Guid>
    {
        public string Text { get; set; }
        public virtual Question Question { get; set; }
        public virtual ICollection<Choice> Choices { get; set; }
    }
}
