﻿using System;
using System.Collections.Generic;

namespace GamePortal.Domain.Models.Entities
{
    public class Tag : BaseEntity<Guid>
    {
        public string Name { get; set; }

        public ICollection<Game> Games { get; set; }
        public ICollection<News> News { get; set; }
    }
}
