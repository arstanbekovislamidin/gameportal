﻿using System;
using GamePortal.Domain.Enums;
using GamePortal.Domain.Models.Identity;

namespace GamePortal.Domain.Models.Entities
{
    public class Vote : BaseEntity<Guid>
    {
        public ScoreType Score { get; set; }
        public DateTime CreateDateTime { get; set; }

        public virtual Game Game { get; set; }
        public virtual News News { get; set; }
        public virtual Comment Comment { get; set; }
        public virtual User CreatedByUser { get; set; }
        public Guid CreatedByUserId { get; set; }
    }
}
