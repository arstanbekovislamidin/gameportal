﻿using System;
using System.Collections.Generic;

namespace GamePortal.Domain.Models.Entities
{
    public class Question : BaseEntity<Guid>
    {
        public string Text { get; set; }
        public virtual Survey Survey { get; set; }
        public Guid Survey_Id { get; set; }
        public virtual ICollection<Answer> Answers { get; set; }
    }
}
