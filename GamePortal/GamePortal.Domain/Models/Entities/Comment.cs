﻿using System;
using System.Collections.Generic;
using GamePortal.Domain.Models.Identity;

namespace GamePortal.Domain.Models.Entities
{
    public class Comment : BaseEntity<Guid>
    {
        public string Text { get; set; }
        public DateTime CreateDateTime { get; set; }
        public DateTime LastUpdateDateTime { get; set; }

        public virtual Game Game { get; set; }
        public virtual News News { get; set; }
        public virtual Comment CommentReply { get; set; }
        public virtual User CreatedByUser { get; set; }
        public Guid CreatedByUserId { get; set; }
        public virtual ICollection<Vote> Votes { get; set; }
    }
}
