﻿using System;
using System.Collections.Generic;
using System.Web;
using GamePortal.Domain.Models;
using GamePortal.Domain.Models.DTO.ResponseModels;

namespace GamePortal.Domain.Contracts.Services
{
    public interface IGenreService : IService
    {
        PaginationResponseModel<GenreResponseModel> GetGenresPagination(int page, int pageSize, string nameFilter, List<Guid> selectedGenreIds);
        GenreResponseModel GetGenre(Guid genreId);
        ExecuteResult AddGenre(GenreResponseModel genre, HttpPostedFileBase file);
        ExecuteResult UpdateGenre(GenreResponseModel genre, HttpPostedFileBase file);
        ExecuteResult DeleteGenre(Guid genreId);
    }
}
