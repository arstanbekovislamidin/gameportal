﻿using System;
using GamePortal.Domain.Models;
using GamePortal.Domain.Models.DTO.ResponseModels;

namespace GamePortal.Domain.Contracts.Services
{
    public interface IClientService : IService
    {
        PaginationResponseModel<ClientResponseModel> GetClientsPagination(int page, int pageSize, string nameFilter, Guid currentUserId);
        ClientResponseModel GetClient(Guid clientId);
        ExecuteResult AddClient(ClientResponseModel client, Guid currentUserId);
        ExecuteResult UpdateClient(ClientResponseModel client);
        ExecuteResult DeleteClient(Guid clientId, Guid currentUserId);

    }
}
