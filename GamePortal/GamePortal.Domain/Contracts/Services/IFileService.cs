﻿using System;
using System.Web;
using GamePortal.Domain.Models.DTO.ResponseModels;

namespace GamePortal.Domain.Contracts.Services
{
    public interface IFileService : IService
    {
        FileResponseModel UploadGameFiles(HttpPostedFileBase file);
        FileResponseModel UploadNewsFiles(HttpPostedFileBase file);
        Guid UploadGameFile(HttpPostedFileBase file);
        Guid UploadNewsFile(HttpPostedFileBase file);
        Guid UploadGenreFile(HttpPostedFileBase file);
        Guid UploadUserFile(HttpPostedFileBase file);
        string GetGameFilePath(Guid fileId);
        string GetNewsFilePath(Guid fileId);
        string GetGenreFilePath(Guid fileId);
        string GetUserFilePath(Guid fileId);
        void DeleteGameFile(string fileName);
        void DeleteNewsFile(string fileName);
        void DeleteGenreFile(string fileName);
        void DeleteUserFile(string fileName);
    }
}
