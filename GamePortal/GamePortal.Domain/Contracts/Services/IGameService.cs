﻿using System;
using System.Collections.Generic;
using System.Web;
using GamePortal.Domain.Enums;
using GamePortal.Domain.Models;
using GamePortal.Domain.Models.DTO.ResponseModels;
using GamePortal.Domain.Models.DTO.ViewModels;
using GamePortal.Domain.Models.Identity;

namespace GamePortal.Domain.Contracts.Services
{
    public interface IGameService : IService
    {
        PaginationResponseModel<GameResponseModel> GetGamesPagination(int page, int pageSize, string nameFilter, List<Guid> genresFilter, List<Guid> tagsFilter);
        PaginationResponseModel<GameWebApiResponseModel> GetGamesWebApiPagination(int page, int pageSize, string nameFilter, List<Guid> genresFilter, List<Guid> tagsFilter);
        GameResponseModel GetGame(Guid gameId);
        GameViewModel GetGameWithUser(Guid gameId, User currentUser);
        ExecuteResult AddGame(GameResponseModel game, HttpPostedFileBase file, Guid createdBy);
        ExecuteResult UpdateGame(GameResponseModel game, HttpPostedFileBase file);
        ExecuteResult DeleteGame(Guid gameId);
        ExecuteResult VoteGame(Guid gameId, ScoreType score, Guid userId);
        ExecuteResult VoteComment(Guid commentId, ScoreType score, Guid userId);
        ExecuteResult AddComment(string text, Guid gameId, Guid? replyId, Guid userId);
    }
}
