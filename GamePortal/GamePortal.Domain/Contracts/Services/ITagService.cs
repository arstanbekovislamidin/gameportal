﻿using System;
using System.Collections.Generic;
using GamePortal.Domain.Models;
using GamePortal.Domain.Models.DTO.ResponseModels;

namespace GamePortal.Domain.Contracts.Services
{
    public interface ITagService : IService
    {
        PaginationResponseModel<TagResponseModel> GetTagsPagination(int page, int pageSize, string nameFilter, List<Guid> selectedTagIds);
        TagResponseModel GetTag(Guid tagId);
        ExecuteResult AddTag(TagResponseModel tag);
        ExecuteResult UpdateTag(TagResponseModel tag);
        ExecuteResult DeleteTag(Guid tagId);
    }
}
