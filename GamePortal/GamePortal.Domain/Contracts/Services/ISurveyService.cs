﻿using System;
using GamePortal.Domain.Models;
using GamePortal.Domain.Models.DTO.ResponseModels;
using GamePortal.Domain.Models.DTO.ViewModels;
using GamePortal.Domain.Models.Identity;

namespace GamePortal.Domain.Contracts.Services
{
    public interface ISurveyService : IService
    {
        PaginationResponseModel<SurveyResponseModel> GetSurveysPagination(int page, int pageSize, string nameFilter, bool isAdmin);
        SurveyResponseModel GetSurvey(Guid surveyId);
        SurveyViewModel GetSurveyWithUser(Guid surveyId, Guid userId);
        ExecuteResult AddSurvey(SurveyResponseModel survey, Guid userId);
        ExecuteResult UpdateSurvey(SurveyResponseModel survey);
        ExecuteResult DeleteSurvey(Guid surveyId);
        ExecuteResult ChooseAnswer(Guid answerId, Guid currentUserId);
    }
}
