﻿using System;
using System.Collections.Generic;
using System.Web;
using GamePortal.Domain.Enums;
using GamePortal.Domain.Models;
using GamePortal.Domain.Models.DTO.ResponseModels;
using GamePortal.Domain.Models.DTO.ViewModels;
using GamePortal.Domain.Models.Identity;

namespace GamePortal.Domain.Contracts.Services
{
    public interface INewsService : IService
    {
        PaginationResponseModel<NewsResponseModel> GetNewsPagination(int page, int pageSize, string titleFilter, List<Guid> tagsFilter);
        PaginationResponseModel<NewsWebApiResponseModel> GetNewsWebApiPagination(int page, int pageSize, string titleFilter, List<Guid> tagsFilter);
        NewsResponseModel GetNews(Guid newsId);
        NewsViewModel GetNewsWithUser(Guid newsId, User user);
        ExecuteResult AddNews(NewsResponseModel news, HttpPostedFileBase file, Guid userId);
        ExecuteResult UpdateNews(NewsResponseModel news, HttpPostedFileBase file);
        ExecuteResult DeleteNews(Guid newsId);
        ExecuteResult VoteNews(Guid newsId, ScoreType score, Guid userId);
        ExecuteResult VoteComment(Guid commentId, ScoreType score, Guid userId);
        ExecuteResult AddComment(string text, Guid newsId, Guid? replyId, Guid userId);
    }
}
