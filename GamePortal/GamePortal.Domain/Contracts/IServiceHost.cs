﻿namespace GamePortal.Domain.Contracts
{
    public interface IServiceHost
    {
        void Register<T>(T service) where T : IService;
        T GetService<T>() where T : IService;
    }
}
