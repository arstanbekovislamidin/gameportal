﻿using System;
using GamePortal.Domain.Contracts.GenericRepository;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Domain.Contracts.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<T> GetRepository<T>() where T : BaseEntity<Guid>;

        void SaveChanges();

        void Dispose(bool disposing);
    }
}
