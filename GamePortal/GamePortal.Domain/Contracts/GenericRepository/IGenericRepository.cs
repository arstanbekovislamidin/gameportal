﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Domain.Contracts.GenericRepository
{
    public interface IGenericRepository<T> where T : BaseEntity<Guid>
    {
        IQueryable<T> GetAll();
        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);
        T FindById(Guid id);
        bool Any(Expression<Func<T, bool>> predicate);
        T FirstOrDefault(Expression<Func<T, bool>> predicate);
        Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> predicate);
        T Add(T entity);
        void AddRange(IEnumerable<T> entities);
        void Delete(T entity);
        void DeleteRange(IEnumerable<T> entities);
        T Edit(T entity);
        void EditRange(IEnumerable<T> entities);
        T Attach(T entity);
    }
}
