﻿namespace GamePortal.Domain.Enums
{
    public enum SurveyStatus
    {
        Queued,
        Started,
        Ended
    }
}
