﻿namespace GamePortal.Domain.Enums
{
    public enum ScoreType
    {
        Negative = -1,
        Positive = 1
    }
}
