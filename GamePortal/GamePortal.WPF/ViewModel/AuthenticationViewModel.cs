﻿using System;
using System.ComponentModel;
using System.Windows.Controls;
using GamePortal.WPF.Service;
using GamePortal.WPF.Views;

namespace GamePortal.WPF.ViewModel
{
    public interface IViewModel { }

    public class AuthenticationViewModel : IViewModel, INotifyPropertyChanged
    {
        public AuthenticationViewModel()
        {
        }

        public AuthenticationViewModel(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
            LoginCommand = new DelegateCommand(Login, CanLogin);
            LogoutCommand = new DelegateCommand(Logout, CanLogout);
            ShowGames = new DelegateCommand(ShowGameWindow, CanLogout);
            ShowNews = new DelegateCommand(ShowNewsWindow, CanLogout);
        }

        #region Properties
        private readonly IAuthenticationService _authenticationService;
        private string _username;
        private string _status;
        private int _page;
        private int _pageSize;
        private string _nameFilter;
        private string _pageInformation;

        public int TotalItems { get; set; }
        public int TotalPages { get; set; }
        protected enum PagingMode
        {
            First = 1, Previous = 2, Next = 3, Last = 4
        }

        public int Page
        {
            get => _page;
            set
            {
                _page = value;
                NotifyPropertyChanged("Page");
            }
        }

        public int PageSize
        {
            get => _pageSize;
            set
            {
                _pageSize = value;
                NotifyPropertyChanged("PageSize");
            }
        }

        public string NameFilter
        {
            get => _nameFilter;
            set
            {
                _nameFilter = value;
                NotifyPropertyChanged("NameFilter");
            }
        }

        public string Username
        {
            get => _username;
            set { _username = value; NotifyPropertyChanged("Username"); }
        }

        public string PageInformation
        {
            get => _pageInformation;
            set
            {
                _pageInformation = value;
                NotifyPropertyChanged("PageInformation");
            }
        }

        public string AccessToken { get; protected set; }

        public string AuthenticatedUser => IsAuthenticated ? $"Signed in as {Username}." : "Not authenticated!";

        public string Status
        {
            get => _status;
            set { _status = value; NotifyPropertyChanged("Status"); }
        }
        #endregion

        #region Commands
        public DelegateCommand LoginCommand { get; }

        public DelegateCommand LogoutCommand { get; }

        public DelegateCommand ShowGames { get; }
        
        public DelegateCommand ShowNews { get; }

        #endregion

        private void Login(object parameter)
        {
            if (!(parameter is PasswordBox passwordBox)) return;
            string clearTextPassword = passwordBox.Password;
            try
            {
                //Validate credentials through the authentication service
                var accessToken = _authenticationService.AuthenticateUser(Username, clearTextPassword);
                AccessToken = accessToken;
                //Update UI
                NotifyPropertyChanged("AuthenticatedUser");
                NotifyPropertyChanged("IsAuthenticated");
                LoginCommand.RaiseCanExecuteChanged();
                LogoutCommand.RaiseCanExecuteChanged();
                Username = string.Empty; //reset
                passwordBox.Password = string.Empty; //reset
                Status = string.Empty;
            }
            catch (UnauthorizedAccessException)
            {
                Status = "Login failed! Please provide some valid credentials.";
            }
            catch (Exception ex)
            {
                Status = $"ERROR: {ex.Message}";
            }
        }

        private bool CanLogin(object parameter)
        {
            return !IsAuthenticated;
        }

        private void Logout(object parameter)
        {
            AccessToken = string.Empty;
            NotifyPropertyChanged("AuthenticatedUser");
            NotifyPropertyChanged("IsAuthenticated");
            LoginCommand.RaiseCanExecuteChanged();
            LogoutCommand.RaiseCanExecuteChanged();
            Status = string.Empty;
        }

        private bool CanLogout(object parameter)
        {
            return IsAuthenticated;
        }

        public bool IsAuthenticated => !string.IsNullOrEmpty(AccessToken);

        public void ShowGameWindow(object parameter)
        {
            IView view = new GameWindow(AccessToken);
            view.Show();
        }

        public void ShowNewsWindow(object parameter)
        {
            IView view = new NewsWindow(AccessToken);
            view.Show();
        }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
