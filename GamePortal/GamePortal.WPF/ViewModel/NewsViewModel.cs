﻿using System.Collections.ObjectModel;
using System.IO;
using System.Net;
using System.Windows.Media.Imaging;
using GamePortal.Domain.Models.DTO.ResponseModels;
using GamePortal.WPF.ResponseModel;
using Newtonsoft.Json;

namespace GamePortal.WPF.ViewModel
{
    public class NewsViewModel : AuthenticationViewModel
    {
        public ObservableCollection<NewsModel> News { get; set; }
        public DelegateCommand FirstCommand { get; }
        public DelegateCommand PreviousCommand { get; }
        public DelegateCommand NextCommand { get; }
        public DelegateCommand LastCommand { get; }

        public NewsViewModel(string accessToken)
        {
            AccessToken = accessToken;
            FirstCommand = new DelegateCommand(Navigate, CanGoFirstOrPrevious);
            PreviousCommand = new DelegateCommand(Navigate, CanGoFirstOrPrevious);
            NextCommand = new DelegateCommand(Navigate, CanGoNext);
            LastCommand = new DelegateCommand(Navigate, CanGoLast);
            GetNews();
        }

        public void GetNews()
        {
            string endpoint = "http://localhost:51930/api/news/paginated";
            string data = $"?page={Page}&pageSize={PageSize}&nameFilter={NameFilter}";

            WebClient wc = new WebClient
            {
                Headers =
                {
                    ["Content-Type"] = "application/json",
                    ["Authorization"] = $"bearer {AccessToken}"
                }
            };

            string response = wc.DownloadString(endpoint + data);
            var result = JsonConvert.DeserializeObject<PaginatedResponse<PaginationResponseModel<NewsModel>>>(response);
            News = new ObservableCollection<NewsModel>(result.data.Items);
            foreach (var news in News)
            {
                using (MemoryStream stream = new MemoryStream(news.HeaderFileContent))
                {
                    BitmapImage imageSource = new BitmapImage();
                    stream.Seek(0, SeekOrigin.Begin);
                    imageSource.BeginInit();
                    imageSource.StreamSource = stream;
                    imageSource.CacheOption = BitmapCacheOption.OnLoad;
                    imageSource.EndInit();
                    news.HeaderImage = imageSource;
                }
            }
            TotalItems = result.data.TotalItems;
            TotalPages = result.data.TotalPages;
        }

        private void Navigate(object parameter)
        {
            switch ((PagingMode)parameter)
            {
                case PagingMode.First:
                    if (Page == 1) return;
                    Page = 1;
                    GetNews();
                    break;
                case PagingMode.Previous:
                    if (Page == 1) return;
                    Page--;
                    GetNews();
                    break;
                case PagingMode.Next:
                    if (Page < TotalPages) return;
                    Page++;
                    GetNews();
                    break;
                case PagingMode.Last:
                    if (Page != TotalPages) return;
                    Page = TotalPages;
                    GetNews();
                    break;
            }
        }

        private bool CanGoFirstOrPrevious(object parameter)
        {
            return Page != 1;
        }
        private bool CanGoNext(object parameter)
        {
            return Page < TotalPages;
        }
        private bool CanGoLast(object parameter)
        {
            return Page != TotalPages;
        }
    }

    public class NewsModel : NewsWebApiResponseModel
    {
        public BitmapImage HeaderImage { get; set; }
    }
}