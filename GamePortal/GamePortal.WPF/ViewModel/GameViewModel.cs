﻿using System.Collections.ObjectModel;
using System.IO;
using System.Net;
using System.Windows.Media.Imaging;
using GamePortal.Domain.Models.DTO.ResponseModels;
using GamePortal.WPF.ResponseModel;
using Newtonsoft.Json;

namespace GamePortal.WPF.ViewModel
{
    public class GameViewModel : AuthenticationViewModel
    {
        public ObservableCollection<GameModel> Games { get; set; }
        public DelegateCommand FirstCommand { get; }
        public DelegateCommand PreviousCommand { get; }
        public DelegateCommand NextCommand { get; }
        public DelegateCommand LastCommand { get; }

        public GameViewModel(string accessToken)
        {
            AccessToken = accessToken;
            FirstCommand = new DelegateCommand(Navigate, CanGoFirstOrPrevious);
            PreviousCommand = new DelegateCommand(Navigate, CanGoFirstOrPrevious);
            NextCommand = new DelegateCommand(Navigate, CanGoNext);
            LastCommand = new DelegateCommand(Navigate, CanGoLast);
            GetGames();
        }

        public void GetGames()
        {
            string endpoint = "http://localhost:51930/api/game/paginated";
            string data = $"?page={Page}&pageSize={PageSize}&nameFilter={NameFilter}";

            WebClient wc = new WebClient
            {
                Headers =
                {
                    ["Content-Type"] = "application/json",
                    ["Authorization"] = $"bearer {AccessToken}"
                }
            };

            string response = wc.DownloadString(endpoint + data);
            var result = JsonConvert.DeserializeObject<PaginatedResponse<PaginationResponseModel<GameModel>>>(response);
            Games = new ObservableCollection<GameModel>(result.data.Items);
            foreach (var game in Games)
            {
                using (MemoryStream stream = new MemoryStream(game.HeaderFileContent))
                {
                    BitmapImage imageSource = new BitmapImage();
                    stream.Seek(0, SeekOrigin.Begin);
                    imageSource.BeginInit();
                    imageSource.StreamSource = stream;
                    imageSource.CacheOption = BitmapCacheOption.OnLoad;
                    imageSource.EndInit();
                    game.HeaderImage = imageSource;
                }
            }
            TotalItems = result.data.TotalItems;
            TotalPages = result.data.TotalPages;
            PageInformation = $"Страница {Page} из {TotalPages}";
        }

        private void Navigate(object parameter)
        {
            switch ((PagingMode)parameter)
            {
                case PagingMode.First:
                    if (Page == 1) return;
                    Page = 1;
                    GetGames();
                    break;
                case PagingMode.Previous:
                    if (Page == 1) return;
                    Page--;
                    GetGames();
                    break;
                case PagingMode.Next:
                    if (Page < TotalPages) return;
                    Page++;
                    GetGames();
                    break;
                case PagingMode.Last:
                    if (Page != TotalPages) return;
                    Page = TotalPages;
                    GetGames();
                    break;
            }
        }

        private bool CanGoFirstOrPrevious(object parameter)
        {
            return Page != 1;
        }
        private bool CanGoNext(object parameter)
        {
            return Page < TotalPages;
        }
        private bool CanGoLast(object parameter)
        {
            return Page != TotalPages;
        }
    }

    public class GameModel : GameWebApiResponseModel
    {
        public BitmapImage HeaderImage { get; set; }
    }
}