﻿using GamePortal.WPF.ViewModel;

namespace GamePortal.WPF.Views
{
    /// <summary>
    /// Interaction logic for GameWindow.xaml
    /// </summary>
    public partial class GameWindow : IView
    {
        public GameWindow(string accessToken)
        {
            ViewModel = new GameViewModel(accessToken);
            InitializeComponent();
        }

        public IViewModel ViewModel
        {
            get => DataContext as IViewModel;
            set => DataContext = value;
        }
    }
}
