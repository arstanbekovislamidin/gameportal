﻿using GamePortal.WPF.ViewModel;

namespace GamePortal.WPF.Views
{
    /// <summary>
    /// Interaction logic for GameWindow.xaml
    /// </summary>
    public partial class NewsWindow : IView
    {
        public NewsWindow(string accessToken)
        {
            ViewModel = new NewsViewModel(accessToken);
            InitializeComponent();
        }

        public IViewModel ViewModel
        {
            get => DataContext as IViewModel;
            set => DataContext = value;
        }
    }
}
