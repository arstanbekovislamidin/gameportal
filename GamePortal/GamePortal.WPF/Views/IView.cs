﻿using GamePortal.WPF.ViewModel;

namespace GamePortal.WPF.Views
{

    public interface IView
    {
        IViewModel ViewModel
        {
            get;
            set;
        }

        void Show();
    }
}
