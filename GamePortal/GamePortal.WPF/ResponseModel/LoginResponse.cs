﻿namespace GamePortal.WPF.ViewModel
{
    public class LoginResponse
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public string userName { get; set; }
        public string userId { get; set; }
        public string clientId { get; set; }
        public string roles { get; set; }
        public string[] Roles { get; set; }
    }
}
