﻿namespace GamePortal.WPF.ResponseModel
{
    public class PaginatedResponse<T> where T : class 
    {
        public T data { get; set; }
    }
}
