﻿using System;
using System.Configuration;
using System.Net;
using GamePortal.WPF.ViewModel;
using Newtonsoft.Json;

namespace GamePortal.WPF.Service
{
    public interface IAuthenticationService
    {
        string AuthenticateUser(string username, string password);
    }

    public class AuthenticationService : IAuthenticationService
    {
        public string AuthenticateUser(string username, string clearTextPassword)
        {
            var clientId = ConfigurationManager.AppSettings.Get("clientId");
            var clientSecret = ConfigurationManager.AppSettings.Get("clientSecret");
            string endpoint = "http://localhost:51930/api/token";
            string method = "POST";
            string data =
                $"grant_type=password&username={username}&password={clearTextPassword}&client_id={clientId}&client_secret={clientSecret}";

            WebClient wc = new WebClient
            {
                Headers =
                {
                    ["Content-Type"] = "application/json"
                }
            };

            string response = wc.UploadString(endpoint, method, data);
            LoginResponse jsonResponse = JsonConvert.DeserializeObject<LoginResponse>(response);
            return jsonResponse.access_token ?? throw new UnauthorizedAccessException();
        }
    }
}
