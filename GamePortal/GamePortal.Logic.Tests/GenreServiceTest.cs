﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using AutoMapper;
using GamePortal.Domain.Contracts.GenericRepository;
using GamePortal.Domain.Contracts.Services;
using GamePortal.Domain.Contracts.UnitOfWork;
using GamePortal.Domain.Models.DTO.ResponseModels;
using GamePortal.Domain.Models.Entities;
using GamePortal.Logic.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Unity;
using Unity.ServiceLocation;

namespace GamePortal.Logic.Tests
{
    [TestClass]
    public class GenreServiceTest : UnityContainerTests
    {
        private readonly Mock<IUnitOfWork> _mockUnitOfWork = new Mock<IUnitOfWork>();

        private readonly Mock<IGenericRepository<Genre>> _mockGenreRepository =
            new Mock<IGenericRepository<Genre>>();

        [TestInitialize]
        public override void InitializeTests()
        {
            _mockUnitOfWork.Setup(x => x.GetRepository<Genre>())
                .Returns(_mockGenreRepository.Object)
                .Verifiable("Method should be called");
            UnityContainer = new UnityContainer()
                .RegisterType<IGenreService, GenreService>()
                .RegisterInstance(typeof(IUnitOfWork), _mockUnitOfWork.Object)
                .RegisterInstance(typeof(IGenericRepository<Genre>), _mockGenreRepository.Object);
            ServiceLocator = new UnityServiceLocator(UnityContainer);
            Mapper.Reset();
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<AutoMapperSettings>();
            });
        }

        [TestMethod]
        public void GetGenresPagination()
        {
            //Arrange
            var genres = new List<Genre>().AsQueryable();
            _mockGenreRepository.Setup(x => x.FindBy(It.IsAny<Expression<Func<Genre, bool>>>())).Returns(genres).Verifiable();

            //Act
            if (ServiceLocator.GetInstance(typeof(IGenreService)) is GenreService service)
            {
                var result = service.GetGenresPagination(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<List<Guid>>());

                //Assert
                Assert.IsNotNull(result);
            }
            _mockGenreRepository.Verify();
        }

        [TestMethod]
        public void GetGenre()
        {
            //Arrange
            var genre = new Genre();
            _mockGenreRepository.Setup(x => x.FindById(It.IsAny<Guid>())).Returns(genre).Verifiable();

            //Act
            if (ServiceLocator.GetInstance(typeof(IGenreService)) is GenreService service)
            {
                var result = service.GetGenre(It.IsAny<Guid>());

                //Assert
                Assert.IsNotNull(result);
            }
            _mockGenreRepository.Verify();
        }

        [TestMethod]
        public void AddGenre()
        {
            //Arrange
            _mockGenreRepository.Setup(x => x.Any(It.IsAny<Expression<Func<Genre, bool>>>())).Verifiable();
            _mockGenreRepository.Setup(x => x.Add(It.IsAny<Genre>())).Verifiable();
            _mockUnitOfWork.Setup(x => x.SaveChanges()).Verifiable();

            //Act
            if (ServiceLocator.GetInstance(typeof(IGenreService)) is GenreService service)
            {
                var result = service.AddGenre(It.IsAny<GenreResponseModel>(), It.IsAny<HttpPostedFileBase>());

                //Assert
                Assert.IsNotNull(result);
            }
            _mockGenreRepository.Verify();
        }

        [TestMethod]
        public void UpdateGenre()
        {
            //Arrange
            _mockGenreRepository.Setup(x => x.Any(It.IsAny<Expression<Func<Genre, bool>>>())).Verifiable();
            _mockGenreRepository.Setup(x => x.Any(It.IsAny<Expression<Func<Genre, bool>>>())).Verifiable();
            _mockGenreRepository.Setup(x => x.Edit(It.IsAny<Genre>())).Verifiable();
            _mockUnitOfWork.Setup(x => x.SaveChanges()).Verifiable();

            //Act
            if (ServiceLocator.GetInstance(typeof(IGenreService)) is GenreService service)
            {
                var result = service.UpdateGenre(It.IsAny<GenreResponseModel>(), It.IsAny<HttpPostedFileBase>());

                //Assert
                Assert.IsNotNull(result);
            }
            _mockGenreRepository.Verify();
        }

        [TestMethod]
        public void DeleteGenre()
        {
            //Arrange
            _mockGenreRepository.Setup(x => x.FindById(It.IsAny<Guid>())).Verifiable();
            _mockGenreRepository.Setup(x => x.Delete(It.IsAny<Genre>())).Verifiable();
            _mockUnitOfWork.Setup(x => x.SaveChanges()).Verifiable();

            //Act
            if (ServiceLocator.GetInstance(typeof(IGenreService)) is GenreService service)
            {
                var result = service.DeleteGenre(It.IsAny<Guid>());

                //Assert
                Assert.IsNotNull(result);
            }
            _mockGenreRepository.Verify();
        }
    }
}
