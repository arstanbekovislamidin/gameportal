﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using AutoMapper;
using GamePortal.Domain.Contracts.GenericRepository;
using GamePortal.Domain.Contracts.Services;
using GamePortal.Domain.Contracts.UnitOfWork;
using GamePortal.Domain.Models.DTO.ResponseModels;
using GamePortal.Domain.Models.Entities;
using GamePortal.Domain.Models.Identity;
using GamePortal.Logic.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Unity;
using Unity.ServiceLocation;

namespace GamePortal.Logic.Tests
{
    [TestClass]
    public class GameServiceTest : UnityContainerTests
    {
        private readonly Mock<IUnitOfWork> _mockUnitOfWork = new Mock<IUnitOfWork>();

        private readonly Mock<IGenericRepository<Game>> _mockGameRepository =
            new Mock<IGenericRepository<Game>>();

        [TestInitialize]
        public override void InitializeTests()
        {
            _mockUnitOfWork.Setup(x => x.GetRepository<Game>())
                .Returns(_mockGameRepository.Object)
                .Verifiable("Method should be called");
            UnityContainer = new UnityContainer()
                .RegisterType<IGameService, GameService>()
                .RegisterInstance(typeof(IUnitOfWork), _mockUnitOfWork.Object)
                .RegisterInstance(typeof(IGenericRepository<Game>), _mockGameRepository.Object);
            ServiceLocator = new UnityServiceLocator(UnityContainer);
            Mapper.Reset();
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<AutoMapperSettings>();
            });
        }

        [TestMethod]
        public void GetGamesPagination()
        {
            //Arrange
            var games = new List<Game>().AsQueryable();
            _mockGameRepository.Setup(x => x.FindBy(It.IsAny<Expression<Func<Game, bool>>>())).Returns(games).Verifiable();

            //Act
            if (ServiceLocator.GetInstance(typeof(IGameService)) is GameService service)
            {
                var result = service.GetGamesPagination(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<List<Guid>>(), It.IsAny<List<Guid>>());

                //Assert
                Assert.IsNotNull(result);
            }
            _mockGameRepository.Verify();
        }

        [TestMethod]
        public void GetGame()
        {
            //Arrange
            var game = new Game();
            _mockGameRepository.Setup(x => x.FindById(It.IsAny<Guid>())).Returns(game).Verifiable();

            //Act
            if (ServiceLocator.GetInstance(typeof(IGameService)) is GameService service)
            {
                var result = service.GetGame(It.IsAny<Guid>());

                //Assert
                Assert.IsNotNull(result);
            }
            _mockGameRepository.Verify();
        }

        [TestMethod]
        public void GetGameWithUser()
        {
            //Arrange
            var game = new Game();
            _mockGameRepository.Setup(x => x.FindById(It.IsAny<Guid>())).Returns(game).Verifiable();

            //Act
            if (ServiceLocator.GetInstance(typeof(IGameService)) is GameService service)
            {
                var result = service.GetGameWithUser(It.IsAny<Guid>(), It.IsAny<User>());

                //Assert
                Assert.IsNotNull(result);
            }
            _mockGameRepository.Verify();
        }

        [TestMethod]
        public void AddGame()
        {
            //Arrange
            _mockGameRepository.Setup(x => x.Any(It.IsAny<Expression<Func<Game, bool>>>())).Verifiable();
            _mockGameRepository.Setup(x => x.Add(It.IsAny<Game>())).Verifiable();
            _mockUnitOfWork.Setup(x => x.SaveChanges()).Verifiable();

            //Act
            if (ServiceLocator.GetInstance(typeof(IGameService)) is GameService service)
            {
                var result = service.AddGame(new GameResponseModel(), It.IsAny<HttpPostedFileBase>(), It.IsAny<Guid>());

                //Assert
                Assert.IsNotNull(result);
            }
            _mockGameRepository.Verify();
        }

        [TestMethod]
        public void UpdateGame()
        {
            //Arrange
            _mockGameRepository.Setup(x => x.FindById(It.IsAny<Guid>())).Verifiable();
            _mockGameRepository.Setup(x => x.Any(It.IsAny<Expression<Func<Game, bool>>>())).Verifiable();
            _mockGameRepository.Setup(x => x.Edit(It.IsAny<Game>())).Verifiable();
            _mockUnitOfWork.Setup(x => x.SaveChanges()).Verifiable();

            //Act
            if (ServiceLocator.GetInstance(typeof(IGameService)) is GameService service)
            {
                var result = service.UpdateGame(new GameResponseModel(), It.IsAny<HttpPostedFileBase>());

                //Assert
                Assert.IsNotNull(result);
            }
            _mockGameRepository.Verify();
        }

        [TestMethod]
        public void DeleteGame()
        {
            //Arrange
            _mockGameRepository.Setup(x => x.FindById(It.IsAny<Guid>())).Verifiable();
            _mockGameRepository.Setup(x => x.Delete(It.IsAny<Game>())).Verifiable();
            _mockUnitOfWork.Setup(x => x.SaveChanges()).Verifiable();

            //Act
            if (ServiceLocator.GetInstance(typeof(IGameService)) is GameService service)
            {
                var result = service.DeleteGame(It.IsAny<Guid>());

                //Assert
                Assert.IsNotNull(result);
            }
            _mockGameRepository.Verify();
        }
    }
}
