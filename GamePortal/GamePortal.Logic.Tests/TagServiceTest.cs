﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using GamePortal.Domain.Contracts.GenericRepository;
using GamePortal.Domain.Contracts.Services;
using GamePortal.Domain.Contracts.UnitOfWork;
using GamePortal.Domain.Models.DTO.ResponseModels;
using GamePortal.Domain.Models.Entities;
using GamePortal.Logic.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Unity;
using Unity.ServiceLocation;

namespace GamePortal.Logic.Tests
{
    [TestClass]
    public class TagServiceTest : UnityContainerTests
    {
        private readonly Mock<IUnitOfWork> _mockUnitOfWork = new Mock<IUnitOfWork>();

        private readonly Mock<IGenericRepository<Tag>> _mockTagRepository =
            new Mock<IGenericRepository<Tag>>();

        [TestInitialize]
        public override void InitializeTests()
        {
            _mockUnitOfWork.Setup(x => x.GetRepository<Tag>())
                .Returns(_mockTagRepository.Object)
                .Verifiable("Method should be called");
            UnityContainer = new UnityContainer()
                .RegisterType<ITagService, TagService>()
                .RegisterInstance(typeof(IUnitOfWork), _mockUnitOfWork.Object)
                .RegisterInstance(typeof(IGenericRepository<Tag>), _mockTagRepository.Object);
            ServiceLocator = new UnityServiceLocator(UnityContainer);
            Mapper.Reset();
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<AutoMapperSettings>();
            });
        }

        [TestMethod]
        public void GetTagsPagination()
        {
            //Arrange
            var tags = new List<Tag>().AsQueryable();
            _mockTagRepository.Setup(x => x.FindBy(It.IsAny<Expression<Func<Tag, bool>>>())).Returns(tags).Verifiable();

            //Act
            if (ServiceLocator.GetInstance(typeof(ITagService)) is TagService service)
            {
                var result = service.GetTagsPagination(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<List<Guid>>());

                //Assert
                Assert.IsNotNull(result);
            }
            _mockTagRepository.Verify();
        }

        [TestMethod]
        public void GetTag()
        {
            //Arrange
            var tag = new Tag();
            _mockTagRepository.Setup(x => x.FindById(It.IsAny<Guid>())).Returns(tag).Verifiable();

            //Act
            if (ServiceLocator.GetInstance(typeof(ITagService)) is TagService service)
            {
                var result = service.GetTag(It.IsAny<Guid>());

                //Assert
                Assert.IsNotNull(result);
            }
            _mockTagRepository.Verify();
        }

        [TestMethod]
        public void AddTag()
        {
            //Arrange
            _mockTagRepository.Setup(x => x.Any(It.IsAny<Expression<Func<Tag, bool>>>())).Verifiable();
            _mockTagRepository.Setup(x => x.Add(It.IsAny<Tag>())).Verifiable();
            _mockUnitOfWork.Setup(x => x.SaveChanges()).Verifiable();

            //Act
            if (ServiceLocator.GetInstance(typeof(ITagService)) is TagService service)
            {
                var result = service.AddTag(It.IsAny<TagResponseModel>());

                //Assert
                Assert.IsNotNull(result);
            }
            _mockTagRepository.Verify();
        }

        [TestMethod]
        public void UpdateTag()
        {
            //Arrange
            _mockTagRepository.Setup(x => x.Any(It.IsAny<Expression<Func<Tag, bool>>>())).Verifiable();
            _mockTagRepository.Setup(x => x.Edit(It.IsAny<Tag>())).Verifiable();
            _mockUnitOfWork.Setup(x => x.SaveChanges()).Verifiable();

            //Act
            if (ServiceLocator.GetInstance(typeof(ITagService)) is TagService service)
            {
                var result = service.UpdateTag(It.IsAny<TagResponseModel>());

                //Assert
                Assert.IsNotNull(result);
            }
            _mockTagRepository.Verify();
        }

        [TestMethod]
        public void DeleteTag()
        {
            //Arrange
            _mockTagRepository.Setup(x => x.FindById(It.IsAny<Guid>())).Verifiable();
            _mockTagRepository.Setup(x => x.Delete(It.IsAny<Tag>())).Verifiable();
            _mockUnitOfWork.Setup(x => x.SaveChanges()).Verifiable();

            //Act
            if (ServiceLocator.GetInstance(typeof(ITagService)) is TagService service)
            {
                var result = service.DeleteTag(It.IsAny<Guid>());

                //Assert
                Assert.IsNotNull(result);
            }
            _mockTagRepository.Verify();
        }
    }
}
