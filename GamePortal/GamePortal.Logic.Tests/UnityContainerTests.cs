﻿using CommonServiceLocator;
using Unity;

namespace GamePortal.Logic.Tests
{
    public abstract class UnityContainerTests
    {
        protected IServiceLocator ServiceLocator;

        protected IUnityContainer UnityContainer;

        public abstract void InitializeTests();
    }
}
