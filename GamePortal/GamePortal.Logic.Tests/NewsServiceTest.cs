﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using AutoMapper;
using GamePortal.Domain.Contracts.GenericRepository;
using GamePortal.Domain.Contracts.Services;
using GamePortal.Domain.Contracts.UnitOfWork;
using GamePortal.Domain.Models.DTO.ResponseModels;
using GamePortal.Domain.Models.Entities;
using GamePortal.Domain.Models.Identity;
using GamePortal.Logic.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Unity;
using Unity.ServiceLocation;

namespace GamePortal.Logic.Tests
{
    [TestClass]
    public class NewsServiceTest : UnityContainerTests
    {
        private readonly Mock<IUnitOfWork> _mockUnitOfWork = new Mock<IUnitOfWork>();

        private readonly Mock<IGenericRepository<News>> _mockNewsRepository =
            new Mock<IGenericRepository<News>>();

        [TestInitialize]
        public override void InitializeTests()
        {
            _mockUnitOfWork.Setup(x => x.GetRepository<News>())
                .Returns(_mockNewsRepository.Object)
                .Verifiable("Method should be called");
            UnityContainer = new UnityContainer()
                .RegisterType<INewsService, NewsService>()
                .RegisterInstance(typeof(IUnitOfWork), _mockUnitOfWork.Object)
                .RegisterInstance(typeof(IGenericRepository<News>), _mockNewsRepository.Object);
            ServiceLocator = new UnityServiceLocator(UnityContainer);
            Mapper.Reset();
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<AutoMapperSettings>();
            });
        }

        [TestMethod]
        public void GetNewsPagination()
        {
            //Arrange
            var news = new List<News>().AsQueryable();
            _mockNewsRepository.Setup(x => x.FindBy(It.IsAny<Expression<Func<News, bool>>>())).Returns(news).Verifiable();

            //Act
            if (ServiceLocator.GetInstance(typeof(INewsService)) is NewsService service)
            {
                var result = service.GetNewsPagination(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<List<Guid>>());

                //Assert
                Assert.IsNotNull(result);
            }
            _mockNewsRepository.Verify();
        }

        [TestMethod]
        public void GetNews()
        {
            //Arrange
            var news = new News();
            _mockNewsRepository.Setup(x => x.FindById(It.IsAny<Guid>())).Returns(news).Verifiable();

            //Act
            if (ServiceLocator.GetInstance(typeof(INewsService)) is NewsService service)
            {
                var result = service.GetNews(It.IsAny<Guid>());

                //Assert
                Assert.IsNotNull(result);
            }
            _mockNewsRepository.Verify();
        }

        [TestMethod]
        public void GetNewsWithUser()
        {
            //Arrange
            var news = new News();
            _mockNewsRepository.Setup(x => x.FindById(It.IsAny<Guid>())).Returns(news).Verifiable();

            //Act
            if (ServiceLocator.GetInstance(typeof(INewsService)) is NewsService service)
            {
                var result = service.GetNewsWithUser(It.IsAny<Guid>(), It.IsAny<User>());

                //Assert
                Assert.IsNotNull(result);
            }
            _mockNewsRepository.Verify();
        }

        [TestMethod]
        public void AddNews()
        {
            //Arrange
            _mockNewsRepository.Setup(x => x.Any(It.IsAny<Expression<Func<News, bool>>>())).Verifiable();
            _mockNewsRepository.Setup(x => x.Add(It.IsAny<News>())).Verifiable();
            _mockUnitOfWork.Setup(x => x.SaveChanges()).Verifiable();

            //Act
            if (ServiceLocator.GetInstance(typeof(INewsService)) is NewsService service)
            {
                var result = service.AddNews(new NewsResponseModel(), It.IsAny<HttpPostedFileBase>(), It.IsAny<Guid>());

                //Assert
                Assert.IsNotNull(result);
            }
            _mockNewsRepository.Verify();
        }

        [TestMethod]
        public void UpdateNews()
        {
            //Arrange
            _mockNewsRepository.Setup(x => x.FindById(It.IsAny<Guid>())).Verifiable();
            _mockNewsRepository.Setup(x => x.Any(It.IsAny<Expression<Func<News, bool>>>())).Verifiable();
            _mockNewsRepository.Setup(x => x.Edit(It.IsAny<News>())).Verifiable();
            _mockUnitOfWork.Setup(x => x.SaveChanges()).Verifiable();

            //Act
            if (ServiceLocator.GetInstance(typeof(INewsService)) is NewsService service)
            {
                var result = service.UpdateNews(new NewsResponseModel(), It.IsAny<HttpPostedFileBase>());

                //Assert
                Assert.IsNotNull(result);
            }
            _mockNewsRepository.Verify();
        }

        [TestMethod]
        public void DeleteNews()
        {
            //Arrange
            _mockNewsRepository.Setup(x => x.FindById(It.IsAny<Guid>())).Verifiable();
            _mockNewsRepository.Setup(x => x.Delete(It.IsAny<News>())).Verifiable();
            _mockUnitOfWork.Setup(x => x.SaveChanges()).Verifiable();

            //Act
            if (ServiceLocator.GetInstance(typeof(INewsService)) is NewsService service)
            {
                var result = service.DeleteNews(It.IsAny<Guid>());

                //Assert
                Assert.IsNotNull(result);
            }
            _mockNewsRepository.Verify();
        }
    }
}
