﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using GamePortal.Domain.Contracts.GenericRepository;
using GamePortal.Domain.Contracts.Services;
using GamePortal.Domain.Contracts.UnitOfWork;
using GamePortal.Domain.Models.DTO.ResponseModels;
using GamePortal.Domain.Models.Entities;
using GamePortal.Logic.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Unity;
using Unity.ServiceLocation;

namespace GamePortal.Logic.Tests
{
    [TestClass]
    public class SurveyServiceTest : UnityContainerTests
    {
        private readonly Mock<IUnitOfWork> _mockUnitOfWork = new Mock<IUnitOfWork>();

        private readonly Mock<IGenericRepository<Survey>> _mockSurveyRepository =
            new Mock<IGenericRepository<Survey>>();

        [TestInitialize]
        public override void InitializeTests()
        {
            _mockUnitOfWork.Setup(x => x.GetRepository<Survey>())
                .Returns(_mockSurveyRepository.Object)
                .Verifiable("Method should be called");
            UnityContainer = new UnityContainer()
                .RegisterType<ISurveyService, SurveyService>()
                .RegisterInstance(typeof(IUnitOfWork), _mockUnitOfWork.Object)
                .RegisterInstance(typeof(IGenericRepository<Survey>), _mockSurveyRepository.Object);
            ServiceLocator = new UnityServiceLocator(UnityContainer);
            Mapper.Reset();
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<AutoMapperSettings>();
            });
        }

        [TestMethod]
        public void GetSurveysPagination()
        {
            //Arrange
            var surveys = new List<Survey>().AsQueryable();
            _mockSurveyRepository.Setup(x => x.FindBy(It.IsAny<Expression<Func<Survey, bool>>>())).Returns(surveys).Verifiable();

            //Act
            if (ServiceLocator.GetInstance(typeof(ISurveyService)) is SurveyService service)
            {
                var result = service.GetSurveysPagination(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<bool>());

                //Assert
                Assert.IsNotNull(result);
            }
            _mockSurveyRepository.Verify();
        }

        [TestMethod]
        public void GetSurvey()
        {
            //Arrange
            var survey = new Survey();
            _mockSurveyRepository.Setup(x => x.FindById(It.IsAny<Guid>())).Returns(survey).Verifiable();

            //Act
            if (ServiceLocator.GetInstance(typeof(ISurveyService)) is SurveyService service)
            {
                var result = service.GetSurvey(It.IsAny<Guid>());

                //Assert
                Assert.IsNotNull(result);
            }
            _mockSurveyRepository.Verify();
        }

        [TestMethod]
        public void AddSurvey()
        {
            //Arrange
            _mockSurveyRepository.Setup(x => x.Add(It.IsAny<Survey>())).Verifiable();
            _mockUnitOfWork.Setup(x => x.SaveChanges()).Verifiable();

            //Act
            if (ServiceLocator.GetInstance(typeof(ISurveyService)) is SurveyService service)
            {
                var result = service.AddSurvey(new SurveyResponseModel(), It.IsAny<Guid>());

                //Assert
                Assert.IsNotNull(result);
            }
            _mockSurveyRepository.Verify();
        }

        [TestMethod]
        public void UpdateSurvey()
        {
            //Arrange
            _mockSurveyRepository.Setup(x => x.FindById(It.IsAny<Guid>())).Verifiable();
            _mockSurveyRepository.Setup(x => x.Edit(It.IsAny<Survey>())).Verifiable();
            _mockUnitOfWork.Setup(x => x.SaveChanges()).Verifiable();

            //Act
            if (ServiceLocator.GetInstance(typeof(ISurveyService)) is SurveyService service)
            {
                var result = service.UpdateSurvey(new SurveyResponseModel());

                //Assert
                Assert.IsNotNull(result);
            }
            _mockSurveyRepository.Verify();
        }

        [TestMethod]
        public void DeleteSurvey()
        {
            //Arrange
            _mockSurveyRepository.Setup(x => x.FindById(It.IsAny<Guid>())).Verifiable();
            _mockSurveyRepository.Setup(x => x.Delete(It.IsAny<Survey>())).Verifiable();
            _mockUnitOfWork.Setup(x => x.SaveChanges()).Verifiable();

            //Act
            if (ServiceLocator.GetInstance(typeof(ISurveyService)) is SurveyService service)
            {
                var result = service.DeleteSurvey(It.IsAny<Guid>());

                //Assert
                Assert.IsNotNull(result);
            }
            _mockSurveyRepository.Verify();
        }
    }
}
