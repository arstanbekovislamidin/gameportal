﻿using System.Linq;
using AutoMapper;
using GamePortal.Domain.Enums;
using GamePortal.Domain.Models.DTO.ResponseModels;
using GamePortal.Domain.Models.DTO.ViewModels;
using GamePortal.Domain.Models.Entities;
using GamePortal.Domain.Models.Identity;

namespace GamePortal.Logic
{
    public class AutoMapperSettings : Profile
    {
        public AutoMapperSettings()
        {
            CreateMap<Game, GameResponseModel>(MemberList.None)
                .ForMember(d => d.Comments, expression => expression.Ignore())
                .ForMember(d => d.HeaderFile, expression => expression.MapFrom(s => new FileResponseModel
                {
                    Id = s.HeaderFileId,
                    Name = s.HeaderFile.Name,
                    Url = $"/Game/Image?fileId={s.HeaderFileId}"
                }))
                .ReverseMap()
                .ForMember(d => d.CreateDateTime, expression => expression.Ignore())
                .ForMember(d => d.Comments, expression => expression.Ignore())
                .ForMember(d => d.LastUpdateDateTime, expression => expression.Ignore())
                .ForMember(d => d.CreatedByUserId, expression => expression.Ignore())
                .ForMember(d => d.CreatedByUser, expression => expression.Ignore())
                .ForMember(d => d.Votes, expression => expression.Ignore())
                .ForAllMembers(d => d.Condition(src => src != null));
            CreateMap<Game, GameWebApiResponseModel>(MemberList.None)
                .ForMember(d => d.Comments, expression => expression.Ignore())
                .ForMember(d => d.HeaderFile, expression => expression.Ignore())
                .ReverseMap()
                .ForMember(d => d.CreateDateTime, expression => expression.Ignore())
                .ForMember(d => d.Comments, expression => expression.Ignore())
                .ForMember(d => d.LastUpdateDateTime, expression => expression.Ignore())
                .ForMember(d => d.CreatedByUserId, expression => expression.Ignore())
                .ForMember(d => d.CreatedByUser, expression => expression.Ignore())
                .ForMember(d => d.Votes, expression => expression.Ignore())
                .ForAllMembers(d => d.Condition(src => src != null));
            CreateMap<Game, GameViewModel>(MemberList.None)
                .ForMember(d => d.PositiveVotes, expression => expression.MapFrom(s => s.Votes.Count(v => v.Score == ScoreType.Positive)))
                .ForMember(d => d.NegativeVotes, expression => expression.MapFrom(s => s.Votes.Count(v => v.Score == ScoreType.Negative)))
                .ForMember(d => d.HeaderFilePath, expressions => expressions.MapFrom(s => $"/News/Image?fileId={s.HeaderFile.Id}"))
                .ReverseMap()
                .ForAllMembers(d => d.Condition(src => src != null));
            CreateMap<News, NewsResponseModel>(MemberList.None)
                .ForMember(d => d.Comments, expression => expression.Ignore())
                .ForMember(d => d.HeaderFile, expression => expression.MapFrom(s => new FileResponseModel
                {
                    Id = s.HeaderFileId,
                    Name = s.HeaderFile.Name,
                    Url = $"/News/Image?fileId={s.HeaderFileId}"
                }))
                .ReverseMap()
                .ForMember(d => d.CreateDateTime, expression => expression.Ignore())
                .ForMember(d => d.Comments, expression => expression.Ignore())
                .ForMember(d => d.LastUpdateDateTime, expression => expression.Ignore())
                .ForMember(d => d.CreatedByUserId, expression => expression.Ignore())
                .ForMember(d => d.CreatedByUser, expression => expression.Ignore())
                .ForMember(d => d.Votes, expression => expression.Ignore())
                .ForAllMembers(d => d.Condition(src => src != null));
            CreateMap<News, NewsWebApiResponseModel>(MemberList.None)
                .ForMember(d => d.Comments, expression => expression.Ignore())
                .ForMember(d => d.HeaderFile, expression => expression.Ignore())
                .ReverseMap()
                .ForMember(d => d.CreateDateTime, expression => expression.Ignore())
                .ForMember(d => d.Comments, expression => expression.Ignore())
                .ForMember(d => d.LastUpdateDateTime, expression => expression.Ignore())
                .ForMember(d => d.CreatedByUserId, expression => expression.Ignore())
                .ForMember(d => d.CreatedByUser, expression => expression.Ignore())
                .ForMember(d => d.Votes, expression => expression.Ignore())
                .ForAllMembers(d => d.Condition(src => src != null));
            CreateMap<News, NewsViewModel>(MemberList.None)
                .ForMember(d => d.PositiveVotes, expression => expression.MapFrom(s => s.Votes.Count(v => v.Score == ScoreType.Positive)))
                .ForMember(d => d.NegativeVotes, expression => expression.MapFrom(s => s.Votes.Count(v => v.Score == ScoreType.Negative)))
                .ForMember(d => d.HeaderFilePath, expressions => expressions.MapFrom(s => $"/News/Image?fileId={s.HeaderFile.Id}"))
                .ReverseMap()
                .ForAllMembers(d => d.Condition(src => src != null));
            CreateMap<Tag, TagResponseModel>(MemberList.None)
                .ReverseMap()
                .ForAllMembers(d => d.Condition(src => src != null));
            CreateMap<User, UserResponseModel>(MemberList.None)
                .ReverseMap()
                .ForAllMembers(d => d.Condition(src => src != null));
            CreateMap<User, UpdateUserViewModel>(MemberList.None)
                .ForMember(d => d.HeaderFile, expression => expression.MapFrom(s => new FileResponseModel
                {
                    Id = s.HeaderFileId,
                    Name = s.HeaderFile.Name,
                    Url = $"/Account/Image?fileId={s.HeaderFileId}"
                }))
                .ReverseMap()
                .ForAllMembers(d => d.Condition(src => src != null));
            CreateMap<User, RegisterViewModel>(MemberList.None)
                .ForMember(d => d.HeaderFile, expression => expression.MapFrom(s => new FileResponseModel
                {
                    Id = s.HeaderFileId,
                    Name = s.HeaderFile.Name,
                    Url = $"/Account/Image?fileId={s.HeaderFileId}"
                }))
                .ReverseMap()
                .ForMember(d => d.Comments, expression => expression.Ignore())
                .ForMember(d => d.Votes, expression => expression.Ignore())
                .ForMember(d => d.HeaderFile, expression => expression.Ignore())
                .ForAllMembers(d => d.Condition(src => src != null));
            CreateMap<Genre, GenreResponseModel>(MemberList.None)
                .ForMember(d => d.HeaderFile, expression => expression.MapFrom(s => new FileResponseModel
                {
                    Id = s.HeaderFileId,
                    Name = s.HeaderFile.Name,
                    Url = $"/Genre/Image?fileId={s.HeaderFileId}"
                }))
                .ReverseMap()
                .ForAllMembers(d => d.Condition(src => src != null));
            CreateMap<Comment, CommentResponseModel>(MemberList.None)
                .ForMember(d => d.CreatedByUserName, expression => expression.MapFrom(s => s.CreatedByUser.UserName))
                .ForMember(d => d.ReplyId, expression => expression.MapFrom(s => s.CommentReply.Id))
                .ForMember(d => d.PositiveVotes, expression => expression.MapFrom(s => s.Votes.Count(v => v.Score == ScoreType.Positive)))
                .ForMember(d => d.NegativeVotes, expression => expression.MapFrom(s => s.Votes.Count(v => v.Score == ScoreType.Negative)))
                .ForMember(d => d.GameId, expression => expression.MapFrom(s => s.Game.Id))
                .ForMember(d => d.NewsId, expression => expression.MapFrom(s => s.News.Id))
                .ForMember(d => d.CreateDateTime, expression => expression.MapFrom(s => s.CreateDateTime.ToString("dd/MM/yyyy HH:mm:ss")))
                .ReverseMap()
                .ForAllMembers(d => d.Condition(src => src != null));
            CreateMap<Vote, VoteResponseModel>(MemberList.None)
                .ForMember(d => d.CommentId, expression => expression.MapFrom(s => s.Comment.Id))
                .ForMember(d => d.GameId, expression => expression.MapFrom(s => s.Game.Id))
                .ForMember(d => d.NewsId, expression => expression.MapFrom(s => s.News.Id))
                .ReverseMap()
                .ForAllMembers(d => d.Condition(src => src != null));
            CreateMap<File, FileResponseModel>(MemberList.None)
                .ReverseMap()
                .ForAllMembers(d => d.Condition(src => src != null));
            CreateMap<Survey, SurveyResponseModel>(MemberList.None)
                .ReverseMap()
                .ForMember(d => d.CreatedDateTime, expression => expression.Ignore())
                .ForMember(d => d.CreatedByUser, expression => expression.Ignore())
                .ForMember(d => d.CreatedByUserId, expression => expression.Ignore())
                .ForMember(d => d.Status, expression => expression.Ignore())
                .ForMember(d => d.StartJobId, expression => expression.Ignore())
                .ForMember(d => d.EndJobId, expression => expression.Ignore())
                .ForAllMembers(d => d.Condition(src => src != null));
            CreateMap<Question, QuestionResponseModel>(MemberList.None)
                .ReverseMap()
                .ForAllMembers(d => d.Condition(src => src != null));
            CreateMap<Answer, AnswerResponseModel>(MemberList.None)
                .ReverseMap()
                .ForMember(d => d.Choices, expression => expression.Ignore())
                .ForAllMembers(d => d.Condition(src => src != null));
            CreateMap<Choice, ChoiceResponseModel>(MemberList.None)
                .ReverseMap()
                .ForMember(d => d.CreatedByUserId, expression => expression.Ignore())
                .ForMember(d => d.CreatedByUser, expression => expression.Ignore())
                .ForAllMembers(d => d.Condition(src => src != null));
            CreateMap<Client, ClientResponseModel>(MemberList.None)
                .ReverseMap()
                .ForAllMembers(d => d.Condition(src => src != null));
        }
    }
}
