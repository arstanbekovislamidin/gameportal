﻿using System;
using System.Collections.Generic;
using GamePortal.Domain.Contracts;
using Unity;

namespace GamePortal.Logic
{
    public class ServiceHost : IServiceHost
    {
        private readonly IUnityContainer _container;
        private readonly Dictionary<Type, IService> _service;

        public ServiceHost(IUnityContainer container)
        {
            _container = container;
            _service = new Dictionary<Type, IService>();
        }

        public void Register<T>(T service) where T : IService
        {
            if (!_service.ContainsKey(typeof(T)))
                _service.Add(typeof(T), service);
        }

        public T GetService<T>() where T : IService
        {
            if (_service.ContainsKey(typeof(T)))
                return (T)_service[typeof(T)];

            var service = _container.Resolve<T>();
            Register(service);

            return service;
        }
    }
}
