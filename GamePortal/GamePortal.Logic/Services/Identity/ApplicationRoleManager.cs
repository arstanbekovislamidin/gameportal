﻿using System;
using GamePortal.Domain.Models.Identity;
using Microsoft.AspNet.Identity;

namespace GamePortal.Logic.Services.Identity
{
    public class ApplicationRoleManager : RoleManager<Role, Guid>
    {
        public ApplicationRoleManager(IRoleStore<Role, Guid> store) : base(store)
        {
        }
    }
}
