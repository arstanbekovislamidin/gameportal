﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using GamePortal.Domain.Models.DTO.ResponseModels;
using GamePortal.Domain.Models.DTO.ViewModels;
using GamePortal.Domain.Models.Identity;
using Microsoft.AspNet.Identity;

namespace GamePortal.Logic.Services.Identity
{
    public class ApplicationUserManager : UserManager<User, Guid>
    {
        public ApplicationUserManager(IUserStore<User, Guid> store) : base(store)
        {
            UserValidator = new UserValidator<User, Guid>(this)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };

            UserLockoutEnabledByDefault = true;
            DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            MaxFailedAccessAttemptsBeforeLockout = 5;
        }

        public PaginationResponseModel<UpdateUserViewModel> GetUsers(int page = 1, int pageSize = 20, string firstNameFilter = "", string lastNameFilter = "", string emailFilter = "", string userNameFilter = "")
        {
            var query = Users.Include(x => x.HeaderFile).Where(x =>
                x.FirstName.Contains(firstNameFilter) && x.LastName.Contains(lastNameFilter) &&
                x.Email.Contains(emailFilter) && x.UserName.Contains(userNameFilter));
            var count = query.Count();
            var users = Mapper.Map<List<UpdateUserViewModel>>(query.OrderBy(x => x.FirstName).Skip((page - 1) * pageSize)
                .Take(pageSize).ToList());
            return new PaginationResponseModel<UpdateUserViewModel>
            {
                Items = users,
                Page = page,
                PageSize = pageSize,
                TotalItems = count,
                TotalPages = count / pageSize + 1
            };
        }

        public async Task<IdentityResult> UpdateUserAsync(UpdateUserViewModel user)
        {
            var userEntity = Users.FirstOrDefault(x => x.Id == user.Id);
            Mapper.Map(user, userEntity);
            return await UpdateAsync(userEntity);
        }

        public async Task<UpdateUserViewModel> GetUserByIdAsync(Guid userId) =>
            Mapper.Map<UpdateUserViewModel>(await FindByIdAsync(userId));

        public async Task<IdentityResult> CreateUserAsync(RegisterViewModel user) =>
            await CreateAsync(Mapper.Map<User>(user), user.Password);

        public User GetUserWithIncludes(Guid userId)
        {
            return Users.Include(x => x.Comments).Include(x => x.Votes).First(x => x.Id == userId);
        }
    }
}
