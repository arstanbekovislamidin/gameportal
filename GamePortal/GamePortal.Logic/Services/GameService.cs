﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GamePortal.Domain.Contracts.Services;
using GamePortal.Domain.Contracts.UnitOfWork;
using GamePortal.Domain.Models;
using GamePortal.Domain.Models.Entities;
using AutoMapper;
using GamePortal.Domain.Enums;
using GamePortal.Domain.Models.DTO.ResponseModels;
using GamePortal.Domain.Models.DTO.ViewModels;
using GamePortal.Domain.Models.Identity;
using Unity.Interception.Utilities;

namespace GamePortal.Logic.Services
{
    public class GameService : BaseService, IGameService
    {
        private readonly FileService _fileService;
        public GameService(IUnitOfWork unitOfWork, FileService fileService) : base(unitOfWork)
        {
            _fileService = fileService;
        }

        public PaginationResponseModel<GameResponseModel> GetGamesPagination(int page, int pageSize, string nameFilter, List<Guid> genresFilter, List<Guid> tagsFilter)
        {
            var repository = UnitOfWork.GetRepository<Game>();
            var query = repository.FindBy(x => x.Name.Contains(nameFilter));
            if (genresFilter != null && genresFilter.Any())
            {
                query = query.Where(x => !genresFilter.Except(x.Genres.Select(g => g.Id)).Any());
            }
            if (tagsFilter != null && tagsFilter.Any())
            {
                query = query.Where(x => !tagsFilter.Except(x.Tags.Select(t => t.Id)).Any());
            }
            var count = query.Count();
            if (page == 0) page = 1;
            if (pageSize == 0) pageSize = 20;
            var games = Mapper.Map<List<GameResponseModel>>(query.OrderBy(x => x.Name).Skip((page - 1) * pageSize)
                .Take(pageSize).ToList());
            return new PaginationResponseModel<GameResponseModel>
            {
                Items = games,
                Page = page,
                PageSize = pageSize,
                TotalItems = count,
                TotalPages = count / pageSize + 1
            };
        }

        public PaginationResponseModel<GameWebApiResponseModel> GetGamesWebApiPagination(int page, int pageSize, string nameFilter, List<Guid> genresFilter, List<Guid> tagsFilter)
        {
            var repository = UnitOfWork.GetRepository<Game>();
            var query = repository.FindBy(x => x.Name.Contains(nameFilter));
            if (genresFilter != null && genresFilter.Any())
            {
                query = query.Where(x => !genresFilter.Except(x.Genres.Select(g => g.Id)).Any());
            }
            if (tagsFilter != null && tagsFilter.Any())
            {
                query = query.Where(x => !tagsFilter.Except(x.Tags.Select(t => t.Id)).Any());
            }
            var count = query.Count();
            if (page == 0) page = 1;
            if (pageSize == 0) pageSize = 20;
            var games = Mapper.Map<List<GameWebApiResponseModel>>(query.OrderBy(x => x.Name).Skip((page - 1) * pageSize)
                .Take(pageSize).ToList());
            games.ForEach(g =>
            {
                g.HeaderFileContent = System.IO.File.ReadAllBytes(_fileService.GetGameFilePath(g.HeaderFileId));
            });
            return new PaginationResponseModel<GameWebApiResponseModel>
            {
                Items = games,
                Page = page,
                PageSize = pageSize,
                TotalItems = count,
                TotalPages = count / pageSize + 1
            };
        }

        public GameResponseModel GetGame(Guid gameId)
        {
            var repository = UnitOfWork.GetRepository<Game>();
            var gameResponse = Mapper.Map<GameResponseModel>(repository.FindById(gameId));
            return gameResponse ?? new GameResponseModel();
        }

        public GameViewModel GetGameWithUser(Guid gameId, User user)
        {
            var repository = UnitOfWork.GetRepository<Game>();
            var gameResponse = Mapper.Map<GameViewModel>(repository.FindById(gameId));
            if (gameResponse == null || user == null) return new GameViewModel();
            gameResponse.CurrentUser = Mapper.Map<UserResponseModel>(user);
            gameResponse.CurrentUser.Votes = gameResponse.CurrentUser.Votes.Where(x => x.NewsId == null).ToList();
            gameResponse.CurrentUser.Comments = gameResponse.CurrentUser.Comments.Where(x => x.NewsId == null).ToList();
            gameResponse.Comments = GetHierarchyComments(gameId, gameResponse.CurrentUser, null);
            gameResponse.IsLiked = gameResponse.CurrentUser.Votes.Any(x => x.GameId == gameResponse.Id && x.Score == ScoreType.Positive);
            gameResponse.IsUnliked = gameResponse.CurrentUser.Votes.Any(x => x.GameId == gameResponse.Id && x.Score == ScoreType.Negative);
            return gameResponse;
        }

        public ExecuteResult AddGame(GameResponseModel game, HttpPostedFileBase file, Guid createdBy)
        {
            return Execute(() =>
            {
                var gameRepository = UnitOfWork.GetRepository<Game>();
                var checkUnique = gameRepository.Any(x => x.Name == game.Name);
                if (checkUnique) return ExecuteResult.Error("Игра с таким именем уже существует");
                var gameToAdd = Mapper.Map<Game>(game);
                gameToAdd.CreateDateTime = DateTime.Now;
                gameToAdd.LastUpdateDateTime = DateTime.Now;
                gameToAdd.HeaderFileId = _fileService.UploadGameFile(file);
                gameToAdd.CreatedByUserId = createdBy;
                var fileRepository = UnitOfWork.GetRepository<File>();
                gameToAdd.Files?.ForEach(x =>
                {
                    fileRepository.Attach(x);
                });
                var genreRepository = UnitOfWork.GetRepository<Genre>();
                gameToAdd.Genres?.ForEach(x =>
                {
                    genreRepository.Attach(x);
                });
                var tagRepository = UnitOfWork.GetRepository<Tag>();
                gameToAdd.Tags?.ForEach(x =>
                {
                    tagRepository.Attach(x);
                });
                gameRepository.Add(gameToAdd);
                UnitOfWork.SaveChanges();
                return ExecuteResult.Ok("Игра успешно добавлена");
            });
        }

        public ExecuteResult UpdateGame(GameResponseModel game, HttpPostedFileBase file)
        {
            return Execute(() =>
            {
                var repository = UnitOfWork.GetRepository<Game>();
                var oldGame = repository.FindById(game.Id);
                //if (oldGame == null) return ExecuteResult.Error("Изменяемой игры больше не существует");
                var checkUnique = repository.Any(x => x.Name == game.Name && x.Id != game.Id);
                if (checkUnique) return ExecuteResult.Error("Игра с таким именем уже существует");
                Mapper.Map(game, oldGame);
                if (oldGame != null)
                {
                    var fileRepository = UnitOfWork.GetRepository<File>();
                    if (file != null && file.ContentLength > 0)
                    {
                        _fileService.DeleteGameFile(oldGame.HeaderFile.Name);
                        fileRepository.Delete(oldGame.HeaderFile);
                        oldGame.HeaderFileId = _fileService.UploadGameFile(file);
                    }
                    oldGame.Files.Clear();
                    oldGame.Genres.Clear();
                    oldGame.Tags.Clear();
                    if (game.Files != null && game.Files.Any())
                    {
                        var filesIds = game.Files.Select(x => x.Id);
                        oldGame.Files = fileRepository.GetAll().Where(x => filesIds.Contains(x.Id))
                            .ToList();
                    }
                    if (game.Genres != null && game.Genres.Any())
                    {
                        var genreRepository = UnitOfWork.GetRepository<Genre>();
                        var genresIds = game.Genres.Select(x => x.Id);
                        oldGame.Genres = genreRepository.GetAll().Where(x => genresIds.Contains(x.Id))
                            .ToList();
                    }
                    if (game.Tags != null && game.Tags.Any())
                    {
                        var tagRepository = UnitOfWork.GetRepository<Tag>();
                        var tagsIds = game.Tags.Select(x => x.Id);
                        oldGame.Tags = tagRepository.GetAll().Where(x => tagsIds.Contains(x.Id)).ToList();
                    }
                    oldGame.LastUpdateDateTime = DateTime.Now;
                }
                repository.Edit(oldGame);
                UnitOfWork.SaveChanges();
                return ExecuteResult.Ok("Игра успешно изменена");
            });
        }

        public ExecuteResult DeleteGame(Guid gameId)
        {
            return Execute(() =>
            {
                var repository = UnitOfWork.GetRepository<Game>();
                var fileRepository = UnitOfWork.GetRepository<File>();
                var gameToDelete = repository.FindById(gameId);
                if (gameToDelete != null)
                {
                    _fileService.DeleteGameFile(gameToDelete.HeaderFile.Name);
                    fileRepository.Delete(gameToDelete.HeaderFile);
                    gameToDelete.Files.ForEach(x =>
                    {
                        _fileService.DeleteGameFile(x.Name);
                        fileRepository.Delete(x);
                    });
                }
                repository.Delete(gameToDelete);
                UnitOfWork.SaveChanges();
                return ExecuteResult.Ok("Игра успешно удалена");
            });
        }

        private List<CommentResponseModel> GetHierarchyComments(Guid gameId, UserResponseModel currentUser, Guid? parentId)
        {
            var result = Mapper.Map<List<CommentResponseModel>>(
                UnitOfWork.GetRepository<Comment>().GetAll()
                    .Where(x => x.Game.Id == gameId && x.CommentReply.Id == parentId).ToList());
            result.ForEach(x =>
            {
                x.IsLiked = currentUser.Votes.Any(v => v.CommentId == x.Id && v.Score == ScoreType.Positive);
                x.IsUnliked = currentUser.Votes.Any(v => v.CommentId == x.Id && v.Score == ScoreType.Negative);
                x.RepliesToComment = GetHierarchyComments(gameId, currentUser, x.Id);
            });
            return result;
        }

        public ExecuteResult VoteGame(Guid gameId, ScoreType score, Guid userId)
        {
            return Execute(() =>
            {
                var repository = UnitOfWork.GetRepository<Game>();
                var game = repository.FindById(gameId);
                var vote = game.Votes.FirstOrDefault(x => x.CreatedByUser.Id == userId);
                if (vote == null)
                {
                    game.Votes.Add(new Vote
                    {
                        CreatedByUserId = userId,
                        Score = score,
                        Game = game,
                        CreateDateTime = DateTime.Now
                    });
                }
                else
                {
                    if (vote.Score == score)
                    {
                        UnitOfWork.GetRepository<Vote>().Delete(vote);
                    }
                    else
                    {
                        vote.Score = score;
                    }
                }
                UnitOfWork.SaveChanges();
                return ExecuteResult.Ok();
            });
        }

        public ExecuteResult VoteComment(Guid commentId, ScoreType score, Guid userId)
        {
            return Execute(() =>
            {
                var repository = UnitOfWork.GetRepository<Comment>();
                var comment = repository.FindById(commentId);
                var vote = comment.Votes.FirstOrDefault(x => x.CreatedByUser.Id == userId);
                if (vote == null)
                {
                    comment.Votes.Add(new Vote
                    {
                        CreatedByUserId = userId,
                        Score = score,
                        Comment = comment,
                        CreateDateTime = DateTime.Now
                    });
                }
                else
                {
                    if (vote.Score == score)
                    {
                        UnitOfWork.GetRepository<Vote>().Delete(vote);
                    }
                    else
                    {
                        vote.Score = score;
                    }
                }
                UnitOfWork.SaveChanges();
                return ExecuteResult.Ok();
            });
        }

        public ExecuteResult AddComment(string text, Guid gameId, Guid? replyId, Guid userId)
        {
            return Execute(() =>
            {
                var repository = UnitOfWork.GetRepository<Game>();
                var game = repository.FindById(gameId);
                game.Comments.Add(new Comment
                {
                    CreatedByUserId = userId,
                    Text = text,
                    CreateDateTime = DateTime.Now,
                    CommentReply = UnitOfWork.GetRepository<Comment>().FindById(replyId ?? Guid.Empty),
                    LastUpdateDateTime = DateTime.Now
                });
                UnitOfWork.SaveChanges();
                return ExecuteResult.Ok();
            });
        }
    }
}

