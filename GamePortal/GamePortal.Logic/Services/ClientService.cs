﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using GamePortal.Domain.Contracts.Services;
using GamePortal.Domain.Contracts.UnitOfWork;
using GamePortal.Domain.Models;
using GamePortal.Domain.Models.DTO.ResponseModels;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Logic.Services
{
    public class ClientService : BaseService, IClientService
    {
        public ClientService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public PaginationResponseModel<ClientResponseModel> GetClientsPagination(int page, int pageSize, string nameFilter, Guid currentUserId)
        {
            var repository = UnitOfWork.GetRepository<Client>();
            var list = repository.FindBy(x => x.Name.Contains(nameFilter) && x.CreatedById == currentUserId);
            list = list.OrderBy(x => x.Name);
            var count = list.Count();
            if (page == 0) page = 1;
            if (pageSize == 0) pageSize = 20;
            return new PaginationResponseModel<ClientResponseModel>
            {
                Items = Mapper.Map<List<ClientResponseModel>>(list.Skip((page - 1) * pageSize).Take(pageSize).ToList()),
                Page = page,
                PageSize = pageSize,
                TotalItems = count,
                TotalPages = count / pageSize + 1
            };
        }

        public ClientResponseModel GetClient(Guid clientId)
        {
            var repository = UnitOfWork.GetRepository<Client>();
            var client = repository.FindById(clientId);
            return Mapper.Map<ClientResponseModel>(client);
        }

        public ExecuteResult AddClient(ClientResponseModel client, Guid currentUserId)
        {
            return Execute(() =>
            {
                var repository = UnitOfWork.GetRepository<Client>();
                var clientToAdd = Mapper.Map<Client>(client);
                clientToAdd.CreatedById = currentUserId;
                repository.Add(clientToAdd);
                UnitOfWork.SaveChanges();
                return ExecuteResult.Ok("Клиент успешно добавлен");
            });
        }

        public ExecuteResult UpdateClient(ClientResponseModel client)
        {
            return Execute(() =>
            {
                var repository = UnitOfWork.GetRepository<Client>();
                var clients = repository.GetAll().AsNoTracking();
                var oldClient = clients.Any(x => x.Id == client.Id);
                if (!oldClient && client != null) return ExecuteResult.Error("Изменяемого клиента больше не существует");
                var newClient = Mapper.Map<Client>(client);
                repository.Edit(newClient);
                UnitOfWork.SaveChanges();
                return ExecuteResult.Ok("Клиент успешно изменен");
            });
        }

        public ExecuteResult DeleteClient(Guid clientId, Guid currentUserId)
        {
            return Execute(() =>
            {
                var repository = UnitOfWork.GetRepository<Client>();
                var clientToDelete = repository.FindById(clientId);
                if (clientToDelete.CreatedById != currentUserId)
                {
                    return ExecuteResult.Error("У вас нет прав на удаление");
                }
                repository.Delete(clientToDelete);
                UnitOfWork.SaveChanges();
                return ExecuteResult.Ok("Клиент успешно удален");
            });
        }
    }
}
