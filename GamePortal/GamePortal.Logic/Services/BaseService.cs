﻿using System;
using GamePortal.Domain.Contracts.UnitOfWork;
using GamePortal.Domain.Models;

namespace GamePortal.Logic.Services
{
    public class BaseService
    {
        protected readonly IUnitOfWork UnitOfWork;

        protected BaseService(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        protected ExecuteResult Execute(Func<ExecuteResult> func)
        {
            try
            {
                return func();
            }
            catch (Exception exp)
            {
                return ExecuteResult.Error(exp.Message);
            }
        }
    }
}
