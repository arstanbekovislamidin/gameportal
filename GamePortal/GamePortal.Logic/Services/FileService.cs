﻿using System;
using System.IO;
using System.Web;
using AutoMapper;
using GamePortal.Common;
using GamePortal.Domain.Contracts.Services;
using GamePortal.Domain.Contracts.UnitOfWork;
using GamePortal.Domain.Models.DTO.ResponseModels;
using File = GamePortal.Domain.Models.Entities.File;

namespace GamePortal.Logic.Services
{
    public class FileService : BaseService, IFileService
    {
        private static readonly string PathToDataStorage =
            System.Configuration.ConfigurationManager.AppSettings["PathToDataStorage"];
        private static readonly string PathToGenreFiles = Path.Combine(PathToDataStorage, "Genres");
        private static readonly string PathToGameFiles = Path.Combine(PathToDataStorage, "Games");
        private static readonly string PathToNewsFiles = Path.Combine(PathToDataStorage, "News");
        private static readonly string PathToUserFiles = Path.Combine(PathToDataStorage, "Users");

        public FileService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public FileResponseModel UploadGameFiles(HttpPostedFileBase file)
        {
            var fileName = FileHelper.SaveFileToDisk(PathToGameFiles, file);
            var repository = UnitOfWork.GetRepository<File>();
            repository.Add(new File
            {
                Name = fileName
            });
            UnitOfWork.SaveChanges();
            return Mapper.Map<FileResponseModel>(repository.FirstOrDefault(x => x.Name.Equals(fileName)));
        }

        public FileResponseModel UploadNewsFiles(HttpPostedFileBase file)
        {
            var fileName = FileHelper.SaveFileToDisk(PathToNewsFiles, file);
            var repository = UnitOfWork.GetRepository<File>();
            repository.Add(new File
            {
                Name = fileName
            });
            UnitOfWork.SaveChanges();
            return Mapper.Map<FileResponseModel>(repository.FirstOrDefault(x => x.Name.Equals(fileName)));
        }

        public Guid UploadGameFile(HttpPostedFileBase file)
        {
            var fileName = FileHelper.SaveFileToDisk(PathToGameFiles, file);
            var repository = UnitOfWork.GetRepository<File>();
            repository.Add(new File
            {
                Name = fileName
            });
            UnitOfWork.SaveChanges();
            return repository.FirstOrDefault(x => x.Name.Equals(fileName)).Id;
        }
        
        public Guid UploadNewsFile(HttpPostedFileBase file)
        {
            var fileName = FileHelper.SaveFileToDisk(PathToNewsFiles, file);
            var repository = UnitOfWork.GetRepository<File>();
            repository.Add(new File
            {
                Name = fileName
            });
            UnitOfWork.SaveChanges();
            return repository.FirstOrDefault(x => x.Name.Equals(fileName)).Id;
        }

        public Guid UploadGenreFile(HttpPostedFileBase file)
        {
            var fileName = FileHelper.SaveFileToDisk(PathToGenreFiles, file);
            var repository = UnitOfWork.GetRepository<File>();
            repository.Add(new File
            {
                Name = fileName
            });
            UnitOfWork.SaveChanges();
            return repository.FirstOrDefault(x => x.Name.Equals(fileName)).Id;
        }

        public Guid UploadUserFile(HttpPostedFileBase file)
        {
            var fileName = FileHelper.SaveFileToDisk(PathToUserFiles, file);
            var repository = UnitOfWork.GetRepository<File>();
            repository.Add(new File
            {
                Name = fileName
            });
            UnitOfWork.SaveChanges();
            return repository.FirstOrDefault(x => x.Name.Equals(fileName)).Id;
        }

        public string GetGameFilePath(Guid fileId)
        {
            return Path.Combine(PathToGameFiles, UnitOfWork.GetRepository<File>().FindById(fileId).Name);
        }

        public string GetNewsFilePath(Guid fileId)
        {
            return Path.Combine(PathToNewsFiles, UnitOfWork.GetRepository<File>().FindById(fileId).Name);
        }

        public string GetGenreFilePath(Guid fileId)
        {
            return Path.Combine(PathToGenreFiles, UnitOfWork.GetRepository<File>().FindById(fileId).Name);
        }

        public string GetUserFilePath(Guid fileId)
        {
            return Path.Combine(PathToUserFiles, UnitOfWork.GetRepository<File>().FindById(fileId).Name);
        }

        public void DeleteGameFile(string fileName)
        {
            FileHelper.DeleteFile(Path.Combine(PathToGameFiles, fileName));
        }
        
        public void DeleteNewsFile(string fileName)
        {
            FileHelper.DeleteFile(Path.Combine(PathToNewsFiles, fileName));
        }

        public void DeleteGenreFile(string fileName)
        {
            FileHelper.DeleteFile(Path.Combine(PathToGenreFiles, fileName));
        }

        public void DeleteUserFile(string fileName)
        {
            FileHelper.DeleteFile(Path.Combine(PathToUserFiles, fileName));
        }
    }
}
