﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using GamePortal.Domain.Contracts.Services;
using GamePortal.Domain.Contracts.UnitOfWork;
using GamePortal.Domain.Models;
using GamePortal.Domain.Models.DTO.ResponseModels;
using GamePortal.Domain.Models.Entities;

namespace GamePortal.Logic.Services
{
    public class TagService : BaseService, ITagService
    {
        public TagService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public PaginationResponseModel<TagResponseModel> GetTagsPagination(int page, int pageSize, string nameFilter, List<Guid> selectedTagIds)
        {
            var repository = UnitOfWork.GetRepository<Tag>();
            var list = repository.FindBy(x => x.Name.Contains(nameFilter));
            if (selectedTagIds != null && selectedTagIds.Count > 0)
            {
                list = list.Where(x => !selectedTagIds.Contains(x.Id));
            }
            list = list.OrderBy(x => x.Name);
            var count = list.Count();
            if (page == 0) page = 1;
            if (pageSize == 0) pageSize = 20;
            return new PaginationResponseModel<TagResponseModel>
            {
                Items = Mapper.Map<List<TagResponseModel>>(list.Skip((page - 1) * pageSize).Take(pageSize).ToList()),
                Page = page,
                PageSize = pageSize,
                TotalItems = count,
                TotalPages = count / pageSize + 1
            };
        }

        public TagResponseModel GetTag(Guid tagId)
        {
            var repository = UnitOfWork.GetRepository<Tag>();
            return Mapper.Map<TagResponseModel>(repository.FindById(tagId));
        }

        public ExecuteResult AddTag(TagResponseModel tag)
        {
            return Execute(() =>
            {
                var repository = UnitOfWork.GetRepository<Tag>();
                var checkUniqueTag = repository.Any(x => x.Name == tag.Name);
                if (checkUniqueTag) return ExecuteResult.Error("Тэг с таким именем уже существует");
                var tagToAdd = Mapper.Map<Tag>(tag);
                repository.Add(tagToAdd);
                UnitOfWork.SaveChanges();
                return ExecuteResult.Ok("Тэг успешно добавлен");
            });
        }

        public ExecuteResult UpdateTag(TagResponseModel tag)
        {
            return Execute(() =>
            {
                var repository = UnitOfWork.GetRepository<Tag>();
                var oldTag = repository.Any(x => x.Id == tag.Id);
                if (!oldTag && tag != null) return ExecuteResult.Error("Изменяемого тэга больше не существует");
                var checkUniqueTag = repository.Any(x => x.Name == tag.Name && x.Id != tag.Id);
                if (checkUniqueTag) return ExecuteResult.Error("Тэг с таким именем уже существует");
                var newTag = Mapper.Map<Tag>(tag);
                repository.Edit(newTag);
                UnitOfWork.SaveChanges();
                return ExecuteResult.Ok("Тэг успешно изменен");
            });
        }

        public ExecuteResult DeleteTag(Guid tagId)
        {
            return Execute(() =>
            {
                var repository = UnitOfWork.GetRepository<Tag>();
                var tagToDelete = repository.FindById(tagId);
                repository.Delete(tagToDelete);
                UnitOfWork.SaveChanges();
                return ExecuteResult.Ok("Тэг успешно удален");
            });
        }
    }
}
