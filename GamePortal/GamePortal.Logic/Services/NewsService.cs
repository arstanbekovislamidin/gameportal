﻿using System;
using AutoMapper;
using System.Web;
using System.Linq;
using GamePortal.Domain.Models;
using System.Collections.Generic;
using Unity.Interception.Utilities;
using GamePortal.Domain.Models.Entities;
using GamePortal.Domain.Models.Identity;
using GamePortal.Domain.Contracts.Services;
using GamePortal.Domain.Contracts.UnitOfWork;
using GamePortal.Domain.Enums;
using GamePortal.Domain.Models.DTO.ResponseModels;
using GamePortal.Domain.Models.DTO.ViewModels;
using File = GamePortal.Domain.Models.Entities.File;

namespace GamePortal.Logic.Services
{
    public class NewsService : BaseService, INewsService
    {
        private readonly FileService _fileService;
        public NewsService(IUnitOfWork unitOfWork, FileService fileService) : base(unitOfWork)
        {
            _fileService = fileService;
        }

        public PaginationResponseModel<NewsResponseModel> GetNewsPagination(int page, int pageSize, string titleFilter, List<Guid> tagsFilter)
        {
            var repository = UnitOfWork.GetRepository<News>();
            var query = repository.FindBy(x => x.Title.Contains(titleFilter));
            if (tagsFilter != null && tagsFilter.Any())
            {
                query = query.Where(x => !tagsFilter.Except(x.Tags.Select(t => t.Id)).Any());
            }
            var count = query.Count();
            if (page == 0) page = 1;
            if (pageSize == 0) pageSize = 20;
            var news = Mapper.Map<List<NewsResponseModel>>(query.OrderBy(x => x.Title).Skip((page - 1) * pageSize)
                .Take(pageSize).ToList());
            return new PaginationResponseModel<NewsResponseModel>
            {
                Items = news,
                Page = page,
                PageSize = pageSize,
                TotalItems = count,
                TotalPages = count / pageSize + 1
            };
        }

        public PaginationResponseModel<NewsWebApiResponseModel> GetNewsWebApiPagination(int page, int pageSize, string titleFilter, List<Guid> tagsFilter)
        {
            var repository = UnitOfWork.GetRepository<News>();
            var query = repository.FindBy(x => x.Title.Contains(titleFilter));
            if (tagsFilter != null && tagsFilter.Any())
            {
                query = query.Where(x => !tagsFilter.Except(x.Tags.Select(t => t.Id)).Any());
            }
            var count = query.Count();
            if (page == 0) page = 1;
            if (pageSize == 0) pageSize = 20;
            var news = Mapper.Map<List<NewsWebApiResponseModel>>(query.OrderBy(x => x.Title).Skip((page - 1) * pageSize)
                .Take(pageSize).ToList());
            news.ForEach(n =>
            {
                n.HeaderFileContent = System.IO.File.ReadAllBytes(_fileService.GetGameFilePath(n.HeaderFileId));
            });
            return new PaginationResponseModel<NewsWebApiResponseModel>
            {
                Items = news,
                Page = page,
                PageSize = pageSize,
                TotalItems = count,
                TotalPages = count / pageSize + 1
            };
        }

        public NewsResponseModel GetNews(Guid newsId)
        {
            var repository = UnitOfWork.GetRepository<News>();
            var news = Mapper.Map<NewsResponseModel>(repository.FindById(newsId));
            return news ?? new NewsResponseModel();
        }

        public NewsViewModel GetNewsWithUser(Guid newsId, User user)
        {
            var repository = UnitOfWork.GetRepository<News>();
            var newsResponse = Mapper.Map<NewsViewModel>(repository.FindById(newsId));
            if (newsResponse == null || user == null) return new NewsViewModel();
            newsResponse.CurrentUser = Mapper.Map<UserResponseModel>(user);
            newsResponse.CurrentUser.Votes = newsResponse.CurrentUser.Votes.Where(x => x.GameId == null).ToList();
            newsResponse.CurrentUser.Comments = newsResponse.CurrentUser.Comments.Where(x => x.GameId == null).ToList();
            newsResponse.Comments = GetHierarchyComments(newsId, newsResponse.CurrentUser, null);
            newsResponse.IsLiked = newsResponse.CurrentUser.Votes.Any(x => x.NewsId == newsResponse.Id && x.Score == ScoreType.Positive);
            newsResponse.IsUnliked = newsResponse.CurrentUser.Votes.Any(x => x.NewsId == newsResponse.Id && x.Score == ScoreType.Negative);
            return newsResponse;
        }

        public ExecuteResult AddNews(NewsResponseModel news, HttpPostedFileBase file, Guid userId)
        {
            return Execute(() =>
            {
                var repository = UnitOfWork.GetRepository<News>();
                var checkUnique = repository.Any(x => x.Title == news.Title);
                if (checkUnique) return ExecuteResult.Error("Новость с таким заголовком уже существует");
                var newsToAdd = Mapper.Map<News>(news);
                newsToAdd.CreateDateTime = DateTime.Now;
                newsToAdd.LastUpdateDateTime = DateTime.Now;
                newsToAdd.HeaderFileId = _fileService.UploadNewsFile(file);
                newsToAdd.CreatedByUserId = userId;
                var fileRepository = UnitOfWork.GetRepository<File>();
                newsToAdd.Files.ForEach(x =>
                {
                    fileRepository.Attach(x);
                });
                var tagRepository = UnitOfWork.GetRepository<Tag>();
                newsToAdd.Tags.ForEach(x =>
                {
                    tagRepository.Attach(x);
                });
                repository.Add(newsToAdd);
                UnitOfWork.SaveChanges();
                return ExecuteResult.Ok("Новость успешно добавлена");
            });
        }

        public ExecuteResult UpdateNews(NewsResponseModel news, HttpPostedFileBase file)
        {
            return Execute(() =>
            {
                var repository = UnitOfWork.GetRepository<News>();
                var oldNews = repository.FindById(news.Id);
                //if (oldNews == null) return ExecuteResult.Error("Изменяемой новости больше не существует");
                var checkUnique = repository.Any(x => x.Title == news.Title && x.Id != news.Id);
                if (checkUnique) return ExecuteResult.Error("Новость с таким заголовком уже существует");
                Mapper.Map(news, oldNews);
                if (oldNews != null)
                {
                    oldNews.Files.Clear();
                    oldNews.Tags.Clear();
                    var fileRepository = UnitOfWork.GetRepository<File>();
                    if (file != null && file.ContentLength > 0)
                    {
                        _fileService.DeleteNewsFile(oldNews.HeaderFile.Name);
                        fileRepository.Delete(oldNews.HeaderFile);
                        oldNews.HeaderFileId = _fileService.UploadNewsFile(file);
                    }
                    if (news.Files != null && news.Files.Any())
                    {
                        var filesIds = news.Files.Select(x => x.Id);
                        oldNews.Files = fileRepository.GetAll().Where(x => filesIds.Contains(x.Id))
                            .ToList();
                    }
                    if (news.Tags != null && news.Tags.Any())
                    {
                        var tagRepository = UnitOfWork.GetRepository<Tag>();
                        var tagsIds = news.Tags.Select(x => x.Id);
                        oldNews.Tags = tagRepository.GetAll().Where(x => tagsIds.Contains(x.Id)).ToList();
                    }
                    oldNews.LastUpdateDateTime = DateTime.Now;
                }
                repository.Edit(oldNews);
                UnitOfWork.SaveChanges();
                return ExecuteResult.Ok("Новость успешно изменена");
            });
        }

        public ExecuteResult DeleteNews(Guid newsId)
        {
            return Execute(() =>
            {
                var repository = UnitOfWork.GetRepository<News>();
                var fileRepository = UnitOfWork.GetRepository<File>();
                var newsToDelete = repository.FindById(newsId);
                if (newsToDelete != null)
                {
                    _fileService.DeleteNewsFile(newsToDelete.HeaderFile.Name);
                    newsToDelete.Files.ForEach(x =>
                    {
                        fileRepository.Delete(x);
                        _fileService.DeleteNewsFile(x.Name);
                    });
                    fileRepository.Delete(newsToDelete.HeaderFile);
                }
                repository.Delete(newsToDelete);
                UnitOfWork.SaveChanges();
                return ExecuteResult.Ok("Новость успешно удалена");
            });
        }

        private List<CommentResponseModel> GetHierarchyComments(Guid newsId, UserResponseModel currentUser, Guid? parentId)
        {
            var result = Mapper.Map<List<CommentResponseModel>>(
                UnitOfWork.GetRepository<Comment>().GetAll()
                    .Where(x => x.News.Id == newsId && x.CommentReply.Id == parentId).ToList());
            result.ForEach(x =>
            {
                x.IsLiked = currentUser.Votes.Any(v => v.CommentId == x.Id && v.Score == ScoreType.Positive);
                x.IsUnliked = currentUser.Votes.Any(v => v.CommentId == x.Id && v.Score == ScoreType.Negative);
                x.RepliesToComment = GetHierarchyComments(newsId, currentUser, x.Id);
            });
            return result;
        }

        public ExecuteResult VoteNews(Guid newsId, ScoreType score, Guid userId)
        {
            return Execute(() =>
            {
                var repository = UnitOfWork.GetRepository<News>();
                var news = repository.FirstOrDefault(x => x.Id == newsId);
                var vote = news.Votes.FirstOrDefault(x => x.CreatedByUser.Id == userId);
                if (vote == null)
                {
                    news.Votes.Add(new Vote
                    {
                        CreatedByUserId = userId,
                        Score = score,
                        News = news,
                        CreateDateTime = DateTime.Now
                    });
                }
                else
                {
                    if (vote.Score == score)
                    {
                        UnitOfWork.GetRepository<Vote>().Delete(vote);
                    }
                    else
                    {
                        vote.Score = score;
                    }
                }
                UnitOfWork.SaveChanges();
                return ExecuteResult.Ok();
            });
        }

        public ExecuteResult VoteComment(Guid commentId, ScoreType score, Guid userId)
        {
            return Execute(() =>
            {
                var repository = UnitOfWork.GetRepository<Comment>();
                var comment = repository.FirstOrDefault(x => x.Id == commentId);
                var vote = comment.Votes.FirstOrDefault(x => x.CreatedByUser.Id == userId);
                if (vote == null)
                {
                    comment.Votes.Add(new Vote
                    {
                        CreatedByUserId = userId,
                        Score = score,
                        Comment = comment,
                        CreateDateTime = DateTime.Now
                    });
                }
                else
                {
                    if (vote.Score == score)
                    {
                        UnitOfWork.GetRepository<Vote>().Delete(vote);
                    }
                    else
                    {
                        vote.Score = score;
                    }
                }
                UnitOfWork.SaveChanges();
                return ExecuteResult.Ok();
            });
        }

        public ExecuteResult AddComment(string text, Guid newsId, Guid? replyId, Guid userId)
        {
            return Execute(() =>
            {
                var repository = UnitOfWork.GetRepository<News>();
                var news = repository.FirstOrDefault(x => x.Id == newsId);
                news.Comments.Add(new Comment
                {
                    CreatedByUserId = userId,
                    Text = text,
                    CreateDateTime = DateTime.Now,
                    CommentReply = UnitOfWork.GetRepository<Comment>().FirstOrDefault(x => x.Id == replyId),
                    LastUpdateDateTime = DateTime.Now
                });
                UnitOfWork.SaveChanges();
                return ExecuteResult.Ok();
            });
        }
    }
}
