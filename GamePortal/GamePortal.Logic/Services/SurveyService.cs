﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using GamePortal.Domain.Models;
using GamePortal.Domain.Contracts.Services;
using GamePortal.Domain.Contracts.UnitOfWork;
using GamePortal.Domain.Enums;
using GamePortal.Domain.Models.DTO.ResponseModels;
using GamePortal.Domain.Models.DTO.ViewModels;
using GamePortal.Domain.Models.Entities;
using Hangfire;

namespace GamePortal.Logic.Services
{
    public class SurveyService : BaseService, ISurveyService
    {
        public SurveyService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public PaginationResponseModel<SurveyResponseModel> GetSurveysPagination(int page, int pageSize, string nameFilter, bool isAdmin)
        {
            var repository = UnitOfWork.GetRepository<Survey>();
            var query = repository.FindBy(x => x.Name.Contains(nameFilter));
            var count = query.Count();
            if (page == 0) page = 1;
            if (pageSize == 0) pageSize = 20;
            if (!isAdmin)
            {
                query = query.Where(x => x.Status == SurveyStatus.Started);
            }
            var surveys = Mapper.Map<List<SurveyResponseModel>>(query.OrderBy(x => x.Name).Skip((page - 1) * pageSize)
                .Take(pageSize).ToList());
            return new PaginationResponseModel<SurveyResponseModel>
            {
                Items = surveys,
                Page = page,
                PageSize = pageSize,
                TotalItems = count,
                TotalPages = count / pageSize + 1
            };
        }

        public SurveyResponseModel GetSurvey(Guid surveyId)
        {
            var repository = UnitOfWork.GetRepository<Survey>();
            var surveyResponse = Mapper.Map<SurveyResponseModel>(repository.FindById(surveyId));
            return surveyResponse ?? new SurveyResponseModel();
        }

        public SurveyViewModel GetSurveyWithUser(Guid surveyId, Guid userId)
        {
            var repository = UnitOfWork.GetRepository<Survey>();
            var surveyViewModel = Mapper.Map<SurveyViewModel>(repository.FindById(surveyId));
            if (surveyViewModel == null) return new SurveyViewModel();
            surveyViewModel.Questions.ForEach(q =>
            {
                q.Answers.ForEach(a =>
                {
                    a.IsAnsweredByCurrentUser = a.Choices.Any(c => c.CreatedByUserId == userId);
                    a.Choices.Clear();
                });
            });
            return surveyViewModel;
        }

        public ExecuteResult AddSurvey(SurveyResponseModel survey, Guid userId)
        {
            return Execute(() =>
            {
                var repository = UnitOfWork.GetRepository<Survey>();
                var surveyToAdd = Mapper.Map<Survey>(survey);
                surveyToAdd.CreatedDateTime = DateTime.Now;
                surveyToAdd.CreatedByUserId = userId;
                repository.Add(surveyToAdd);
                surveyToAdd.StartJobId = BackgroundJob.Schedule(() => StartSurvey(surveyToAdd.Id), new TimeSpan(surveyToAdd.StartDateTime.Ticks));
                surveyToAdd.EndJobId = BackgroundJob.Schedule(() => EndSurvey(surveyToAdd.Id), new TimeSpan(surveyToAdd.EndDateTime.Ticks));
                UnitOfWork.SaveChanges();
                return ExecuteResult.Ok("Конкурс успешно создан");
            });
        }

        public ExecuteResult UpdateSurvey(SurveyResponseModel survey)
        {
            return Execute(() =>
            {
                var repository = UnitOfWork.GetRepository<Survey>();
                var oldSurvey = repository.FindById(survey.Id);
                //if (oldSurvey == null) return ExecuteResult.Error("Изменяемого конкурса не существует");
                if (oldSurvey != null)
                {
                    if (survey.StartDateTime != oldSurvey.StartDateTime && survey.StartDateTime > DateTime.Now)
                    {
                        BackgroundJob.Delete(oldSurvey.StartJobId);
                        oldSurvey.StartJobId = BackgroundJob.Schedule(() => StartSurvey(survey.Id), new TimeSpan(survey.StartDateTime.Ticks));
                    }
                    if (survey.EndDateTime != oldSurvey.EndDateTime && survey.EndDateTime > DateTime.Now &&
                        survey.EndDateTime > survey.StartDateTime)
                    {
                        BackgroundJob.Delete(oldSurvey.EndJobId);
                        oldSurvey.EndJobId = BackgroundJob.Schedule(() => EndSurvey(survey.Id), new TimeSpan(survey.EndDateTime.Ticks));
                    }
                    var questionRepository = UnitOfWork.GetRepository<Question>();
                    questionRepository.DeleteRange(oldSurvey.Questions);
                }
                Mapper.Map(survey, oldSurvey);
                repository.Edit(oldSurvey);
                UnitOfWork.SaveChanges();
                return ExecuteResult.Ok("Конкурс успешно обновлён");
            });
        }

        public ExecuteResult DeleteSurvey(Guid surveyId)
        {
            return Execute(() =>
            {
                var repository = UnitOfWork.GetRepository<Survey>();
                var surveyToDelete = repository.FindById(surveyId);
                if (surveyToDelete != null)
                {
                    BackgroundJob.Delete(surveyToDelete.StartJobId);
                    BackgroundJob.Delete(surveyToDelete.EndJobId);
                }
                repository.Delete(surveyToDelete);
                UnitOfWork.SaveChanges();
                return ExecuteResult.Ok("Конкурс успешно удалён");
            });
        }

        public ExecuteResult ChooseAnswer(Guid answerId, Guid currentUserId)
        {
            return Execute(() =>
            {
                var repository = UnitOfWork.GetRepository<Answer>();
                var answer = repository.FindById(answerId);
                if (answer.Question.Survey.Status != SurveyStatus.Started)
                {
                    return ExecuteResult.Error("Конкурс ещё не начался");
                }
                var choice = answer.Choices.FirstOrDefault(c => c.CreatedByUserId == currentUserId);
                if (choice != null)
                {
                    var choiseRepository = UnitOfWork.GetRepository<Choice>();
                    choiseRepository.Delete(choice);
                }
                else
                {
                    var previousAnwer = answer.Question.Answers.FirstOrDefault(a =>
                        a.Id != answerId && a.Choices.Any(c => c.CreatedByUserId == currentUserId));
                    if (previousAnwer != null)
                    {
                        var previousChoise = previousAnwer.Choices.First(c => c.CreatedByUserId == currentUserId);
                        previousChoise.Answer = answer;
                        previousChoise.CreateDateTime = DateTime.Now;
                    }
                    else
                    {
                        answer.Choices.Add(new Choice
                        {
                            CreateDateTime = DateTime.Now,
                            CreatedByUserId = currentUserId
                        });
                    }
                }
                UnitOfWork.SaveChanges();
                return ExecuteResult.Ok("Ответ был успешно сохранен");
            });
        }

        public void StartSurvey(Guid surveyId)
        {
            var repository = UnitOfWork.GetRepository<Survey>();
            var survey = repository.FindById(surveyId);
            survey.Status = SurveyStatus.Started;
            UnitOfWork.SaveChanges();
        }

        public void EndSurvey(Guid surveyId)
        {
            var repository = UnitOfWork.GetRepository<Survey>();
            var survey = repository.FindById(surveyId);
            survey.Status = SurveyStatus.Ended;
            UnitOfWork.SaveChanges();
        }
    }
}
