﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using GamePortal.Domain.Contracts.Services;
using GamePortal.Domain.Contracts.UnitOfWork;
using GamePortal.Domain.Models;
using GamePortal.Domain.Models.DTO.ResponseModels;
using GamePortal.Domain.Models.Entities;
using File = GamePortal.Domain.Models.Entities.File;

namespace GamePortal.Logic.Services
{
    public class GenreService : BaseService, IGenreService
    {
        private readonly FileService _fileService;
        public GenreService(IUnitOfWork unitOfWork, FileService fileService) : base(unitOfWork)
        {
            _fileService = fileService;
        }

        public PaginationResponseModel<GenreResponseModel> GetGenresPagination(int page, int pageSize, string nameFilter, List<Guid> selectedGenreIds)
        {
            var repository = UnitOfWork.GetRepository<Genre>();
            var list = repository.FindBy(x => x.Name.Contains(nameFilter));
            if (selectedGenreIds != null && selectedGenreIds.Any())
            {
                list = list.Where(x => !selectedGenreIds.Contains(x.Id));
            }
            list = list.OrderBy(x => x.Name);
            var count = list.Count();
            if (page == 0) page = 1;
            if (pageSize == 0) pageSize = 20;
            var models = Mapper.Map<List<GenreResponseModel>>(list.Skip((page - 1) * pageSize).Take(pageSize).ToList());
            return new PaginationResponseModel<GenreResponseModel>
            {
                Items = models,
                Page = page,
                PageSize = pageSize,
                TotalItems = count,
                TotalPages = count / pageSize + 1
            };
        }

        public GenreResponseModel GetGenre(Guid genreId)
        {
            var repository = UnitOfWork.GetRepository<Genre>();
            var model = Mapper.Map<GenreResponseModel>(repository.FindById(genreId));
            return model ?? new GenreResponseModel();
        }

        public ExecuteResult AddGenre(GenreResponseModel genre, HttpPostedFileBase file)
        {
            return Execute(() =>
            {
                var repository = UnitOfWork.GetRepository<Genre>();
                var checkUniqueGenre = repository.Any(x => x.Name == genre.Name);
                if (checkUniqueGenre) return ExecuteResult.Error("Жанр с таким именем уже существует");
                var genreToAdd = Mapper.Map<Genre>(genre);
                if (file != null && file.ContentLength > 0)
                {
                    genreToAdd.HeaderFileId = _fileService.UploadGenreFile(file);
                }
                repository.Add(genreToAdd);
                UnitOfWork.SaveChanges();
                return ExecuteResult.Ok("Жанр успешно добавлен");
            });
        }

        public ExecuteResult UpdateGenre(GenreResponseModel genre, HttpPostedFileBase file)
        {
            return Execute(() =>
            {
                var repository = UnitOfWork.GetRepository<Genre>();
                var oldGenre = repository.Any(x => x.Id == genre.Id);
                if (!oldGenre && genre != null) return ExecuteResult.Error("Изменяемого жанра больше не существует");
                var checkUniqueGenre = repository.Any(x => x.Name == genre.Name && x.Id != genre.Id);
                if (checkUniqueGenre) return ExecuteResult.Error("Жанр с таким именем уже существует");
                var newGenre = Mapper.Map<Genre>(genre);
                if (file != null && file.ContentLength > 0)
                {
                    var fileRepository = UnitOfWork.GetRepository<File>();
                    _fileService.DeleteGenreFile(newGenre.HeaderFile.Name);
                    newGenre.HeaderFileId = _fileService.UploadGenreFile(file);
                    fileRepository.Delete(newGenre.HeaderFile);
                }
                repository.Edit(newGenre);
                UnitOfWork.SaveChanges();
                return ExecuteResult.Ok("Жанр успешно изменен");
            });
        }

        public ExecuteResult DeleteGenre(Guid genreId)
        {
            return Execute(() =>
            {
                var repository = UnitOfWork.GetRepository<Genre>();
                var genreToDelete = repository.FindById(genreId);
                if (genreToDelete != null)
                    _fileService.DeleteGenreFile(genreToDelete.HeaderFile.Name);
                repository.Delete(genreToDelete);
                UnitOfWork.SaveChanges();
                return ExecuteResult.Ok("Жанр успешно удален");
            });
        }
    }
}
