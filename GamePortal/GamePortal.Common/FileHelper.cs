using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace GamePortal.Common
{
    public static class FileHelper
    {
        private static readonly IDictionary<string, string> Mappings =
            new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase)
            {
                {"image/png", ".png"},
                {"image/bmp", ".bmp"},
                {"image/x-icon", ".ico"},
                {"image/jpeg", ".jpeg"},
                {"image/jpg", ".jpg"},
                {"image/gif", ".gif"}
            };

        public static string SaveFileToDisk(string path, HttpPostedFileBase file)
        {
            if (file == null || file.ContentLength == 0) return string.Empty;
            var fileName = Guid.NewGuid() + Mappings[file.ContentType];
            var bytes = ConvertFromFileToByteArray(file);
            var fullPath = Path.Combine(path, fileName);
            File.WriteAllBytes(fullPath, bytes);
            return fileName;
        }

        public static byte[] ConvertFromFileToByteArray(HttpPostedFileBase file)
        {
            if (file == null || file.ContentLength <= 0) return null;
            using (MemoryStream target = new MemoryStream())
            {
                file.InputStream.CopyTo(target);
                return target.ToArray();
            }
        }

        public static void DeleteFile(string path)
        {
            if (!File.Exists(path)) return;
            var fullPath = Path.Combine(path);
            File.Delete(fullPath);
        }

        public static bool IsImage(string fileContentType)
        {
            return Mappings.ContainsKey(fileContentType);
        }

        public static string GetMymeType(string path)
        {
            var myme = path.Split('.').Last();
            return Mappings.FirstOrDefault(x => x.Value == $".{myme}").Key;
        }
    }
}
